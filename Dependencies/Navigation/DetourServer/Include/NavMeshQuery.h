#ifndef NAVMESH_QUERY
#define NAVMESH_QUERY

#include "DetourNavMesh.h"
#include "DetourNavMeshQuery.h"

/// Performs a vector copy.
///  @param[out]	dest	The result. [(x, y, z)]
///  @param[in]		v		The vector to copy. [(x, y, z)]
inline void rcVcopy(float* dest, const float* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}

class NavMeshQuery
{
public:
	NavMeshQuery();
	~NavMeshQuery();

	bool import(class NavMeshImporter* importer);
	int findPath(float spos[3], float epos[3], float* path);
	void setAreaCost(const int index, const float cost) { m_filter.setAreaCost(index, cost); }
	bool checkValidPath(float spos[3], float epos[3]);
private:	
	static const int MAX_POLYS = 256;
	// Explicitly disabled copy constructor and copy assignment operator.
	NavMeshQuery(const NavMeshQuery&);
	NavMeshQuery& operator=(const NavMeshQuery&);

	dtNavMesh* m_navMesh;
	dtNavMeshQuery* m_navQuery;
	dtQueryFilter m_filter;
	float m_polyPickExt[3];
};
#endif