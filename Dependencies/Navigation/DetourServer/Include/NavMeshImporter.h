#ifndef NAVMESH_IMPORTER
#define NAVMESH_IMPORTER

#include<string>

typedef unsigned char byte;

class NavMeshImporter
{
public:
	NavMeshImporter();
	~NavMeshImporter();

	const unsigned short* getVerts() const { return m_verts; }
	const unsigned short* getPolys() const { return m_polys; }
	const unsigned short* getFlags() const { return m_flags; }
	const unsigned char* getAreas() const { return m_areas; }
	int getNumOfVerts() const { return m_nverts; }
	int getNumOfPolys() const { return m_npolys; }
	int getMaxNumPerPoly() const { return m_nvp; }
	const float* getMinBounds() const { return m_bmin; }
	const float* getMaxBounds() const { return m_bmax; }
	float getXZCellSize() const { return m_cs; }
	float getYCellSize() const { return m_ch; }
	bool load(const std::string& fileName);
	const std::string& getFileName() const { return m_filename; }
private:
	// Explicitly disabled copy constructor and copy assignment operator.
	NavMeshImporter(const NavMeshImporter&);
	NavMeshImporter& operator=(const NavMeshImporter&);

	std::string m_filename;
	unsigned short* m_verts;	///< The mesh vertices. [Form: (x, y, z) * #nverts]
	unsigned short* m_polys;	///< Polygon and neighbor data. [Length: #maxpolys * 2 * #nvp]
	unsigned short* m_flags;	///< The user defined flags for each polygon. [Length: #maxpolys]
	unsigned char* m_areas;		///< The area id assigned to each polygon. [Length: #maxpolys]
	int m_nverts;				///< The number of vertices.
	int m_npolys;				///< The number of polygons.
	int m_nvp;					///< The maximum number of vertices per polygon.
	float m_bmin[3];			///< The minimum bounds in world space. [(x, y, z)]
	float m_bmax[3];			///< The maximum bounds in world space. [(x, y, z)]
	float m_cs;					///< The size of each cell. (On the xz-plane.)
	float m_ch;					///< The height of each cell. (The minimum increment along the y-axis.)
};
#endif