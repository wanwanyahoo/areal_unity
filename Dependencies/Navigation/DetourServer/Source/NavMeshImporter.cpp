#include "NavMeshImporter.h"
#include <stdio.h>
#include <string>

NavMeshImporter::NavMeshImporter() :
	m_verts(0),
	m_polys(0),
	m_flags(0),
	m_areas(0),
	m_nverts(0),
	m_npolys(0),
	m_nvp(0),
	m_cs(0),
	m_ch(0)
{
	memset(m_bmin, 0, sizeof(float) * 3);
	memset(m_bmin, 0, sizeof(float) * 3);
}

NavMeshImporter::~NavMeshImporter()
{
	delete[] m_verts;
	delete[] m_polys;
	delete[] m_flags;
	delete[] m_areas;
}

float readFloat(byte* buf, int& ptr)
{
	float f;
	memcpy(&f, buf + ptr, 4);
	ptr += 4;
	return f;
}
int readInt(byte* buf, int& ptr)
{
	int i;
	memcpy(&i, buf + ptr, 4);
	ptr += 4;
	return i;
}
unsigned short readUnsignedShort(byte* buf, int& ptr)
{
	unsigned short us;
	memcpy(&us, buf + ptr, 2);
	ptr += 2;
	return us;
}
byte readByte(byte* buf, int& ptr)
{
	return buf[ptr++];
}

bool NavMeshImporter::load(const std::string& filename)
{
	byte* buf = 0;
	FILE* fp = 0;
	fopen_s(&fp, filename.c_str(), "rb");
	if (!fp)
	{
		return false;
	}
	fseek(fp, 0, SEEK_END);
	int bufSize = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	buf = new byte[bufSize];
	if (!buf)
	{
		fclose(fp);
		return false;
	}
	size_t readLen = fread(buf, bufSize, 1, fp);
	fclose(fp);
	if (readLen != 1)
	{
		delete[] buf;
		return false;
	}
	//Read basic informations
	int ptr = 0;
	for (int i = 0; i < 3; ++i)
	{
		m_bmin[i] = readFloat(buf, ptr);
	}
	for (int i = 0; i < 3; ++i)
	{
		m_bmax[i] = readFloat(buf, ptr);
	}
	m_cs = readFloat(buf, ptr);
	m_ch = readFloat(buf, ptr);
	m_nvp = readInt(buf, ptr);
	//Read vertices
	m_nverts = readInt(buf, ptr);
	m_verts = new unsigned short[m_nverts * 3];
	for (int i = 0; i < m_nverts * 3; ++i)
	{
		m_verts[i] = readUnsignedShort(buf, ptr);
	}
	//Read polys
	m_npolys = readInt(buf, ptr);
	m_polys = new unsigned short[m_npolys * 2 * m_nvp];
	for (int i = 0; i < m_npolys * 2 * m_nvp; ++i)
	{
		m_polys[i] = readUnsignedShort(buf, ptr);
	}
	//Read areas and flags
	m_areas = new byte[m_npolys];
	for (int i = 0; i < m_npolys; ++i)
	{
		m_areas[i] = readByte(buf, ptr);
	}
	m_flags = new unsigned short[m_npolys];
	for (int i = 0; i < m_npolys; ++i)
	{
		m_flags[i] = readUnsignedShort(buf, ptr);
	}
	return true;
}