#include <jni.h>
#include <map>
#include "DetourNavigation.h"
#include "NavMeshImporter.h"
#include "NavMeshQuery.h"

std::map<int, NavMeshQuery*> meshQueriesMap;

JNIEXPORT jboolean JNICALL Java_game_gameserver_common_navmesh_DetourNavigation_load
(JNIEnv *env, jclass cls, jint id, jstring filename)
{
	const char* str = env->GetStringUTFChars(filename, 0);
	auto importer = new NavMeshImporter;
	bool result = importer->load(str);
	if (result)
	{
		auto query = new NavMeshQuery;
		result = query->import(importer);
		if (result)
		{
			meshQueriesMap.insert(std::pair<int, NavMeshQuery*>(id, query));
		}
		else
		{
			delete query;
		}
	}
	env->ReleaseStringUTFChars(filename, str);
	delete importer;
	return result;
}

JNIEXPORT jint JNICALL Java_game_gameserver_common_navmesh_DetourNavigation_findPath
(JNIEnv *env, jclass cls, jint id, jfloatArray spos, jfloatArray epos, jfloatArray path)
{
	int npath = 0;
	jfloat* jspos = env->GetFloatArrayElements(spos, 0);
	jfloat* jepos = env->GetFloatArrayElements(epos, 0);
	jfloat* jpath = env->GetFloatArrayElements(path, 0);
	auto it = meshQueriesMap.find(id);
	if (it != meshQueriesMap.end())
	{
		NavMeshQuery* query = it->second;
		npath = query->findPath(jspos, jepos, jpath);
	}
	jint jnpath = npath;
	env->ReleaseFloatArrayElements(spos, jspos, 0);
	env->ReleaseFloatArrayElements(epos, jepos, 0);
	env->ReleaseFloatArrayElements(path, jpath, 0);
	return jnpath;
}

JNIEXPORT jboolean JNICALL Java_game_gameserver_common_navmesh_DetourNavigation_checkValidPath
(JNIEnv *env, jclass cls, jint id, jfloatArray spos, jfloatArray epos)
{
	jfloat* jspos = env->GetFloatArrayElements(spos, 0);
	jfloat* jepos = env->GetFloatArrayElements(epos, 0);
	jboolean result = 0;
	auto it = meshQueriesMap.find(id);
	if (it != meshQueriesMap.end())
	{
		NavMeshQuery* query = it->second;
		result = query->checkValidPath(jspos, jepos);
	}
	env->ReleaseFloatArrayElements(spos, jspos, 0);
	env->ReleaseFloatArrayElements(epos, jepos, 0);
	return result;
}

JNIEXPORT void JNICALL Java_game_gameserver_common_navmesh_DetourNavigation_setAreaCost
(JNIEnv *env, jclass cls, jint id, jint areaIndex, jfloat cost)
{
	auto it = meshQueriesMap.find(id);
	if (it != meshQueriesMap.end())
	{
		NavMeshQuery* query = it->second;
		query->setAreaCost(areaIndex, cost);
	}
}