#include "NavMeshQuery.h"
#include "DetourCommon.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshBuilder.h"
#include "DetourNavMeshQuery.h"
#include "NavMeshImporter.h"

NavMeshQuery::NavMeshQuery() :
	m_navMesh(0),
	m_navQuery(0)
{
	m_polyPickExt[0] = 2;
	m_polyPickExt[1] = 4;
	m_polyPickExt[2] = 2;
}

NavMeshQuery::~NavMeshQuery()
{
	dtFreeNavMeshQuery(m_navQuery);
	dtFreeNavMesh(m_navMesh);
}

bool NavMeshQuery::import(NavMeshImporter* importer)
{
	dtNavMeshCreateParams params;
	memset(&params, 0, sizeof(params));
	params.verts = importer->getVerts();
	params.vertCount = importer->getNumOfVerts();
	params.polys = importer->getPolys();
	params.polyAreas = importer->getAreas();
	params.polyFlags = importer->getFlags();
	params.polyCount = importer->getNumOfPolys();
	params.nvp = importer->getMaxNumPerPoly();
	params.detailMeshes = NULL;
	params.offMeshConCount = 0;
	rcVcopy(params.bmin, importer->getMinBounds());
	rcVcopy(params.bmax, importer->getMaxBounds());
	params.cs = importer->getXZCellSize();
	params.ch = importer->getYCellSize();
	params.buildBvTree = true;

	unsigned char* navData = 0;
	int navDataSize = 0;
	if (!dtCreateNavMeshData(&params, &navData, &navDataSize))
	{
		printf_s("Could not build Detour navmesh.");
		return false;
	}

	m_navMesh = dtAllocNavMesh();
	if (!m_navMesh)
	{
		dtFree(navData);
		printf_s("Could not create Detour navmesh");
		return false;
	}

	dtStatus status;

	status = m_navMesh->init(navData, navDataSize, DT_TILE_FREE_DATA);
	if (dtStatusFailed(status))
	{
		dtFree(navData);
		printf_s("Could not init Detour navmesh");
		return false;
	}

	m_navQuery = dtAllocNavMeshQuery();
	status = m_navQuery->init(m_navMesh, 2048);
	if (dtStatusFailed(status))
	{
		printf_s("Could not init Detour navmesh query");
		return false;
	}
	return true;
}

int NavMeshQuery::findPath(float spos[3], float epos[3], float* path)
{
	if (!m_navQuery)
	{
		return 0;
	}
	dtPolyRef startRef;
	dtPolyRef endRef;
	m_navQuery->findNearestPoly(spos, m_polyPickExt, &m_filter, &startRef, 0);
	m_navQuery->findNearestPoly(epos, m_polyPickExt, &m_filter, &endRef, 0);

	int npolys;
	dtPolyRef polys[MAX_POLYS];
	int npath;
	unsigned char pathFlags[MAX_POLYS];
	dtPolyRef pathPolys[MAX_POLYS];
	m_navQuery->findPath(startRef, endRef, spos, epos, &m_filter, polys, &npolys, 256);
	if (npolys)
	{
		// In case of partial path, make sure the end point is clamped to the last polygon.
		float cepos[3];
		dtVcopy(cepos, epos);
		if (polys[npolys - 1] != endRef)
		{
			m_navQuery->closestPointOnPoly(polys[npolys - 1], epos, cepos, 0);
		}
		m_navQuery->findStraightPath(spos, cepos, polys, npolys, path, pathFlags, pathPolys, &npath, MAX_POLYS, 0);
	}
	return npath;
}

bool NavMeshQuery::checkValidPath(float spos[3], float epos[3])
{
	float path[MAX_POLYS];
	int nPath = findPath(spos, epos, path);
	if (path[(nPath - 1) * 3] == epos[0] &&
		path[(nPath - 1) * 3 + 1] == epos[1] &&
		path[(nPath - 1) * 3 + 2] == epos[2])
		return true;
	else
		return false;
}