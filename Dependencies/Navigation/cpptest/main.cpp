#include "NavMeshImporter.h"
#include "NavMeshQuery.h"

int main()
{
	auto importer = new NavMeshImporter;
	if (!importer->load("map1.nvm"))
	{
		return 0;
	}
	auto query = new NavMeshQuery;
	query->import(importer);

	query->setAreaCost(0, 1.0f);

	float spos[] = { 142.643494, 0.252494812, 15.2277374 };
	float epos[] = { 25.0915985, -1.50787354, 133.605362 };
	if (query->checkValidPath(spos, epos))
	{
		printf("The path is valid\n");
	}
	float path[256];
	int nPath = query->findPath(spos, epos, path);
	for (int i = 0; i < nPath; ++i)
	{
		printf("%f %f %f\n", path[i * 3], path[i * 3 + 1], path[i * 3 + 2]);
	}

	char s;
	scanf_s("%d", &s);
	delete importer;
	return 0;
}