
public class Main {
	public static void main(String[] args)
	{
		float spos[] = { 142.643494f, 0.252494812f, 15.2277374f };
		float epos[] = { 25.0915985f, -1.50787354f, 133.605362f };
		if (DetourNavigation.load(0, "map1.nvm"))
		{
			System.out.println("Successful loaded");
			DetourNavigation.setAreaCost(0, 0, 1);
			if (DetourNavigation.checkValidPath(0, spos, epos))
			{
				System.out.println("Valid path exists");
				float path[] = new float[256];
				int pointCount = DetourNavigation.findPath(0, spos, epos, path);
				for (int i = 0;i < pointCount;++i)
				{
					System.out.println(path[i * 3] + " " + path[i * 3 + 1] + " " + path[i * 3 + 2]);
				}
			}
		}
	}
}
