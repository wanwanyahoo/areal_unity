
public class DetourNavigation {
	static{
		System.loadLibrary("DetourServer");
	}
	
	public static native boolean load(int id, String filename);
	
	public static native int findPath(int id, float spos[], float epos[], float path[]);
	
	public static native boolean checkValidPath(int id, float spos[], float epos[]);
	
	public static native void setAreaCost(int id, int areaIndex, float cost);
}
