cd Generate\authc

rd /s /q defines
rd /s /q game
rd /s /q authc

java -jar rpcgen.jar -debug -aio -unity authc.xml
java -jar rpcgen.jar -debug -aio -unity gnet.xml

java -jar rpcgen.jar -debug -aio -unity lua_client.xml

md defines
md defines\rpcgen

move authc\gnet defines\gnet
move authc\rpcgen\gnet defines\rpcgen\gnet
move authc\protocols.lua.txt defines\authc.lua.txt
move game\knight defines\knight
move game\rpcgen\knight defines\rpcgen\knight
move game\protocols.lua.txt defines\game.lua.txt

rd /s /q game
rd /s /q authc

rd /s /q ..\..\Assets\GameAssets\lua\authc\defines
move defines ..\..\Assets\GameAssets\lua\authc\defines
pause
