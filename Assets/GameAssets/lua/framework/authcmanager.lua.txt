local AuthcManager = {}

local authcProtocolsNames = {}
local gameProtocolsNames = {}
local protocolProcessNames = {}
local protocolDefines = {}

local function ProtocolTypeToName(protocolType)
	if gameProtocolsNames[protocolType] then
		return gameProtocolsNames[protocolType]
	elseif authcProtocolsNames[protocolType] then
		return authcProtocolsNames[protocolType]
	else
		return nil
	end
end

local function ProtocolSend(self)
	local os = self:Encode()
	CS.AuthcManager.Instance:Send(os:Octets())
end

local function ProtocolGetType(self)
	return self.PROTOCOL_TYPE
end

local function ProtocolGetName(self)
	return self.PROTOCOL_NAME
end

local function GetProtocolDefineByName(protocolName)
	local newDefine = require("lua.authc.defines." .. protocolName)
	if not protocolDefines[newDefine.PROTOCOL_TYPE] then
		newDefine.Send = ProtocolSend
		newDefine.GetType = ProtocolGetType
		newDefine.GetName = ProtocolGetName
		if protocolProcessNames[protocolName] then
			newDefine.Process = require("lua.authc.process." .. protocolName)
		end
		protocolDefines[newDefine.PROTOCOL_TYPE] = newDefine
	end
	return protocolDefines[newDefine.PROTOCOL_TYPE]
end

local function GetProtocolDefineByType(protocolType)
	if not protocolDefines[protocolType] then
		local protocolName = ProtocolTypeToName(protocolType)
		if not protocolName then
			return nil
		end
		GetProtocolDefineByName(protocolName)
	end
	return protocolDefines[protocolType]
end

function AuthcManager.Init()
	authcProtocolsNames = require("lua.authc.defines.authc")
	gameProtocolsNames = require("lua.authc.defines.game")
	protocolProcessNames = require("lua.authc.process.list")
	CS.AuthcManager.Instance.ReceiveDelegate = AuthcManager.OnReceiveProtocol
	CS.AuthcManager.Instance.ExceptionDelegate = AuthcManager.OnSocketException
end

function AuthcManager.OnReceiveProtocol(receiveProtocol)
	local thisDefine = GetProtocolDefineByType(receiveProtocol.Type)
	if not thisDefine then
		CS.UnityEngine.Debug.LogError("receive unknown protocol type " .. tostring(receiveProtocol.Type))
		return
	end
	if not thisDefine.Process then
		CS.UnityEngine.Debug.LogWarning("can not find process of protocol" .. thisDefine:GetName())
		return
	end

	local luaProtocol = thisDefine.Create()
	local os = CS.Authc.OctetsStream(receiveProtocol.Data)
	luaProtocol:Unmarshal(os)
	luaProtocol:Process()
end

function AuthcManager.OnSocketException(code, message)

end

function AuthcManager.CreateProtocol(protocolName)
	local thisDefine = GetProtocolDefineByName(protocolName)
	return thisDefine.Create()
end

return AuthcManager