﻿Shader "Custom/SimpleEffect" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue" = "Transparent" }
		Pass {
			Lighting off
			Blend OneMinusDstColor	One
			SetTexture[_MainTex] {
				ConstantColor[_Color]
				combine texture * constant
			}
		}
	}
	FallBack "Diffuse"
}
