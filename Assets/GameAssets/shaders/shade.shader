﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Shade" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ShadeTex ("Shade (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue" = "Transparent" }
		Tags { "ForceNoShadowCasting" = "True" }
		Pass {
			Lighting off
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			sampler2D _ShadeTex;
			fixed4 _Color;
			struct vertin
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};
			struct vertout
			{
				float4 pos : SV_POSITION;
				float2 texcoord : TEXCOORD0;
			};
			vertout vert(vertin IN)
			{
				vertout OUT;
				OUT.pos = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				return OUT;
			}
			fixed4 frag(vertout IN) : COLOR
			{
				float4 maintexcolor =  tex2D(_MainTex, IN.texcoord);
				float4 shadetexcolor = tex2D(_ShadeTex, IN.texcoord);
				_Color.a *= shadetexcolor.r * 0.5;
				return maintexcolor * _Color;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
