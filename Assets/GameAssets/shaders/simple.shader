﻿Shader "Custom/Simple" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue" = "Geometry" }
		Pass {
			Lighting off
			Blend SrcAlpha OneMinusSrcAlpha
			SetTexture[_MainTex] {
				ConstantColor[_Color]
				combine texture * constant
			}
		}
	}
	FallBack "Diffuse"
}
