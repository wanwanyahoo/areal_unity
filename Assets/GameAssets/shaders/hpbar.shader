﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/HPBar" {
	Properties {
		_BColor ("Background Color", Color) = (1,1,1,1)
		_HPColor ("HP Color", Color) = (1,0,0,1)
		_Range ("HP", Range(0, 1)) = 1
	}
	SubShader {
		Tags { "ForceNoShadowCasting" = "True" "IgnoreProjector"="True"}
		Pass {
			ZTest off
			Lighting off
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			fixed4 _BColor;
			fixed4 _HPColor;
			float _Range;
			struct vertin
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};
			struct vertout
			{
				float4 pos : SV_POSITION;
				float2 texcoord : TEXCOORD0;
			};
			vertout vert(vertin IN)
			{
				vertout OUT;
				OUT.pos = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				return OUT;
			}
			fixed4 frag(vertout IN) : COLOR
			{
				fixed4 color;
				if (IN.texcoord.x < _Range)
					color = _HPColor;
				else
					color = _BColor;
				return color; 
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
