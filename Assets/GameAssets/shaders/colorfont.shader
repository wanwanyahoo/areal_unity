﻿Shader "Custom/ColorFont" {
	Properties {
		_MainTex ("Font Texture", 2D) = "white" {}  
		_Color ("Text Color", Color) = (1,1,1,1)  
	}
	SubShader {
		Tags{ "Queue"="Geometry-1" "IgnoreProjector"="True" "RenderType"="Transparent" }  
		Lighting Off 
		Cull Off
		ZTest Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha   
		Pass {
			SetTexture[_MainTex] {
				ConstantColor[_Color]
				combine texture * constant
			}
		}
	}
	FallBack "Diffuse"
}
