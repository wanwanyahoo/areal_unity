﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    static T instance;

    public static T Instance
    {
        get { return instance; }
    }
    public void SetInstance(T t)
    {
        if (instance == null)
        {
            instance = t;
        }
    }
    public virtual void Init()
    {
    }
    public virtual void SelfUpdate()
    {
    }
    public virtual void Release()
    {
    }
}
