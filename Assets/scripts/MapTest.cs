﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapTest : SingletonManager
{
    public string CreatureForTest;
    public Vector2 SpawnPoint;
    void Awake()
    {
        base.Init();
        if (Camera.main == null)
        {
            GameObject mainCamera = new GameObject("Main Camera");
            mainCamera.transform.Translate(new Vector3(0, 7, 0));
            mainCamera.transform.Rotate(new Vector3(30, 0, 0));
            mainCamera.AddComponent<Camera>().orthographic = true;
            mainCamera.AddComponent<CameraFollowing>();
            mainCamera.tag = "MainCamera";
        }
        SingletonManager.InitSingleton<ResourceManager>();
        SingletonManager.InitSingleton<SceneManager>();
        SingletonManager.InitSingleton<CreatureManager>();
        SingletonManager.InitSingleton<EffectManager>();
        SingletonManager.InitSingleton<UIManager>();
    }
    void Start()
    {
        if (Camera.main.GetComponent<CameraFollowing>() == null)
        {
            Camera.main.gameObject.AddComponent<CameraFollowing>();
        }
        if (!string.IsNullOrEmpty(CreatureForTest))
        {
            CreatureManager.Instance.CreateAsync(CreatureForTest, SpawnPoint, (creature) =>
                {
                    Camera.main.GetComponent<CameraFollowing>().SetTarget(creature.gameObject);
                    UIManager.Instance.CreateAsync("sceneboard", (ui) =>
                        {
                            ui.GetComponent<SceneEvent>().SetOnTerrainClick((position) =>
                                {
                                    creature.MoveTo(position);
                                });
                        });
                });
        }
    }
    void Update()
    {
        SingletonManager.UpdateSingletons();
    }
    void OnApplicationQuit()
    {
        SingletonManager.ReleaseSingletons();
    }
}
