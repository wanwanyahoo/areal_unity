﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;
using System.Xml;

[LuaCallCSharp]
public class LuaXMLParser
{
    public static LuaTable ParseFile(string path)
    {
        path = UnityEngine.Application.dataPath + "/serverconfig.bin";
        LuaEnv env = LuaManager.Instance.Env;
        LuaTable table = env.NewTable();

        XmlDocument doc = new XmlDocument();
        doc.Load(path);
        XmlElement root = doc.DocumentElement;

        ReadElement(root, table, env);

        return table;
    }

    private static void ReadElement(XmlElement element, LuaTable table, LuaEnv env)
    {
        table.Set("name", element.Name);
        table.Set("text", element.InnerText);

        LuaTable attributesTable = env.NewTable();
        foreach (XmlAttribute attribute in element.Attributes)
        {
            attributesTable.Set(attribute.Name, attribute.Value);
        }
        table.Set("attributes", attributesTable);

        LuaTable childsTable = env.NewTable();
        int childIndex = 1;
        foreach (XmlElement child in element.ChildNodes)
        {
            LuaTable childTable = env.NewTable();
            ReadElement(child, childTable, env);
            childsTable.Set(childIndex, childTable);
            ++childIndex;
        }
        table.Set("childs", childsTable);
    }
}
