﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTest : SingletonManager
{
    public List<FighterInfo> Info;
    void Awake()
    {
        base.Init();
        if (Camera.main == null)
        {
            GameObject mainCamera = new GameObject("Main Camera");
            mainCamera.transform.position = new Vector3(0, 13, -22.5f);
            mainCamera.transform.Rotate(new Vector3(30, 0, 0));
            var camera = mainCamera.AddComponent<Camera>();
            camera.orthographic = true;
            camera.orthographicSize = 3;
            mainCamera.tag = "MainCamera";
        }
        SingletonManager.InitSingleton<ResourceManager>();
        SingletonManager.InitSingleton<SceneManager>();
        SingletonManager.InitSingleton<CreatureManager>();
        SingletonManager.InitSingleton<EffectManager>();
        SingletonManager.InitSingleton<UIManager>();
    }
    void Start()
    {
        this.gameObject.AddComponent<Battle>().Launch(Info);
    }
    void Update()
    {
        SingletonManager.UpdateSingletons();
    }
    void OnApplicationQuit()
    {
        SingletonManager.ReleaseSingletons();
    }
}
