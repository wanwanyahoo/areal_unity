﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using DG.Tweening;

public class Attack : BattleAction 
{
    int restStage;
    int currentTarget;
    int targetsPerStage;
    public override string ToString()
    {
        return "物理攻击";
    }
    public override void PreAct()
    {
        currentTarget = 0;
        restStage = TotalStage;
        targetsPerStage = (int)Mathf.Ceil(TargetPoints.Count / (float)restStage);
        if (string.IsNullOrEmpty(Trigger))
        {
            SourceFighter.Animator.SetTrigger("ReadyForAttack");
            SourceFighter.Animator.SetInteger("RestStage", restStage);
            SourceFighter.Animator.Update(0);
        }
        float readyDuration = SourceFighter.AnimationEvent.GetFloat("ReadyDuration");
        float attackDistance = SourceFighter.AnimationEvent.GetFloat("AttackDistance");
        Vector3 direction = (SourceFighter.transform.position - TargetFighters[currentTarget].transform.position).normalized;
        SourceFighter.transform.parent.DOMove(TargetFighters[currentTarget].transform.position + direction * attackDistance, readyDuration);
        SourceFighter.transform.DOLookAt(TargetFighters[currentTarget].transform.position, readyDuration);
    }
    public override void Act()
    {
        SourceFighter.AnimationEvent.SetOnEvent("attack", () =>
        {
            SourceFighter.Animator.SetInteger("RestStage", --restStage);
            float attackedDuration = SourceFighter.AnimationEvent.GetFloat("AttackedDuration");
            for (int i = currentTarget; i < currentTarget + targetsPerStage; ++i)
            {
                if (i >= TargetFighters.Count)
                {
                    break;
                }
                TargetFighters[i].Animator.SetTrigger("Attacked");
                TargetFighters[i].transform.rotation = Quaternion.LookRotation(SourceFighter.transform.position - TargetFighters[i].transform.position);
                TargetFighters[i].transform.DORotateQuaternion(TargetTransforms[i].rotation, attackedDuration);
                Battle.DisplayDamage(TargetFighters[i], ConsumeImpact().number);
            }
            currentTarget += targetsPerStage;
            targetsPerStage = (int)Mathf.Ceil((TargetPoints.Count - currentTarget) / (float)restStage);
        });
        SourceFighter.AnimationEvent.SetOnEvent("attacknext", () =>
        {
            float attackInterval = SourceFighter.AnimationEvent.GetFloat("AttackInterval");
            float attackDistance = SourceFighter.AnimationEvent.GetFloat("AttackDistance");
            Vector3 direction = (SourceFighter.transform.position - TargetFighters[currentTarget].transform.position).normalized;
            SourceFighter.transform.parent.DOMove(TargetFighters[currentTarget].transform.position + direction * attackDistance, attackInterval);
            SourceFighter.transform.DOLookAt(TargetFighters[currentTarget].transform.position, attackInterval);
        });
    }
    public override void PostAct()
    {
        SourceFighter.AnimationEvent.SetOnEvent("attackend", () =>
        {
            float backDuration = SourceFighter.AnimationEvent.GetFloat("ReadyDuration");
            SourceFighter.transform.DORotateQuaternion(SourceTransform.rotation, backDuration);
            SourceFighter.transform.parent.DOMove(SourceTransform.position, backDuration).OnComplete(() =>
            {
                Done();
            });
        });
    }
}
