﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using DG.Tweening;

public class Protect : BattleAction 
{
    int attackPoint;
    Creature attacker;
    public Protect() { }
    public Protect(int sourcePoint, List<int> targetPoints, string trigger, int totalStage, List<BattleActionImpact> impacts, int attackPoint)
        : base(sourcePoint, targetPoints, trigger, totalStage, impacts)
    {
        this.attackPoint = attackPoint;
    }
    public override string ToString()
    {
        return "队友保护";
    }
    public override void OnGUI()
    {
#if UNITY_EDITOR
        base.OnGUI();
        attackPoint = EditorGUILayout.IntField("攻击者", attackPoint);
#endif
    }
    public override void PreAct()
    {
        attacker = Battle.GetFighter(attackPoint);
        float readyDuration = SourceFighter.AnimationEvent.GetFloat("ReadyDuration");
        float attackDistance = SourceFighter.AnimationEvent.GetFloat("AttackDistance");
        Vector3 direction = (TargetFighters[0].transform.position - attacker.transform.position).normalized;
        SourceFighter.transform.parent.DOMove(TargetFighters[0].transform.position, readyDuration);
        TargetFighters[0].transform.parent.DOMove(TargetFighters[0].transform.position + direction * attackDistance, readyDuration);
    }
    public override void Act()
    {
        TargetFighters[0].AnimationEvent.SetOnStart("attacked", () =>
        {
            float attackedDuration = SourceFighter.AnimationEvent.GetFloat("AttackedDuration");
            SourceFighter.Animator.SetTrigger("Attacked");
            SourceFighter.transform.rotation = Quaternion.LookRotation(attacker.transform.position - SourceFighter.transform.position);
            SourceFighter.transform.DORotateQuaternion(SourceTransform.rotation, attackedDuration);
            Battle.DisplayDamage(SourceFighter, ConsumeImpact().number);
        });
    }
    public override void PostAct()
    {
        SourceFighter.AnimationEvent.SetOnEnd("attacked", () =>
        {
            float backDuration = SourceFighter.AnimationEvent.GetFloat("ReadyDuration");
            SourceFighter.transform.DORotate(SourceTransform.rotation.eulerAngles, backDuration);
            SourceFighter.transform.parent.DOMove(SourceTransform.position, backDuration);
            TargetFighters[0].transform.parent.DOMove(TargetTransforms[0].position, backDuration);
        });
        Battle.Reserve(this);
        Done();
    }
    protected override void Done()
    {
        isDone = true;
    }
}
