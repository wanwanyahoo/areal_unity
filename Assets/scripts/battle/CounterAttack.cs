﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CounterAttack : BattleAction 
{
    public override string ToString()
    {
        return "反击攻击";
    }
    public override void Act()
    {
        SourceFighter.AnimationEvent.SetOnStart("attacked", () =>
        {
            TargetFighters[0].Animator.SetTrigger("CounterAttacked");
            if (string.IsNullOrEmpty(Trigger))
            {
                SourceFighter.Animator.SetTrigger("ReadyForAttack");
            }
            else
            {
                SourceFighter.Animator.SetTrigger(Trigger);
            }
            SourceFighter.Animator.Update(0);
            SourceFighter.transform.rotation = Quaternion.LookRotation(TargetFighters[0].transform.position - SourceFighter.transform.position);
        });
        SourceFighter.AnimationEvent.SetOnEvent("attackstart", () =>
        {
            float readyDuration = SourceFighter.AnimationEvent.GetFloat("ReadyDuration");
            float attackDistance = SourceFighter.AnimationEvent.GetFloat("AttackDistance");
            Vector3 direction = (SourceFighter.transform.position - TargetFighters[0].transform.position).normalized;
            SourceFighter.transform.parent.DOMove(TargetFighters[0].transform.position + direction * attackDistance, readyDuration);
        });
        SourceFighter.AnimationEvent.SetOnEvent("attack", () =>
        {
            TargetFighters[0].Animator.SetTrigger("Attacked");
            Battle.DisplayDamage(TargetFighters[0], ConsumeImpact().number);
        });
    }
    public override void PostAct()
    {
        TargetFighters[0].AnimationEvent.SetOnEnd("attacked", () =>
        {
            TargetFighters[0].AnimationEvent.DoEvent("attackend");
            float backDuration = SourceFighter.AnimationEvent.GetFloat("ReadyDuration");
            SourceFighter.transform.DORotateQuaternion(SourceTransform.rotation, backDuration);
            SourceFighter.transform.parent.DOMove(SourceTransform.position, backDuration);
        });
        Battle.Reserve(this);
        Done();
    }
    protected override void Done()
    {
        isDone = true;
    }
}
