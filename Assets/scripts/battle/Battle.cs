﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;

[Serializable]
public struct FighterInfo
{
    public int StandPoint;
    public string Shape;
    public int HP;
}

public class Battle : MonoBehaviour 
{
    bool launched;
    bool ready;
    List<FighterInfo> fighterInfos;
    Dictionary<int, Creature> fighters = new Dictionary<int, Creature>();
    List<BattleAction> battleActions = new List<BattleAction>();
    List<BattleAction> reservedBattleActions = new List<BattleAction>();
    int currentBattlerIndex;
    public void Launch(List<FighterInfo> infos)
    {
        if (launched)
            return;

        fighterInfos = new List<FighterInfo>(infos);
        for (int i = 0; i < fighterInfos.Count; ++i)
        {
            Transform standTransform = GetStandPointTransform(fighterInfos[i].StandPoint);
            if (standTransform != null)
            {
                var position = new Vector2(standTransform.position.x, standTransform.position.z);
                CreatureManager.Instance.CreateAsync(fighterInfos[i].Shape, position, (creature) =>
                {
                    creature.Animator.SetTrigger("Enter");
                    creature.transform.rotation = standTransform.rotation;
                    fighters.Add(Int32.Parse(standTransform.name), creature);
                    ConstructBattler(creature, fighterInfos[currentBattlerIndex]);
                    ++currentBattlerIndex;
                });
            }
        }
        Camera.main.DOOrthoSize(7, 1.5f).SetEase(Ease.InOutBounce).OnComplete(() =>
            {
                ready = true;
            });
        launched = true;
    }
    void ConstructBattler(Creature creature, FighterInfo info)
    {
        var battler = new GameObject("battler" + info.StandPoint);
        battler.transform.position = creature.transform.position;
        creature.transform.position = Vector3.zero;
        creature.transform.SetParent(battler.transform, false);
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/elements/nametext", (asset) =>
        {
            var prefab = asset.assetObject as GameObject;
            var nametext = ResourceManager.Instance.CreateGameObject(prefab);
            nametext.transform.SetParent(battler.transform, false);
        });
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/elements/hpbar", (asset) =>
        {
            var prefab = asset.assetObject as GameObject;
            var hpbar = ResourceManager.Instance.CreateGameObject(prefab);
            hpbar.transform.SetParent(battler.transform, false);
        });
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/elements/damagetext", (asset) =>
        {
            var prefab = asset.assetObject as GameObject;
            var damagetext = ResourceManager.Instance.CreateGameObject(prefab);
            damagetext.name = "damagetext";
            damagetext.transform.SetParent(battler.transform, false);
        });
    }
    public void Execute(List<BattleAction> actions)
    {
        if (actions == null || actions.Count == 0)
            return;

        if (battleActions.Count == 0)
        {
            actions[0].Execute(this);
        }
        battleActions.AddRange(actions);
    }
    public void Reserve(BattleAction action)
    {
        if (action == null)
            return;

        reservedBattleActions.Add(action);
    }
    public Creature GetFighter(int standPoint)
    {
        return fighters[standPoint];
    }
    public Transform GetStandPointTransform(int standPoint)
    {
        Transform standTransform = GameObject.Find("buddystandpoints").transform.Find(standPoint.ToString());
        if (standTransform != null)
            return standTransform;
        else
        {
            standTransform = GameObject.Find("enemystandpoints").transform.Find(standPoint.ToString());
            return standTransform;
        }
    }
    public void DisplayDamage(Creature fighter, int number)
    {
        var damagetext = fighter.transform.parent.Find("damagetext");
        damagetext.GetComponent<TextMesh>().text = number.ToString();
        Vector3 initialPos = damagetext.localPosition;
        damagetext.DOMoveY(2, 1).SetEase(Ease.InOutElastic).OnComplete(() =>
        {
            damagetext.GetComponent<TextMesh>().text = string.Empty;
            damagetext.localPosition = initialPos;
        });
    }
    void Update()
    {
        if (!ready)
            return;

        if (battleActions.Count > 0 && battleActions[0].IsDone)
        {
            battleActions.RemoveAt(0);
            if (battleActions.Count > 0)
                battleActions[0].Execute(this);
            else
                reservedBattleActions.Clear();
        }
    }
}
