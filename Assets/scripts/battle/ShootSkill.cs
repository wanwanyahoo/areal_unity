﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using DG.Tweening;

public class ShootSkill : BattleAction 
{
    string effectName;
    int restStage;
    int currentTarget;
    int currentTargetIndex;
    int targetsPerStage;
    public ShootSkill() { }
    public ShootSkill(int sourcePoint, List<int> targetPoints, string trigger, int totalStage, List<BattleActionImpact> impacts, string effectName)
        : base(sourcePoint, targetPoints, trigger, totalStage, impacts)
    {
        this.effectName = effectName;
    }
    public override string ToString()
    {
        return "发射技能";
    }
    public override void OnGUI()
    {
#if UNITY_EDITOR
        base.OnGUI();
        effectName = EditorGUILayout.TextField("技能特效", effectName);
#endif
    }
    public override void PreAct()
    {
        currentTarget = 0;
        currentTargetIndex = 0;
        restStage = TotalStage;
        targetsPerStage = (int)Mathf.Ceil(TargetPoints.Count / (float)restStage);
        if (string.IsNullOrEmpty(Trigger))
        {
            SourceFighter.Animator.SetTrigger("Magic");
            SourceFighter.Animator.SetInteger("RestStage", restStage);
            SourceFighter.Animator.Update(0);
        }
    }
    public override void Act()
    {
        SourceFighter.AnimationEvent.SetOnEvent("magic", () =>
        {
            SourceFighter.Animator.SetInteger("RestStage", --restStage);
            for (int i = currentTarget; i < currentTarget + targetsPerStage; ++i)
            {
                if (i >= TargetFighters.Count)
                {
                    break;
                }
                EffectManager.Instance.CreateAsync(effectName, (effect) =>
                {
                    int targetPoint = TargetPoints[currentTargetIndex];
                    Creature targetFighter = Battle.GetFighter(targetPoint);
                    effect.name = targetPoint.ToString();
                    effect.transform.position = SourceFighter.transform.position;
                    effect.transform.DOMoveX(targetFighter.transform.position.x, 0.8f);
                    effect.transform.DOMoveZ(targetFighter.transform.position.z, 0.8f).SetEase(Ease.OutQuad);
                    effect.AnimationEvent.SetOnEvent("attack", () =>
                    {
                        targetFighter = Battle.GetFighter(int.Parse(effect.name));
                        targetFighter.Animator.SetTrigger("Attacked");
                        Battle.DisplayDamage(targetFighter, ConsumeImpact().number);
                    });
                    effect.AnimationEvent.SetOnEnd(effectName, () =>
                    {
                        EffectManager.Instance.Destroy(effect);
                        if (restStage == 0)
                            Done();
                    });
                    effect.Play();
                    ++currentTargetIndex;
                });
            }
            currentTarget += targetsPerStage;
            targetsPerStage = (int)Mathf.Ceil((TargetPoints.Count - currentTarget) / (float)restStage);
        });
    }
}
