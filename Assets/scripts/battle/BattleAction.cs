﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public struct BattleActionImpact
{
    public int number;
    public int type;
}
public abstract class BattleAction
{
    Battle battle;
    int sourcePoint;
    Transform sourceTransform;
    Creature sourceFighter;
    List<int> targetPoints = new List<int>();
    List<Transform> targetTransforms = new List<Transform>();
    List<Creature> targetFighters = new List<Creature>();
    string targetPointsString = string.Empty;
    string trigger;
    int totalStage;
    List<BattleActionImpact> impacts = new List<BattleActionImpact>();
    string impactsString = string.Empty;

    protected bool isDone;

    public Battle Battle { get { return battle; } }
    public int SourcePoint { get { return sourcePoint; } }
    public Transform SourceTransform { get { return sourceTransform; } }
    public Creature SourceFighter { get { return sourceFighter; } }
    public List<int> TargetPoints { get { return targetPoints; } }
    public List<Transform> TargetTransforms { get { return targetTransforms; } }
    public List<Creature> TargetFighters { get { return targetFighters; } }
    public string Trigger { get { return trigger; } }
    public int TotalStage { get { return totalStage; } }
    public bool IsDone { get { return isDone; } }
    public BattleAction() { }
    public BattleAction(int sourcePoint, List<int> targetPoints, string trigger, int totalStage, List<BattleActionImpact> impacts)
    {
        this.sourcePoint = sourcePoint;
        this.targetPoints = targetPoints;
        this.trigger = trigger;
        this.totalStage = totalStage;
        this.impacts = impacts;
    }
    public override string ToString()
    {
        //Must be overrided if a class inherits this one
        throw new System.NotImplementedException();
    }
    public virtual void OnGUI()
    {
#if UNITY_EDITOR
        sourcePoint = EditorGUILayout.IntField("发起", sourcePoint);
        targetPointsString = EditorGUILayout.TextField("目标", targetPointsString);
        trigger = EditorGUILayout.TextField("触发", trigger);
        totalStage = EditorGUILayout.IntField("阶段数", totalStage);
        impactsString = EditorGUILayout.TextField("影响", impactsString);
#endif
    }
    public void Execute(Battle battle)
    {
        this.battle = battle;
        isDone = false;
        if (!string.IsNullOrEmpty(targetPointsString))
        {
            targetPoints.Clear();
            string[] split = targetPointsString.Split(',');
            for (int i = 0; i < split.Length; ++i)
            {
                if (string.IsNullOrEmpty(split[i]))
                    break;

                targetPoints.Add(int.Parse(split[i]));
            }
        }
        if (!string.IsNullOrEmpty(impactsString))
        {
            impacts.Clear();
            string[] split = impactsString.Split(',');
            for (int i = 0; i < split.Length; ++i)
            {
                if (string.IsNullOrEmpty(split[i]))
                    break;

                var impact = new BattleActionImpact();
                impact.number = int.Parse(split[i]);
                impacts.Add(impact);
            }
        }
        if (targetPoints.Count == 0 || totalStage <= 0)
        {
            isDone = true;
            return;
        }
        sourceFighter = battle.GetFighter(sourcePoint);
        sourceTransform = battle.GetStandPointTransform(sourcePoint);
        targetFighters.Clear();
        targetTransforms.Clear();
        for (int i = 0; i < targetPoints.Count; ++i)
        {
            targetFighters.Add(battle.GetFighter(targetPoints[i]));
            targetTransforms.Add(battle.GetStandPointTransform(targetPoints[i]));
        }
        if (!string.IsNullOrEmpty(trigger))
        {
            SourceFighter.Animator.SetTrigger(trigger);
            SourceFighter.Animator.Update(0);
        }
        PreAct();
        Act();
        PostAct();
    }
    protected BattleActionImpact ConsumeImpact()
    {
        if (impacts.Count == 0)
            return new BattleActionImpact();

        BattleActionImpact result = impacts[0];
        impacts.RemoveAt(0);
        return result;
    }
    protected virtual void Done()
    {
        isDone = true;
        sourceFighter.AnimationEvent.Clear();
        for (int i = 0; i < targetFighters.Count; ++i)
            targetFighters[i].AnimationEvent.Clear();
    }
    public virtual void PreAct() { }
    public abstract void Act();
    public virtual void PostAct() { }
}
