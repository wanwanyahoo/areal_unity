﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour 
{
    public string Destination;
    void OnTriggerEnter(Collider collider)
    {
        MapLoader mapLoader = Game.GetRootComponent().MapLoader;
        mapLoader.Load(Destination, true);
    }
}
