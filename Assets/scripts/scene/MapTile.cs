﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTile : MonoBehaviour 
{
    Renderer shadeRenderer;
    bool loaded;

    [HideInInspector]
    public Renderer Renderer;
    [HideInInspector]
    public Camera Camera;
    [HideInInspector]
    public int MainTexID;
    [HideInInspector]
    public int ShadeTexID;
    [HideInInspector]
    public int ColorID;
    [HideInInspector]
    public Vector2 ExtraRegion = Vector2.one;
    public float DepthOffset;
    void Start()
    {
        Transform shade = transform.Find("shade");
        if (shade != null)
        {
            RebuildMesh(shade);
            shadeRenderer = shade.GetComponent<Renderer>();
        }
    }
    void RebuildMesh(Transform shade)
    {
        var meshFilter = shade.GetComponent<MeshFilter>();
        var vertices = new List<Vector3>(meshFilter.mesh.vertices);
        Vector3 v4 = Vector3.Lerp(vertices[2], vertices[1], DepthOffset);
        Vector3 v5 = Vector3.Lerp(vertices[0], vertices[3], DepthOffset);
        vertices.Add(v4);
        vertices.Add(v5);
        vertices[1] = new Vector3(vertices[1].x, v4.y, v4.y - vertices[1].y);
        vertices[3] = new Vector3(vertices[3].x, v5.y, v5.y - vertices[3].y);
        var triangles = new int[] { 0, 4, 2, 4, 0, 5, 5, 1, 4, 1, 5, 3 };
        var uvs = new List<Vector2>(meshFilter.mesh.uv);
        uvs.Add(new Vector2(1, DepthOffset));
        uvs.Add(new Vector2(0, DepthOffset));
        meshFilter.mesh.vertices = vertices.ToArray();
        meshFilter.mesh.triangles = triangles;
        meshFilter.mesh.uv = uvs.ToArray();
        meshFilter.mesh.UploadMeshData(true);
    }
    public void SelfUpdate()
    {
        Vector2 viewportPoint = Camera.WorldToViewportPoint(transform.position);
        if (Mathf.Abs(viewportPoint.x - 0.5f) < ExtraRegion.x && Mathf.Abs(viewportPoint.y - 0.5f) < ExtraRegion.y)
        {
            LoadTexture();
        }
        else
        {
            UnloadTexture();
        }
    }
    public void LoadTexture()
    {
        if (!loaded)
        {
            loaded = true;
            ResourceManager.Instance.LoadAsync<Texture2D>("textures/maps/" + transform.parent.name + "/ground/" + name + ".jpg", (groundAsset) =>
                {
                    var groundImage = groundAsset.assetObject as Texture2D;
                    Renderer.material.SetTexture(MainTexID, groundImage);
                    Renderer.material.SetColor(ColorID, groundImage ? Color.white : new Color(0, 0, 0, 0));
                    if (shadeRenderer != null)
                    {
                        ResourceManager.Instance.LoadAsync<Texture2D>("textures/maps/" + transform.parent.name + "/shade/" + name + ".jpg", (shadeAsset) =>
                        {
                            var shadeImage = shadeAsset.assetObject as Texture2D;
                            shadeRenderer.material.SetTexture(MainTexID, groundImage);
                            shadeRenderer.material.SetTexture(ShadeTexID, shadeImage);
                            shadeRenderer.material.SetColor(ColorID, shadeImage ? Color.white : new Color(0, 0, 0, 0));
                        });
                    }
                });
        }
    }
    public void UnloadTexture()
    {
        if (loaded)
        {
            loaded = false;
            Texture mainTex = Renderer.material.GetTexture(MainTexID);
            Renderer.material.SetTexture(MainTexID, null);
            Renderer.material.SetColor(ColorID, new Color(0, 0, 0, 0));
            ResourceManager.Instance.UnloadAsset(mainTex);
            if (shadeRenderer != null)
            {
                mainTex = shadeRenderer.material.GetTexture(MainTexID);
                Texture shadeTex = shadeRenderer.material.GetTexture(ShadeTexID);
                shadeRenderer.material.SetTexture(MainTexID, null);
                shadeRenderer.material.SetTexture(ShadeTexID, null);
                shadeRenderer.material.SetColor(ColorID, new Color(0, 0, 0, 0));
                ResourceManager.Instance.UnloadAsset(mainTex);
                ResourceManager.Instance.UnloadAsset(shadeTex);
            }
        }
    }
}
