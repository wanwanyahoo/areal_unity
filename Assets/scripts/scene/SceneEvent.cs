﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[XLua.LuaCallCSharp]
public class SceneEvent : MonoBehaviour 
{
    [XLua.CSharpCallLua]
    public delegate void TerrainClickEvent(Vector3 position);

    TerrainClickEvent onTerrainClick;
	public void OnClick() 
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            switch (hit.transform.tag)
            {
                case "Terrain":
                    if (onTerrainClick != null)
                        onTerrainClick(hit.point);
                    break;
            }
        }
	}
    public void SetOnTerrainClick(TerrainClickEvent action)
    {
        onTerrainClick = action;
    }
}
