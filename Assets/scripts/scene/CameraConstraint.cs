﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraConstraint : MonoBehaviour 
{
    Camera mainCamera;
    public Rect ConstraintRect;
    void Start()
    {
        mainCamera = Camera.main;
    }
	void LateUpdate () 
    {
        var nextPosition = mainCamera.transform.position;
        nextPosition.x = Mathf.Clamp(nextPosition.x, ConstraintRect.xMin, ConstraintRect.xMax);
        nextPosition.z = Mathf.Clamp(nextPosition.z, ConstraintRect.yMin, ConstraintRect.yMax);
        mainCamera.transform.position = nextPosition;
	}
}