﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[XLua.LuaCallCSharp]
public class Creature : MonoBehaviour 
{
    NavMeshAgent navMeshAgent;
    Animator animator;
    AnimationEvent animationEvent;
    public NavMeshAgent NavMeshAgent { get { return navMeshAgent; } }
    public Animator Animator { get { return animator; } }
    public AnimationEvent AnimationEvent { get { return animationEvent; } }
	public void SelfStart() 
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        if (navMeshAgent != null)
        {
            if (!navMeshAgent.enabled)
                navMeshAgent.enabled = true;
        }
        animator = GetComponent<Animator>();
        animationEvent = AnimationEvent.Get(gameObject);
    }
	public void SelfUpdate() 
    {
        if (animator != null && navMeshAgent != null)
            animator.SetFloat("Velocity", navMeshAgent.velocity.magnitude);
    }
    public void MoveTo(Vector3 dest)
    {
        if (navMeshAgent != null)
        {
            if (!navMeshAgent.enabled)
            {
                navMeshAgent.enabled = true;
            }
            navMeshAgent.destination = dest;
        }
    }

    public void Move(float x, float y)
    {
        if (navMeshAgent != null)
        {
            if (!navMeshAgent.enabled)
            {
                navMeshAgent.enabled = true;
            }
            navMeshAgent.SetDestination(new Vector3(x,navMeshAgent.destination.y, y));
        }
    }
    public Material GetMaterial()
    {
        return GetComponentInChildren<Renderer>().material;
    }
    public Material[] GetMaterials()
    {
        var renderers = GetComponentsInChildren<Renderer>();
        var materials = new Material[renderers.Length];
        for (int i = 0; i < renderers.Length; ++i)
        {
            materials[i] = renderers[i].material;
        }
        return materials;
    }
    public void SetMaterial(Material material)
    {
        var renderers = GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderers.Length; ++i)
        {
            var texture = renderers[i].material.GetTexture("_MainTex");
            renderers[i].material = material;
            renderers[i].material.SetTexture("_MainTex", texture);
        }
    }
    public void SetWeapon(Creature weapon)
    {
        var transforms = GetComponentsInChildren<Transform>();
        for (int i = 0; i < transforms.Length; ++i)
        {
            if (transforms[i].name == "HS_Weapon")
            {
                weapon.transform.SetParent(transforms[i], false);
                return;
            }
        }
    }

    public void SetMoveSpeed(float speed)
    {
        if (navMeshAgent != null)
        {
            navMeshAgent.speed = speed;
        }
    }

    public void StartMove()
    {
        if (navMeshAgent != null)
        {
            if (!navMeshAgent.enabled)
            {
                navMeshAgent.enabled = true;
            }
            navMeshAgent.isStopped = false;
        }
    }

    public void StopMove()
    {
        if (navMeshAgent != null)
        {
            if (!navMeshAgent.enabled)
            {
                navMeshAgent.enabled = true;
            }
            navMeshAgent.isStopped = true;
        }
    }

    public Vector3 GetCurPos()
    {
        if (navMeshAgent != null)
        {
            if (!navMeshAgent.enabled)
            {
                navMeshAgent.enabled = true;
            }
            return navMeshAgent.transform.position;
        }
        return new Vector3();
    }
}
