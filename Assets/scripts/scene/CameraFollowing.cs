﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[XLua.LuaCallCSharp]
public class CameraFollowing : MonoBehaviour 
{
    GameObject target;

    public Vector3 FollowOffset = new Vector3(0, 15, -25);
    void LateUpdate()
    {
        if (target != null)
        {
            this.transform.position = target.transform.position + FollowOffset;
        }
    }
    public void SetTarget(GameObject mainCharacter)
    {
        target = mainCharacter;
    }
    public GameObject GetTarget()
    {
        return target;
    }
}
