﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour 
{
    MapTile[] tiles;

    public Texture2D GroundImage;
    public Texture2D UnreachableImage;
    public Texture2D ShadeImage;
    public int TileSize;
    public int QuadScale;
    public bool Sliced;
    public int Row;
    public int Col;
    public bool Optimized;
    public bool SaveTerrain;
    void Start()
    {
        Camera mainCamera = Camera.main;
        int mainTexID = Shader.PropertyToID("_MainTex");
        int shadeTexID = Shader.PropertyToID("_ShadeTex");
        int colorID = Shader.PropertyToID("_Color");
        tiles = new MapTile[transform.childCount];
        for (int i = 0; i < transform.childCount; ++i)
        {
            tiles[i] = transform.GetChild(i).GetComponent<MapTile>();
            tiles[i].Renderer = tiles[i].GetComponent<Renderer>();
            tiles[i].Camera = mainCamera;
            tiles[i].MainTexID = mainTexID;
            tiles[i].ShadeTexID = shadeTexID;
            tiles[i].ColorID = colorID;
        }
    }
    void Update()
    {
        for (int i = 0; i < tiles.Length; ++i)
            tiles[i].SelfUpdate();
    }
    public void SetExtraRegion(Vector2 viewportOffset)
    {
        for (int i = 0; i < tiles.Length; ++i)
            tiles[i].ExtraRegion = viewportOffset;
    }
}