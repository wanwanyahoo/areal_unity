﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[XLua.CSharpCallLua]
public class MapLoader : MonoBehaviour 
{
    [XLua.CSharpCallLua]
    public delegate void OnLoadedEvent();

    RectTransform progressbarRect;
    void Start()
    {
        progressbarRect = this.transform.Find("Progressbar").GetComponent<RectTransform>();
        this.gameObject.SetActive(false);
    }
    public void Load(string scenePath, bool unloadActiveScene, OnLoadedEvent onLoaded = null)
    {
        if (unloadActiveScene)
        {
            SceneManager.Instance.UnloadActiveSceneAsync();
        }
        SceneManager.Instance.LoadAsync(scenePath, (scene) =>
            {
                if (onLoaded != null)
                {
                    onLoaded();
                }
                this.gameObject.SetActive(false);
            }, OnProcessing);
        this.gameObject.SetActive(true);
    }
    public void OnProcessing(float progress)
    {
        Vector3 scale = progressbarRect.localScale;
        scale.x = progress;
        progressbarRect.localScale = scale;
    }
}
