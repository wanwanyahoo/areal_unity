﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[XLua.LuaCallCSharp]
[AddComponentMenu("UI/Page Frame", 10)]
[SelectionBase]
[ExecuteInEditMode]
[DisallowMultipleComponent]
[RequireComponent(typeof(RectTransform))]
public class PageFrame : UIBehaviour
{
    [SerializeField]
    int m_TotalFrame;
    public int totalFrame { get { return m_TotalFrame; } set { SetTotalFrame(value); } }
    [SerializeField]
    int m_CurrentFrame;
    public int currentFrame { get { return m_CurrentFrame; } set { SetCurrentFrame(value); } }
    [SerializeField]
    Sprite m_UnselectedImage;
    public Sprite unselectedImage { get { return m_UnselectedImage; } set { SetUnselectedImage(value); } }
    [SerializeField]
    Sprite m_SelectedImage;
    public Sprite selectedImage { get { return m_SelectedImage; } set { SetSelectedImage(value); } }
    void SetTotalFrame(int totalFrame)
    {
        if (this.m_TotalFrame != totalFrame)
        {
            this.m_TotalFrame = totalFrame;
            RectTransform[] transforms = GetComponentsInChildren<RectTransform>();
            for (int i = 1; i < transforms.Length; ++i)
            {
#if UNITY_EDITOR
                DestroyImmediate(transforms[i].gameObject);
#else
                Destroy(transforms[i].gameObject);
#endif
            }
            for (int i = 0; i < totalFrame; ++i)
            {
                var image = new GameObject("Image");
                image.AddComponent<RectTransform>();
                image.AddComponent<Image>().sprite = m_UnselectedImage;
                image.transform.SetParent(this.transform);
            }
            SetCurrentFrame(m_CurrentFrame);
        }
    }
    void SetCurrentFrame(int currentFrame)
    {
        if (this.m_CurrentFrame != currentFrame)
        {
            this.m_CurrentFrame = currentFrame;
        }
        UpdateImage();
    }
    void SetUnselectedImage(Sprite unselectedImage)
    {
        this.m_UnselectedImage = unselectedImage;
        UpdateImage();
    }
    void SetSelectedImage(Sprite selectedImage)
    {
        this.m_SelectedImage = selectedImage;
        UpdateImage();
    }
    void UpdateImage()
    {
        RectTransform[] transforms = GetComponentsInChildren<RectTransform>();
        for (int i = 1; i < transforms.Length; ++i)
        {
            if (i == m_CurrentFrame)
                transforms[i].GetComponent<Image>().sprite = selectedImage;
            else
                transforms[i].GetComponent<Image>().sprite = unselectedImage;
        }
    }
}
