﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("UI/Close Button", 10)]
public class CloseButton : MonoBehaviour 
{
    public RectTransform SpecifiedUI;
    void Start()
    {
        if (SpecifiedUI != null)
        {
            UIEvent.Get(SpecifiedUI.gameObject).PointerClick = (eventData) =>
                {
                    UIManager.Instance.Destroy(this.gameObject.name);
                };
        }
    }
}
