﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using XLua;

[LuaCallCSharp]
public class UIEvent : EventTrigger 
{
    [CSharpCallLua]
    public delegate void PointerEvent(PointerEventData eventData);
    [CSharpCallLua]
    public delegate void LuaPointerEvent(LuaTable self, PointerEventData eventData);
    [CSharpCallLua]
    public delegate void BaseEvent(BaseEventData eventData);
    [CSharpCallLua]
    public delegate void LuaBaseEvent(LuaTable self, BaseEventData eventData);
    [CSharpCallLua]
    public delegate void AxisEvent(AxisEventData eventData);
    [CSharpCallLua]
    public delegate void LuaAxisEvent(LuaTable self, AxisEventData eventData);

    public PointerEvent PointerEnter;
    public void SetPointerEnter(PointerEvent pointerEvent)
    {
        this.PointerEnter = pointerEvent;
    }
    public void SetPointerEnter(LuaPointerEvent pointerEvent, LuaTable self)
    {
        PointerEnter = (eventData) =>
            {
                pointerEvent(self, eventData);
            };
    }
    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (PointerEnter != null)
            PointerEnter(eventData);
    }

    public PointerEvent PointerExit;
    public void SetPointerExit(PointerEvent pointerEvent)
    {
        this.PointerExit = pointerEvent;
    }
    public void SetPointerExit(LuaPointerEvent pointerEvent, LuaTable self)
    {
        PointerExit = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnPointerExit(PointerEventData eventData)
    {
        if (PointerExit != null)
            PointerExit(eventData);
    }

    public PointerEvent PointerDown;
    public void SetPointerDown(PointerEvent pointerEvent)
    {
        this.PointerDown = pointerEvent;
    }
    public void SetPointerDown(LuaPointerEvent pointerEvent, LuaTable self)
    {
        PointerDown = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (PointerDown != null)
            PointerDown(eventData);
    }

    public PointerEvent PointerUp;
    public void SetPointerUp(PointerEvent pointerEvent)
    {
        this.PointerUp = pointerEvent;
    }
    public void SetPointerUp(LuaPointerEvent pointerEvent, LuaTable self)
    {
        PointerUp = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnPointerUp(PointerEventData eventData)
    {
        if (PointerUp != null)
            PointerUp(eventData);
    }

    public PointerEvent PointerClick;
    public void SetPointerClick(PointerEvent pointerEvent)
    {
        this.PointerClick = pointerEvent;
    }
    public void SetPointerClick(LuaPointerEvent pointerEvent, LuaTable self)
    {
        PointerClick = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnPointerClick(PointerEventData eventData)
    {
        if (PointerClick != null)
            PointerClick(eventData);
    }

    public PointerEvent BeginDrag;
    public void SetBeginDrag(PointerEvent pointerEvent)
    {
        this.BeginDrag = pointerEvent;
    }
    public void SetBeginDrag(LuaPointerEvent pointerEvent, LuaTable self)
    {
        BeginDrag = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        if (BeginDrag != null)
            BeginDrag(eventData);
    }

    public PointerEvent Drag;
    public void SetDrag(PointerEvent pointerEvent)
    {
        this.Drag = pointerEvent;
    }
    public void SetDrag(LuaPointerEvent pointerEvent, LuaTable self)
    {
        Drag = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnDrag(PointerEventData eventData)
    {
        if (Drag != null)
            Drag(eventData);
    }

    public PointerEvent EndDrag;
    public void SetEndDrag(PointerEvent pointerEvent)
    {
        this.EndDrag = pointerEvent;
    }
    public void SetEndDrag(LuaPointerEvent pointerEvent, LuaTable self)
    {
        EndDrag = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnEndDrag(PointerEventData eventData)
    {
        if (EndDrag != null)
            EndDrag(eventData);
    }

    public PointerEvent Drop;
    public void SetDrop(PointerEvent pointerEvent)
    {
        this.Drop = pointerEvent;
    }
    public void SetDrop(LuaPointerEvent pointerEvent, LuaTable self)
    {
        Drop = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnDrop(PointerEventData eventData)
    {
        if (Drop != null)
            Drop(eventData);
    }

    public PointerEvent Scroll;
    public void SetScroll(PointerEvent pointerEvent)
    {
        this.Scroll = pointerEvent;
    }
    public void SetScroll(LuaPointerEvent pointerEvent, LuaTable self)
    {
        Scroll = (eventData) =>
        {
            pointerEvent(self, eventData);
        };
    }
    public override void OnScroll(PointerEventData eventData)
    {
        if (Scroll != null)
            Scroll(eventData);
    }

    public BaseEvent UpdateSelected;
    public void SetUpdateSelected(BaseEvent baseEvent)
    {
        this.UpdateSelected = baseEvent;
    }
    public void SetUpdateSelected(LuaBaseEvent baseEvent, LuaTable self)
    {
        UpdateSelected = (eventData) =>
        {
            baseEvent(self, eventData);
        };
    }
    public override void OnUpdateSelected(BaseEventData eventData)
    {
        if (UpdateSelected != null)
            UpdateSelected(eventData);
    }

    public BaseEvent Select;
    public void SetSelect(BaseEvent baseEvent)
    {
        this.Select = baseEvent;
    }
    public void SetSelect(LuaBaseEvent baseEvent, LuaTable self)
    {
        Select = (eventData) =>
        {
            baseEvent(self, eventData);
        };
    }
    public override void OnSelect(BaseEventData eventData)
    {
        if (Select != null)
            Select(eventData);
    }

    public BaseEvent Deselect;
    public void SetDeselect(BaseEvent baseEvent)
    {
        this.Deselect = baseEvent;
    }
    public void SetDeselect(LuaBaseEvent baseEvent, LuaTable self)
    {
        Deselect = (eventData) =>
        {
            baseEvent(self, eventData);
        };
    }
    public override void OnDeselect(BaseEventData eventData)
    {
        if (Deselect != null)
            Deselect(eventData);
    }

    public AxisEvent Move;
    public void SetMove(AxisEvent axisEvent)
    {
        this.Move = axisEvent;
    }
    public void SetMove(LuaAxisEvent axisEvent, LuaTable self)
    {
        Move = (eventData) =>
        {
            axisEvent(self, eventData);
        };
    }
    public override void OnMove(AxisEventData eventData)
    {
        if (Move != null)
            Move(eventData);
    }

    public BaseEvent Submit;
    public void SetSubmit(BaseEvent baseEvent)
    {
        this.Submit = baseEvent;
    }
    public void SetSubmit(LuaBaseEvent baseEvent, LuaTable self)
    {
        Submit = (eventData) =>
        {
            baseEvent(self, eventData);
        };
    }
    public override void OnSubmit(BaseEventData eventData)
    {
        if (Submit != null)
            Submit(eventData);
    }

    public BaseEvent Cancel;
    public void SetCancel(BaseEvent baseEvent)
    {
        this.Cancel = baseEvent;
    }
    public void SetCancel(LuaBaseEvent baseEvent, LuaTable self)
    {
        Cancel = (eventData) =>
        {
            baseEvent(self, eventData);
        };
    }
    public override void OnCancel(BaseEventData eventData)
    {
        if (Cancel != null)
            Cancel(eventData);
    }

    void OnDestroy()
    {
        PointerClick = null;
        PointerDown = null;
        PointerEnter = null;
        PointerExit = null;
        PointerUp = null;
        BeginDrag = null;
        Drag = null;
        EndDrag = null;
        Drop = null;
        Scroll = null;
        UpdateSelected = null;
        Select = null;
        Deselect = null;
        Move = null;
        Submit = null;
        Cancel = null;
    }

    public static UIEvent Get(GameObject ui)
    {
        var listener = ui.GetComponent<UIEvent>();
        if (listener == null)
            listener = ui.AddComponent<UIEvent>();

        return listener;
    }
}
