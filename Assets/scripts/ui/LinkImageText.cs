﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[XLua.LuaCallCSharp]
[AddComponentMenu("UI/LinkImage Text", 10)]
public class LinkImageText : Text, IPointerClickHandler
{
    const string HrefColor = "blue";
    class HrefInfo
    {
        public int startIndex;
        public int endIndex;
        public string name;
        public readonly List<Rect> boxes = new List<Rect>();
    }
    static readonly Regex ImageRegex = new Regex(@"<quad name=(.+?) size=(\d*\.?\d+%?)/>", RegexOptions.Singleline);
    static readonly Regex HrefRegex = new Regex(@"<a href=([^>\n\s]+)>(.*?)(</a>)", RegexOptions.Singleline);
    static readonly StringBuilder TextBuilder = new StringBuilder();

    [XLua.CSharpCallLua]
    public delegate void HrefClickEvent(string href);
    [XLua.CSharpCallLua]
    public delegate void LuaHrefClickEvent(XLua.LuaTable self, string href);

    readonly List<Image> ImagesPool = new List<Image>();
    readonly List<int> ImagesVertexIndex = new List<int>();
    readonly List<HrefInfo> HrefInfos = new List<HrefInfo>();
    string outputText;
    HrefClickEvent onHrefClick;

    public Sprite[] SpriteAtlas;
    public override void SetVerticesDirty()
    {
        base.SetVerticesDirty();
#if UNITY_EDITOR
        if (UnityEditor.PrefabUtility.GetPrefabType(this) == UnityEditor.PrefabType.Prefab)
        {
            return;
        }
#endif
        outputText = GetOutputText(text);
        ImagesVertexIndex.Clear();
        foreach (Match match in ImageRegex.Matches(outputText))
        {
            var picIndex = match.Index;
            var endIndex = picIndex * 4 + 3;
            ImagesVertexIndex.Add(endIndex);

            ImagesPool.RemoveAll(image => image == null);
            if (ImagesPool.Count == 0)
            {
                GetComponentsInChildren<Image>(ImagesPool);
            }
            if (ImagesVertexIndex.Count > ImagesPool.Count)
            {
                var resources = new DefaultControls.Resources();
                var go = DefaultControls.CreateImage(resources);
                go.layer = gameObject.layer;
                var rt = go.transform as RectTransform;
                if (rt)
                {
                    rt.SetParent(rectTransform);
                    rt.localPosition = Vector3.zero;
                    rt.localRotation = Quaternion.identity;
                    rt.localScale = Vector3.one;
                }
                ImagesPool.Add(go.GetComponent<Image>());
            }
            var spriteName = match.Groups[1].Value;
            var size = float.Parse(match.Groups[2].Value);
            var img = ImagesPool[ImagesVertexIndex.Count - 1];
            if ((img.sprite == null || img.sprite.name != spriteName) && SpriteAtlas != null)
            {
                for (int i = 0; i < SpriteAtlas.Length; ++i)
                {
                    if (SpriteAtlas[i].name == spriteName)
                    {
                        img.sprite = SpriteAtlas[i];
                        break;
                    }
                }
            }
            img.rectTransform.sizeDelta = new Vector2(size, size);
            img.enabled = true;
        }
        for (var i = ImagesVertexIndex.Count; i < ImagesPool.Count; i++)
        {
            if (ImagesPool[i])
                ImagesPool[i].enabled = false;
        }
    }
    protected override void OnPopulateMesh(VertexHelper toFill)
    {
        var orignText = m_Text;
        m_Text = outputText;
        base.OnPopulateMesh(toFill);
        m_Text = orignText;

        UIVertex vert = new UIVertex();
        for (var i = 0; i < ImagesVertexIndex.Count; i++)
        {
            var endIndex = ImagesVertexIndex[i];
            var rt = ImagesPool[i].rectTransform;
            var size = rt.sizeDelta;
            if (endIndex < toFill.currentVertCount)
            {
                toFill.PopulateUIVertex(ref vert, endIndex);
                rt.anchoredPosition = new Vector2(vert.position.x + size.x / 2, vert.position.y + size.y / 2);

                // 抹掉左下角的小黑点
                toFill.PopulateUIVertex(ref vert, endIndex - 3);
                var pos = vert.position;
                for (int j = endIndex, m = endIndex - 3; j > m; j--)
                {
                    toFill.PopulateUIVertex(ref vert, endIndex);
                    vert.position = pos;
                    toFill.SetUIVertex(vert, j);
                }
            }
        }
        if (ImagesVertexIndex.Count != 0)
        {
            ImagesVertexIndex.Clear();
        }
        // 处理超链接包围框
        foreach (var hrefInfo in HrefInfos)
        {
            hrefInfo.boxes.Clear();
            if (hrefInfo.startIndex >= toFill.currentVertCount)
            {
                continue;
            }
            toFill.PopulateUIVertex(ref vert, hrefInfo.startIndex);
            var pos = vert.position;
            var bounds = new Bounds(pos, Vector3.zero);
            for (int i = hrefInfo.startIndex, m = hrefInfo.endIndex; i < m; i++)
            {
                if (i >= toFill.currentVertCount)
                {
                    break;
                }
                toFill.PopulateUIVertex(ref vert, i);
                pos = vert.position;
                if (pos.x < bounds.min.x)
                {
                    hrefInfo.boxes.Add(new Rect(bounds.min, bounds.size));
                    bounds = new Bounds(pos, Vector3.zero);
                }
                else
                {
                    bounds.Encapsulate(pos);
                }
            }
            hrefInfo.boxes.Add(new Rect(bounds.min, bounds.size));
        }
    }
    protected virtual string GetOutputText(string outputText)
    {
        TextBuilder.Length = 0;
        HrefInfos.Clear();
        var indexText = 0;
        foreach (Match match in HrefRegex.Matches(outputText))
        {
            TextBuilder.Append(outputText.Substring(indexText, match.Index - indexText));
            TextBuilder.Append("<color=" + HrefColor + ">");

            var group = match.Groups[1];
            var hrefInfo = new HrefInfo
            {
                startIndex = TextBuilder.Length * 4, 
                endIndex = (TextBuilder.Length + match.Groups[2].Length - 1) * 4 + 3,
                name = group.Value
            };
            HrefInfos.Add(hrefInfo);

            TextBuilder.Append(match.Groups[2].Value);
            TextBuilder.Append("</color>");
            indexText = match.Index + match.Length;
        }
        TextBuilder.Append(outputText.Substring(indexText, outputText.Length - indexText));
        return TextBuilder.ToString();
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Vector2 lp;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out lp);
        foreach (var hrefInfo in HrefInfos)
        {
            var boxes = hrefInfo.boxes;
            for (var i = 0; i < boxes.Count; ++i)
            {
                if (boxes[i].Contains(lp))
                {
                    if (onHrefClick != null)
                        onHrefClick(hrefInfo.name);

                    return;
                }
            }
        }
    }
    public void SetHrefClick(HrefClickEvent hrefClickEvent)
    {
        this.onHrefClick = hrefClickEvent;
    }
    public void SetHrefClick(LuaHrefClickEvent hrefClickEvent, XLua.LuaTable self)
    {
        this.onHrefClick = (href) => { hrefClickEvent(self, href); };
    }
}
