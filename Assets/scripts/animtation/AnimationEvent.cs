﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[XLua.LuaCallCSharp]
public class AnimationEvent : MonoBehaviour 
{
    [XLua.CSharpCallLua]
    public delegate void OnEvent();
    [XLua.CSharpCallLua]
    public delegate void LuaOnEvent(XLua.LuaTable self);

    Animator animator;
    OnEvent onStart;
    OnEvent onEnd;
    Dictionary<string, OnEvent> events = new Dictionary<string, OnEvent>();
    Dictionary<string, float> sets = new Dictionary<string, float>();
    public AnimationClip FindAnimationClip(string clipName)
    {
        var animationclips = animator.runtimeAnimatorController.animationClips;
        for (int i = 0; i < animationclips.Length; ++i)
        {
            if (animationclips[i].name == clipName)
            {
                return animationclips[i];
            }
        }
        return null;
    }
    public bool EventExist(AnimationClip clip, string functionName)
    {
        for (int i = 0; i < clip.events.Length; ++i)
        {
            if (clip.events[i].functionName == functionName)
                return true;
        }
        return false;
    }
    public void SetOnEnd(string clipName, OnEvent onEnd)
    {
        this.onEnd = onEnd;
        AnimationClip animationClip = FindAnimationClip(clipName);
        if (!EventExist(animationClip, "OnEnd"))
        {
            var animationEvent = new UnityEngine.AnimationEvent();
            animationEvent.functionName = "OnEnd";
            animationEvent.time = animationClip.length;
            animationClip.AddEvent(animationEvent);
        }
    }
    public void SetOnStart(string clipName, OnEvent onStart)
    {
        this.onStart = onStart;
        AnimationClip animationClip = FindAnimationClip(clipName);
        if (!EventExist(animationClip, "OnStart"))
        {
            var animationEvent = new UnityEngine.AnimationEvent();
            animationEvent.functionName = "OnStart";
            animationEvent.time = 0;
            animationClip.AddEvent(animationEvent);
        }
    }
    public void SetOnEnd(string clipName, LuaOnEvent onEnd, XLua.LuaTable self)
    {
        SetOnEnd(clipName, () => { onEnd(self); });
    }
    public void SetOnStart(string clipName, LuaOnEvent onStart, XLua.LuaTable self)
    {
        SetOnStart(clipName, () => { onStart(self); });
    }
    public void SetOnEvent(string label, OnEvent onEvent)
    {
        events[label] = onEvent;
    }
    public void SetOnEvent(string label, LuaOnEvent onEvent, XLua.LuaTable self)
    {
        SetOnEvent(label, () => { onEvent(self); });
    }
    public void DoEvent(string label)
    {
        if (events.ContainsKey(label))
            events[label]();
    }
    public void Set(string str)
    {
        string[] split = str.Split('=');
        if (split.Length == 2)
            sets[split[0].Trim()] = float.Parse(split[1].Trim());
    }
    public float GetFloat(string key)
    {
        if (sets.ContainsKey(key))
            return sets[key];
        else
            return animator.GetFloat(key);
    }
    public void NewEvent(string label)
    {
        if (events.ContainsKey(label))
            events[label]();
    }
    public void OnStart()
    {
        if (onStart != null)
            onStart();
    }
    public void OnEnd()
    {
        if (onEnd != null)
            onEnd();
    }
    public void Clear()
    {
        onStart = null;
        onEnd = null;
        events.Clear();
        sets.Clear();
    }

    void OnDestroy()
    {
        Clear();
    }

    public static AnimationEvent Get(GameObject gameObject)
    {
        var listener = gameObject.GetComponent<AnimationEvent>();
        if (listener == null)
        {
            listener = gameObject.AddComponent<AnimationEvent>();
        }
        listener.animator = gameObject.GetComponent<Animator>();
        return listener;
    }
}
