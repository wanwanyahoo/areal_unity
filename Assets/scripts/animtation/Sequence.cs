﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Tweening
{
    [XLua.LuaCallCSharp]
    public class Sequence
    {
        [XLua.CSharpCallLua]
        public delegate void LuaTweenCallback(XLua.LuaTable self);

        DG.Tweening.Sequence sequence;
        public Sequence()
        {
            sequence = DG.Tweening.DOTween.Sequence();
        }
        public void Append(Tweener tweener)
        {
            sequence.Append(tweener.Get());
        }
        public void Prepend(Tweener tweener)
        {
            sequence.Prepend(tweener.Get());
        }
        public void AppendInterval(float interval)
        {
            sequence.AppendInterval(interval);
        }
        public void PrependInterval(float interval)
        {
            sequence.PrependInterval(interval);
        }
        public void AppendCallback(LuaTweenCallback callback, XLua.LuaTable self)
        {
            sequence.AppendCallback(() => { callback(self); });
        }
    }
}