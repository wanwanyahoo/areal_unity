﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MyTweener = Tweening.Tweener;

[XLua.LuaCallCSharp]
public class Tween
{
    public static MyTweener DOMove(Transform transform, Vector3 endValue, float duration)
    {
        return new MyTweener(transform.DOMove(endValue, duration));
    }
    public static MyTweener DOMoveX(Transform transform, float endValue, float duration)
    {
        return new MyTweener(transform.DOMoveX(endValue, duration));
    }
    public static MyTweener DOMoveY(Transform transform, float endValue, float duration)
    {
        return new MyTweener(transform.DOMoveY(endValue, duration));
    }
    public static MyTweener DOMoveZ(Transform transform, float endValue, float duration)
    {
        return new MyTweener(transform.DOMoveZ(endValue, duration));
    }
    public static MyTweener DOMove(RectTransform transform, Vector2 endValue, float duration)
    {
        return new MyTweener(transform.DOAnchorPos(endValue, duration));
    }
    public static MyTweener DOMoveX(RectTransform transform, float endValue, float duration)
    {
        return new MyTweener(transform.DOAnchorPosX(endValue, duration));
    }
    public static MyTweener DOMoveY(RectTransform transform, float endValue, float duration)
    {
        return new MyTweener(transform.DOAnchorPosY(endValue, duration));
    }
    public static MyTweener DORotate(Transform transform, Vector3 endValue, float duration)
    {
        return new MyTweener(transform.DORotate(endValue, duration));
    }
    public static MyTweener DORotateQuaternion(Transform transform, Quaternion endValue, float duration)
    {
        return new MyTweener(transform.DORotateQuaternion(endValue, duration));
    }
    public static MyTweener DOScale(Transform transform, Vector3 endValue, float duration)
    {
        return new MyTweener(transform.DOScale(endValue, duration));
    }
    public static MyTweener DOLookAt(Transform transform, Vector3 endValue, float duration)
    {
        return new MyTweener(transform.DOLookAt(endValue, duration));
    }   
}
