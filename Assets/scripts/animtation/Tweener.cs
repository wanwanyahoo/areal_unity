﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Tweening
{
    [XLua.LuaCallCSharp]
    public class Tweener
    {
        public enum Ease
        {
            Unset = 0,
            Linear = 1,
            InSine = 2,
            OutSine = 3,
            InOutSine = 4,
            InQuad = 5,
            OutQuad = 6,
            InOutQuad = 7,
            InCubic = 8,
            OutCubic = 9,
            InOutCubic = 10,
            InQuart = 11,
            OutQuart = 12,
            InOutQuart = 13,
            InQuint = 14,
            OutQuint = 15,
            InOutQuint = 16,
            InExpo = 17,
            OutExpo = 18,
            InOutExpo = 19,
            InCirc = 20,
            OutCirc = 21,
            InOutCirc = 22,
            InElastic = 23,
            OutElastic = 24,
            InOutElastic = 25,
            InBack = 26,
            OutBack = 27,
            InOutBack = 28,
            InBounce = 29,
            OutBounce = 30,
            InOutBounce = 31,
            Flash = 32,
            InFlash = 33,
            OutFlash = 34,
            InOutFlash = 35
        }
        [XLua.CSharpCallLua]
        public delegate void LuaTweenCallback(XLua.LuaTable self);

        DG.Tweening.Tweener tweener;
        public Tweener(DG.Tweening.Tweener tweener)
        {
            this.tweener = tweener;
        }
        public DG.Tweening.Tweener Get()
        {
            return tweener;
        }
        public void SetLoops(int loops)
        {
            tweener.SetLoops(loops);
        }
        public void SetEase(Ease ease)
        {
            tweener.SetEase((DG.Tweening.Ease)ease);
        }
        public void SetRelative()
        {
            tweener.SetRelative();
        }
        public void Play()
        {
            tweener.Play();
        }
        public void Pause()
        {
            tweener.Pause();
        }
        public void OnComplete(LuaTweenCallback callback, XLua.LuaTable self)
        {
            tweener.OnComplete(() => { callback(self); });
        }
    }
}