﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

[LuaCallCSharp]
public class Game : SingletonManager
{
    public static Game Instance;

    [CSharpCallLua]
    public delegate void LuaGameDelegate();
    private LuaGameDelegate luaGameStart;
    private LuaGameDelegate luaGameUpdate;

    public MapLoader MapLoader { get; private set; }
    public static Game GetRootComponent()
    {
        return GetRootObject().GetComponent<Game>();
    }
    void Awake()
    {
        Application.targetFrameRate = 60;
        Instance = this;

        base.Init();
        //初始化所有Manager
        SingletonManager.InitSingleton<LuaManager>();
        SingletonManager.InitSingleton<LimaxManager>();
        SingletonManager.InitSingleton<ResourceManager>();
        SingletonManager.InitSingleton<SceneManager>();
        SingletonManager.InitSingleton<CreatureManager>();
        SingletonManager.InitSingleton<EffectManager>();
        SingletonManager.InitSingleton<UIManager>();
       
        //执行Lua游戏逻辑
        LuaManager.Instance.Execute("require \"lua.main\"");
        luaGameStart = LuaManager.Instance.Env.Global.Get<LuaTable>("LuaGame").Get<LuaGameDelegate>("Start");
        luaGameUpdate = LuaManager.Instance.Env.Global.Get<LuaTable>("LuaGame").Get<LuaGameDelegate>("Update");
    }
    void Start()
    {
        MapLoader = UIManager.Instance.Create("loadingui", true).GetComponent<MapLoader>();
        //执行Lua Start
        luaGameStart();
    }
    void Update()
    {
        //运行时更新所有Manager
        SingletonManager.UpdateSingletons();
        //执行Lua Update
        luaGameUpdate();
    }
    void OnApplicationQuit()
    {
        luaGameStart = null;
        luaGameUpdate = null;
        //退出时释放所有Manager
        SingletonManager.ReleaseSingletons();
    }
}
