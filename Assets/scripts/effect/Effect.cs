﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[XLua.LuaCallCSharp]
public class Effect : MonoBehaviour 
{
    Animator animator;
    AnimationEvent animationEvent;
    public Animator Animator { get { return animator; } }
    public AnimationEvent AnimationEvent { get { return animationEvent; } }
    public void SelfStart()
    {
        animator = GetComponent<Animator>();
        animationEvent = AnimationEvent.Get(gameObject);
        Stop();
    }
    public void Play()
    {
        Stop();
        this.gameObject.SetActive(true);
    }
    public void Stop()
    {
        this.gameObject.SetActive(false);
    }
}
