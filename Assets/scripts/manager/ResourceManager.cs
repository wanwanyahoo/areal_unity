﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[XLua.CSharpCallLua]
public delegate void LoadTaskCallBack(GameAsset gameAsset);

public class LoadTask
{
    public enum TaskStatus
    {
        NoProcess,
        Processing,
        Finished
    }

    private string mAssetPath;
    private Type mAssetType;
    private bool mWithSubAssets;
    private TaskStatus mStatus;
    private bool mInBundle;
    private LoadTaskCallBack mFinishCallBack;

    public string assetPath
    {
        get { return mAssetPath; }
        set { mAssetPath = value; }
    }
    public Type assetType
    {
        get { return mAssetType; }
        set { mAssetType = value; }
    }
    public bool WithSubAssets
    {
        get { return mWithSubAssets; }
        set { mWithSubAssets = value; }
    }
    public TaskStatus status
    {
        get { return mStatus; }
        set { mStatus = value; }
    }
    public LoadTaskCallBack finishCallBackFunc
    {
        get { return mFinishCallBack; }
        set { mFinishCallBack = value; }
    }
    public LoadTask(string assetPath, Type assetType, bool withSubAssets, LoadTaskCallBack func)
    {
        mAssetPath = assetPath;
        mAssetType = assetType;
        mWithSubAssets = withSubAssets;
        mStatus = TaskStatus.NoProcess;
        mFinishCallBack = func;
    }
}

[XLua.LuaCallCSharp]
public class ResourceManager : Singleton<ResourceManager>
{
    const string TextureFolder = "textures/";
    const string MaterialFolder = "materials/";
    const string SpriteAtlasFolder = "imagesets/";
    
    [XLua.CSharpCallLua]
    public delegate void LuaOnLoadTexutreEvent(XLua.LuaTable self, Texture texture, XLua.LuaTable args);
    [XLua.CSharpCallLua]
    public delegate void LuaOnLoadMaterialEvent(XLua.LuaTable self, Material material, XLua.LuaTable args);

    AssetBundleManifest manifest;
    List<LoadTask> loadTaskList = new List<LoadTask>();
    List<GameAsset> gameAssetList = new List<GameAsset>();
    Dictionary<GameObject, Object> prefabMap = new Dictionary<GameObject, Object>();
    Dictionary<string, Texture> textureCache = new Dictionary<string, Texture>();
    Dictionary<string, Material> materialCache = new Dictionary<string, Material>();
    Dictionary<string, Sprite[]> spriteAtlasCache = new Dictionary<string, Sprite[]>();
    public override void Init()
    {
        LoadManifest();
        StartCoroutine(ProcessLoadTask());
    }
    void LoadManifest()
    {
        string manifestBundleName = "StreamingAssets";
#if UNITY_EDITOR
        if (Directory.Exists(PathHelper.GetAssetsOutterPath()))
        {
            string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, false), manifestBundleName);
            //UnityEngine.Debug.Log("Load game asset at outter path:" + outterFilePath);
            AssetBundle manifestBundle = AssetBundle.LoadFromFile(outterFilePath);
            if (manifestBundle != null)
            {
                manifest = (AssetBundleManifest)manifestBundle.LoadAsset("AssetBundleManifest");
                manifestBundle.Unload(false);
            }
            else
            {
                //UnityEngine.Debug.Log("Load game asset failed:resources");
            }
        }
#else
        string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, false), manifestBundleName);
        UnityEngine.Debug.Log("Load game asset at outter path:" + outterFilePath);
        AssetBundle manifestBundle = AssetBundle.LoadFromFile(outterFilePath);
        if (manifestBundle == null)
        {
            string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(true, false), manifestBundleName);
            UnityEngine.Debug.Log("Load game asset at inner path:" + innerFilePath);
            manifestBundle = AssetBundle.LoadFromFile(innerFilePath);
        }
        if (manifestBundle != null)
        {
            manifest = (AssetBundleManifest)manifestBundle.LoadAsset("AssetBundleManifest");
            manifestBundle.Unload(false);
        }
        else
        {
            UnityEngine.Debug.Log("Load game asset failed:resources");
        }
#endif
    }
    IEnumerator ProcessLoadTask()
    {
        while (true)
        {
            if (loadTaskList == null || loadTaskList.Count == 0)
            {
                yield return null;
            }
            // start every new load task by async
            for (int index = loadTaskList.Count - 1; index >= 0; index--)
            {
                LoadTask loadTask = (LoadTask)loadTaskList[index];
                if (loadTask.status == LoadTask.TaskStatus.Finished)
                {
                    loadTaskList.RemoveAt(index);
                }
                else if (loadTask.status == LoadTask.TaskStatus.NoProcess)
                {
                    yield return StartCoroutine(ExecuteLoadTask(loadTask));
                }
            }
            yield return null;
        }
    }
    // load game asset as async by coroutine
    IEnumerator ExecuteLoadTask(LoadTask loadTask)
    {
        loadTask.status = LoadTask.TaskStatus.Processing;
#if UNITY_EDITOR
        if (!Directory.Exists(PathHelper.GetAssetsOutterPath()))
        {
            GameAsset gameAsset = new GameAsset();
            string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(), loadTask.assetPath);
            //UnityEngine.Debug.Log("Load game asset at inner path:" + innerFilePath);
            if (File.Exists(innerFilePath))
            {
                //UnityEngine.Debug.Log("Load game asset succeed:" + loadTask.assetPath);
                gameAsset.assetPath = loadTask.assetPath;
                if (!loadTask.WithSubAssets)
                {
                    gameAsset.assetObject = AssetDatabase.LoadAssetAtPath(innerFilePath, loadTask.assetType);
                }
                else
                {
                    gameAsset.assetObjects = AssetDatabase.LoadAllAssetsAtPath(innerFilePath);
                }
            }
            else
            {
                //UnityEngine.Debug.Log("Load game asset failed:" + loadTask.assetPath);
            }
            loadTask.finishCallBackFunc(gameAsset);
        }
        else
        {
            string[] depends = manifest.GetAllDependencies(loadTask.assetPath);
            if (!loadTask.WithSubAssets)
            {
                for (int index = 0; index < depends.Length; index++)
                {
                    yield return StartCoroutine(LoadGameAsset(depends[index], true, false));
                }
            }
            yield return StartCoroutine(LoadGameAsset(loadTask.assetPath, false, loadTask.WithSubAssets, depends, loadTask.finishCallBackFunc));
        }
#else
        string[] depends = manifest.GetAllDependencies(loadTask.assetPath);
        if (!loadTask.WithSubAssets)
        {
            for (int index = 0; index < depends.Length; index++)
            {
                yield return StartCoroutine(LoadGameAsset(depends[index], true, false));
            }
        }
        yield return StartCoroutine(LoadGameAsset(loadTask.assetPath, false, loadTask.WithSubAssets, depends, loadTask.finishCallBackFunc));
#endif
        loadTask.status = LoadTask.TaskStatus.Finished;
        yield break;
    }

    // load game asset not collect depends
    IEnumerator LoadGameAsset(string assetPath, bool loadBundleOnly, bool loadAll, string[] depends = null, LoadTaskCallBack finishCallBackFunc = null)
    {
        string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, true), assetPath);
        string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(true, true), assetPath);
        string assetName = Path.GetFileName(assetPath);
        GameAsset gameAsset = GetGameAsset(assetPath);
        WWW www;
        // first find if the asset has been load in memory.(maybe some coroutine has started load that but not finish)
        if (gameAsset != null)
        {
            while (gameAsset.refUseCnt == 0)
            {
                yield return null;
            }
            if (!string.IsNullOrEmpty(assetName) && gameAsset.assetObject == null)
            {
                AssetBundleRequest request = null;
                if (!loadAll)
                {
                    request = gameAsset.assetBundle.LoadAssetAsync(assetName);
                }
                else
                {
                    request = gameAsset.assetBundle.LoadAssetWithSubAssetsAsync(assetName);
                }
                yield return request;

                if (request.isDone && request.asset != null)
                {
                    UnityEngine.Debug.Log("Load game asset in bundle succeed:" + assetPath);
                    if (!loadAll)
                    {
                        gameAsset.assetObject = request.asset;
                    }
                    else
                    {
                        gameAsset.assetObjects = request.allAssets;
                    }
                }
                else
                {
                    UnityEngine.Debug.Log("Load game asset in bundle failed:" + assetPath);
                }
            }
            gameAsset.refUseCnt += 1;
            if (finishCallBackFunc != null)
            {
                finishCallBackFunc(gameAsset);
            }
            yield break;
        }
        // add game asset object to memory first for fear repeat read asset by different coroutine.
        gameAsset = new GameAsset();
        gameAsset.assetPath = assetPath;
        gameAsset.depends = depends;
        gameAssetList.Add(gameAsset);
        // then real load asset from disk.
        // try to load game asset from the outter path.
        UnityEngine.Debug.Log("Load game asset at outter path:" + outterFilePath);
        www = new WWW(outterFilePath);
        yield return www;
        // if can not load game asset, then try to load game asset from the inner path. 
        if (!string.IsNullOrEmpty(www.error))
        {
            UnityEngine.Debug.Log("Load game asset at inner path:" + innerFilePath);
            www.Dispose();
            www = new WWW(innerFilePath);
            yield return www;
        }
        // if load game asset succeed, then to parse the game asset
        if (string.IsNullOrEmpty(www.error) && www.isDone)
        {
            // begin to load game asset
            UnityEngine.Debug.Log("Load game asset succeed:" + assetPath);
            gameAsset.webStream = www;
            gameAsset.assetBundle = www.assetBundle;
            if (!loadBundleOnly)
            {
                AssetBundleRequest request = null;
                if (!loadAll)
                {
                    request = gameAsset.assetBundle.LoadAssetAsync(assetName);
                }
                else
                {
                    request = gameAsset.assetBundle.LoadAssetWithSubAssetsAsync(assetName);
                }
                yield return request;

                if (request.isDone && request.asset != null)
                {
                    UnityEngine.Debug.Log("Load game asset in bundle succeed:" + assetPath);
                    if (!loadAll)
                    {
                        gameAsset.assetObject = request.asset;
                    }
                    else
                    {
                        gameAsset.assetObjects = request.allAssets;
                    }
                }
                else
                {
                    UnityEngine.Debug.Log("Load game asset in bundle failed:" + assetPath);
                }
            }
            gameAsset.refUseCnt += 1;
            if (finishCallBackFunc != null)
            {
                finishCallBackFunc(gameAsset);
            }
        }
        else
        {
            UnityEngine.Debug.Log("Load game asset failed:" + assetPath + " error: " + www.error);
            www.Dispose();
        }
    }
    public T Load<T>(string path) where T : Object
    {
#if UNITY_EDITOR
        if (!Directory.Exists(PathHelper.GetAssetsOutterPath()))
        {
            string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(), path);
            //UnityEngine.Debug.Log("Load game asset at inner path:" + innerFilePath);
            return AssetDatabase.LoadAssetAtPath<T>(innerFilePath);
        }
        else
        {
            string[] depends = manifest.GetAllDependencies(path);
            for (int index = 0; index < depends.Length; index++)
            {
                LoadAssetSyncInBundle<T>(depends[index], true, false);
            }
            return LoadAssetSyncInBundle<T>(path, false, false, depends);
        }
#else
        string[] depends = manifest.GetAllDependencies(path);
        for (int index = 0; index < depends.Length; index++)
        {
            LoadAssetSyncInBundle<T>(depends[index], true, false);
        }
        return LoadAssetSyncInBundle<T>(path, false, false, depends);
#endif
    }
    T LoadAssetSyncInBundle<T>(string assetPath, bool loadBundleOnly, bool loadAll, string[] depends = null) where T : Object
    {
        string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, false), assetPath);
        string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(true, false), assetPath);
        string assetName = Path.GetFileName(assetPath);
        GameAsset gameAsset = GetGameAsset(assetPath);
        AssetBundle assetBundle;
        if (gameAsset != null)
        {
            if (gameAsset.refUseCnt == 0)
            {
                return null;
            }
            gameAsset.refUseCnt += 1;
            if (!loadBundleOnly && gameAsset.assetObject == null)
            {
                if (!loadAll)
                {
                    gameAsset.assetObject = gameAsset.assetBundle.LoadAsset(assetName);
                }
                else
                {
                    gameAsset.assetObjects = gameAsset.assetBundle.LoadAssetWithSubAssets(assetName);
                    return null;
                }
            }
            return gameAsset.assetObject as T;
        }
        gameAsset = new GameAsset();
        gameAsset.assetPath = assetPath;
        gameAsset.depends = depends;
        UnityEngine.Debug.Log("Load game asset at outter path:" + outterFilePath);
        assetBundle = AssetBundle.LoadFromFile(outterFilePath);
        if (assetBundle == null)
        {
            UnityEngine.Debug.Log("Load game asset at inner path:" + innerFilePath);
            assetBundle = AssetBundle.LoadFromFile(innerFilePath);
        }
        if (assetBundle == null)
        {
            UnityEngine.Debug.Log("Load game asset failed:" + assetPath);
            return null;
        }
        gameAsset.assetBundle = assetBundle;
        gameAssetList.Add(gameAsset);
        gameAsset.refUseCnt += 1;
        if (!loadBundleOnly)
        {
            if (!loadAll)
            {
                gameAsset.assetObject = assetBundle.LoadAsset(assetName);
            }
            else
            {
                gameAsset.assetObjects = assetBundle.LoadAssetWithSubAssets(assetName);
                return null;
            }
        }
        return gameAsset.assetObject as T;
    }
    public Object[] LoadWithSubAssets(string path)
    {
#if UNITY_EDITOR
        if (!Directory.Exists(PathHelper.GetAssetsOutterPath()))
        {
            string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(), path);
            //UnityEngine.Debug.Log("Load game asset at inner path:" + innerFilePath);
            return AssetDatabase.LoadAllAssetsAtPath(innerFilePath);
        }
        else
        {
            LoadAssetSyncInBundle<Object>(path, false, true);
            return GetGameAsset(path).assetObjects;
        }
#else
        LoadAssetSyncInBundle<Object>(path, false, true);
        return GetGameAsset(path).assetObjects;
#endif
    }
    public Texture LoadTexture(string name)
    {
        return Load<Texture>(TextureFolder + name + ".png");
    }
    public Material LoadMaterial(string name)
    {
        return Load<Material>(MaterialFolder + name + ".mat");
    }
    public void LoadAsync<T>(string path, LoadTaskCallBack onLoaded) where T : Object
    {
        LoadTask loadTask = new LoadTask(path, typeof(T), false, onLoaded);
        loadTaskList.Add(loadTask);
    }
    public void LoadWithSubAssetsAsync<T>(string path, LoadTaskCallBack onLoaded) where T : Object
    {
        LoadTask loadTask = new LoadTask(path, typeof(T), true, onLoaded);
        loadTaskList.Add(loadTask);
    }
    public void LoadTextureAsync(string name, LuaOnLoadTexutreEvent onLoaded, XLua.LuaTable self, XLua.LuaTable args = null)
    {
        LoadAsync<Texture>(TextureFolder + name + ".png", (asset) => { onLoaded(self, (Texture)asset.assetObject, args); });
    }
    public void LoadMaterialAsync(string name, LuaOnLoadMaterialEvent onLoaded, XLua.LuaTable self, XLua.LuaTable args = null)
    {
        LoadAsync<Material>(MaterialFolder + name + ".mat", (asset) => { onLoaded(self, (Material)asset.assetObject, args); });
    }
    public void CacheTexture(string name)
    {
        LoadAsync<Texture>(TextureFolder + name + ".png", (asset) => { textureCache.Add(name, (Texture)asset.assetObject); });
    }
    public Texture GetTexture(string name)
    {
        return textureCache[name];
    }
    public void CacheMaterial(string name)
    {
        LoadAsync<Material>(MaterialFolder + name + ".mat", (asset) => { materialCache.Add(name, (Material)asset.assetObject); });
    }
    public Material GetMaterial(string name)
    {
        return materialCache[name];
    }
    public void CacheSpriteAtlas(string name)
    {
        LoadWithSubAssetsAsync<Sprite>(SpriteAtlasFolder + name + ".png", (asset) =>
        {
            List<Sprite> filterList = new List<Sprite>();
            for (int i = 0; i < asset.assetObjects.Length; ++i)
            {
                if (asset.assetObjects[i] is Sprite)
                    filterList.Add(asset.assetObjects[i] as Sprite);
            }
            spriteAtlasCache.Add(name, filterList.ToArray());
        });
    }
    public Sprite[] GetSpriteAtlas(string name)
    {
        return spriteAtlasCache[name];
    }
    public Sprite GetSpriteFromAtlas(string atlasName, string spriteName)
    {
        for (int i = 0; i < spriteAtlasCache[atlasName].Length; ++i)
        {
            if (spriteAtlasCache[atlasName][i].name == spriteName)
                return spriteAtlasCache[atlasName][i];
        }
        return null;
    }
    public GameObject CreateGameObject(Object prefab)
    {
        if (prefab == null)
        {
            return null;
        }
        GameObject gameObject = (GameObject)GameObject.Instantiate(prefab);
        if (gameObject == null)
        {
            return null;
        }
        for (int gaIndex = 0; gaIndex <= gameAssetList.Count - 1; gaIndex++)
        {
            GameAsset gameAsset = gameAssetList[gaIndex];
            if (gameAsset.assetObject == prefab)
            {
                gameAsset.refUseCnt += 1;
                break;
            }
        }
        prefabMap.Add(gameObject, prefab);
        return gameObject;
    }
    public void DestroyGameObject(GameObject gameObject)
    {
        if (gameObject == null)
        {
            return;
        }
        if (!prefabMap.ContainsKey(gameObject))
        {
            return;
        }

        Object prefab = prefabMap[gameObject];
        prefabMap.Remove(gameObject);
        GameObject.Destroy(gameObject);
        UnloadAsset(prefab);
    }
    public void UnloadAsset(Object assetObject)
    {
        if (assetObject == null)
        {
            return;
        }
        GameAsset tmpGameAsset = null;
        ArrayList tmpGameAssetIndexList = new ArrayList();
        for (int gaIndex = 0; gaIndex <= gameAssetList.Count - 1; gaIndex++)
        {
            GameAsset gameAsset = gameAssetList[gaIndex];
            if (gameAsset.assetObject == assetObject)
            {
                if (gameAsset.refUseCnt > 1)
                {
                    gameAsset.refUseCnt -= 1;
                    return;
                }
                tmpGameAssetIndexList.Add(gaIndex);
                tmpGameAsset = gameAsset;
                break;
            }
        }
        if (tmpGameAsset != null && tmpGameAsset.depends != null)
        {
            for (int index = 0; index < tmpGameAsset.depends.Length; index++)
            {
                for (int gaIndex = 0; gaIndex <= gameAssetList.Count - 1; gaIndex++)
                {
                    GameAsset gameAsset = gameAssetList[gaIndex];
                    if (gameAsset.assetPath == tmpGameAsset.depends[index])
                    {
                        if (gameAsset.refUseCnt > 1)
                        {
                            gameAsset.refUseCnt -= 1;
                        }
                        else
                        {
                            tmpGameAssetIndexList.Add(gaIndex);
                        }
                    }
                }
            }
        }
        for (int gaIndex = gameAssetList.Count - 1; gaIndex >= 0; gaIndex--)
        {
            if (tmpGameAssetIndexList.Contains(gaIndex))
            {
                GameAsset gameAsset = gameAssetList[gaIndex];
                gameAsset.assetObject = null;
                if (gameAsset.assetBundle != null)
                {
                    gameAsset.assetBundle.Unload(true);
                    gameAsset.assetBundle = null;
                }
                if (gameAsset.webStream != null)
                {
                    gameAsset.webStream.Dispose();
                }
                gameAssetList.RemoveAt(gaIndex);
            }
        }
    }
    public void UnloadAssetsWithSubAssets(Object[] assets)
    {
        UnloadAsset(assets[0]);
    }
    GameAsset GetGameAsset(string assetPath)
    {
        for (int index = 0; index <= gameAssetList.Count - 1; index++)
        {
            GameAsset gameAsset = gameAssetList[index];
            if (gameAsset.assetPath == assetPath)
            {
                return gameAsset;
            }
        }
        return null;
    }
    void CleanAllForGC()
    {
        for (int gaIndex = 0; gaIndex <= gameAssetList.Count - 1; gaIndex++)
        {
            GameAsset gameAsset = gameAssetList[gaIndex];
            gameAsset.assetObject = null;
            if (gameAsset.assetBundle != null)
            {
                gameAsset.assetBundle.Unload(true);
                gameAsset.assetBundle = null;
            }
            if (gameAsset.webStream != null)
            {
                gameAsset.webStream.Dispose();
            }
        }
        gameAssetList.Clear();
        prefabMap.Clear();
        loadTaskList.Clear();
    }
    public override void Release()
    {
        StopAllCoroutines();
    }
}
