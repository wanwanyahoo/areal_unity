﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;
public static class LuaGenConfig
{
    [LuaCallCSharp]
    public static List<Type> mymodule_lua_call_cs_list = new List<Type>()
{
    typeof(GameObject),
    typeof(Transform),
    typeof(Animator),
    typeof(Camera),
};
}
public class LuaManager : Singleton<LuaManager> 
{
    LuaEnv luaEnv;
    float lastGCTime = 0;
    float gcInterval = 10;

    public LuaEnv Env
    {
        get { return luaEnv; }
    }
    public override void Init()
    {
        luaEnv = new LuaEnv();
        luaEnv.AddLoader(CustomLoader);
    }
    byte[] CustomLoader(ref string filePath)
    {
        string realFilePath = filePath.Replace(".", "/") + ".lua.txt";
        TextAsset luaAsset = ResourceManager.Instance.Load<TextAsset>(realFilePath);
        byte[] textByte = luaAsset.bytes;
        ResourceManager.Instance.UnloadAsset(luaAsset);
        return textByte;
    }
    public override void SelfUpdate()
    {
        if (Time.time - lastGCTime > gcInterval)
        {
            luaEnv.Tick();
            lastGCTime = Time.time;
        }
    }
    public override void Release()
    {
        luaEnv.Dispose();
    }
    public object[] Execute(string code)
    {
        return luaEnv.DoString(code);
    }
}
