﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;
using limax.endpoint;
using limax.endpoint.script;
using limax.endpoint.variant;
using limax.net;
using limax.util;
using System;
using limax.codec;
using limax.endpoint.providerendpoint;

[LuaCallCSharp]
public class LimaxManager : Singleton<LimaxManager>, EndpointListener
{
    private EndpointManager endpointManager = null;

    #region ViewMethod
    [CSharpCallLua]
    public delegate void LuaVariantViewChangedListener(VariantViewChangedEvent e);

    #endregion ViewMethod
    #region ProtocolMethod

    [CSharpCallLua]
    public delegate void LuaReceiveProtocolDelegate(int pvid, int type, Octets data);
    private LuaReceiveProtocolDelegate receiveDelegate = null;

    public LuaReceiveProtocolDelegate ReceiveDelegate
    {
        set{ receiveDelegate = value; }

        get{ return receiveDelegate; }
    }
    #endregion ProtocolMethod
    [LuaCallCSharp]
    public static List<Type> LimaxLuaCallCSharp = new List<Type>()
    {
        typeof(Protocol),
        typeof(LuaProtocol),
        typeof(Octets),
        typeof(OctetsStream)
    };

    [CSharpCallLua]
    public delegate void OnTransportAddedDelegate(Transport transport);

    private OnTransportAddedDelegate transportAddedDelegate = null;

    public OnTransportAddedDelegate TransportAddedDelegate
    {
        get { return transportAddedDelegate; }
        set { transportAddedDelegate = value; }
    }
    [CSharpCallLua]
    public delegate void OnTransportRemovedDelegate(Transport transport);

    private OnTransportRemovedDelegate transportRemovedDelegate = null;

    public OnTransportRemovedDelegate TransportRemovedDelegate
    {
        get { return transportRemovedDelegate; }
        set { transportRemovedDelegate = value; }
    }

    [CSharpCallLua]
    public delegate void OnTransportErrorOccuredDelegate(int source, int code, String message);

    private OnTransportErrorOccuredDelegate transportErrorOccuredDelegate = null;

    public OnTransportErrorOccuredDelegate TransportErrorOccuredDelegate
    {
        get { return transportErrorOccuredDelegate; }
        set { transportErrorOccuredDelegate = value; }
    }


    #region MonoBehaviour
    public override void Init()
    {
        Endpoint.openEngine();
    }

    public override void SelfUpdate()
    {
        uiThreadSchedule();
    }

    public override void Release()
    {
        this.TransportAddedDelegate = null;
        this.TransportErrorOccuredDelegate = null;
        this.TransportRemovedDelegate = null;
        this.ReceiveDelegate = null;
        Endpoint.closeEngine(null);
    }

    public void Send(int pvid, int type, Octets data)
    {
        endpointManager.getTransport().sendTypedData(pvid << 24 | type, data);
    }
    #endregion MonoBehaviour

    #region EndpointListener
    public void onAbort(Transport transport)
    {
        Debug.Log("onAbort");
    }

    public void onErrorOccured(int source, int code, Exception exception)
    {
        Debug.Log("onErrorOccured");
        Debug.Log("error : source = " + source + " code = " + code + " e = " + exception);
        if(transportErrorOccuredDelegate  != null)
            transportErrorOccuredDelegate(source, code, exception != null ? exception.ToString():"");

    }

    public void onKeepAlived(int ms)
    {
        Debug.Log("onKeepAlived");
    }

    public void onKeyExchangeDone()
    {
        Debug.Log("onKeyExchangeDone");
    }

    public void onManagerInitialized(Manager manager, Config config)
    {
        Debug.Log("onManagerInitialized");
        endpointManager = (EndpointManager)manager;
    }

    public void onManagerUninitialized(Manager manager)
    {
        Debug.Log("onManagerUninitialized");
        endpointManager = null;
    }

    public void onSocketConnected()
    {
        Debug.Log("onSocketConnected");
    }

    public void onTransportAdded(Transport transport)
    {
        Debug.Log("onTransportAdded");
        if (transportAddedDelegate != null)
            transportAddedDelegate(transport);
    }

    public void onTransportRemoved(Transport transport)
    {
        Debug.Log("onTransportRemoved");
        if (transportRemovedDelegate != null)
            transportRemovedDelegate(transport);
    }

    public void onReceivedLuaProtocol(Protocol p)
    {
        Debug.Log("onReceivedLuaProtocol:" + p.getType());
        if (this.receiveDelegate != null)
        {
            int pvid = p.getType() >> 24 & 0xff;
            int realtype = p.getType() & 0x00ffffff;
            this.receiveDelegate(pvid, realtype, ((LuaProtocol)p).data);
        } 
    }
    public void uiThreadSchedule()
    {
        limax.util.Utils.uiThreadSchedule();
    }
    #endregion EndpointListener

    #region LuaMethod
    public void ConnectedToServer(string ip, int port, string user, string token, string platform)
    {

        EndpointConfig config = Endpoint.createLoginConfigBuilder(ip, port, user, token, platform)
            .scriptEngineHandle(new LuaScriptHandle())
            .executor(r => limax.util.Utils.runOnUiThread(r))
            .build();
        Endpoint.start(config, this);
    }

    public void Close()
    {
        if (endpointManager != null)
            endpointManager.close();
    }

    #endregion LuaMethod
}