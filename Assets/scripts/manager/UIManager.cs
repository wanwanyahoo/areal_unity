﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[XLua.LuaCallCSharp]
public class UIManager : Singleton<UIManager> 
{
    [XLua.CSharpCallLua]
    public delegate void OnCreateEvent(GameObject gameObject);
    [XLua.CSharpCallLua]
    public delegate void LuaOnCreateEvent(XLua.LuaTable self, GameObject gameObject, XLua.LuaTable args);

    Dictionary<string, GameObject> uiDict;
    public override void Init()
    {
        uiDict = new Dictionary<string, GameObject>();
        this.gameObject.AddComponent<EventSystem>();
        this.gameObject.AddComponent<StandaloneInputModule>();
        var canvas = this.gameObject.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = Camera.main;
        canvas.planeDistance = 10;
        canvas.sortingOrder = 100;
    }
    public void SetUIGroup(GameObject ui, string groupName)
    {
        SetUIGroup(new GameObject[] { ui }, groupName);
    }
    public void SetUIGroup(GameObject[] ui, string groupName)
    {
        GameObject groupRoot = GetUIGroup(groupName);
        if (groupRoot == null)
        {
            groupRoot = new GameObject(groupName);
            DontDestroyOnLoad(groupRoot);
        }
        for (int i = 0;i < ui.Length; ++i)
            ui[i].transform.SetParent(groupRoot.transform, false);
    }
    public GameObject GetUIGroup(string groupName)
    {
        return GameObject.Find(groupName);
    }
    public GameObject Create(string name, string keyName, bool isChild = false, bool isResident = false)
    {
        if (uiDict.ContainsKey(keyName))
        {
            return uiDict[keyName];
        }
        GameObject prefab = ResourceManager.Instance.Load<GameObject>("prefabs/ui/" + name + ".prefab");
        var ui = ResourceManager.Instance.CreateGameObject(prefab);
        ResourceManager.Instance.UnloadAsset(prefab);
        ui.name = keyName;
        if (isResident)
        {
            DontDestroyOnLoad(ui);
        }
        if (!isChild)
        {
            uiDict.Add(keyName, ui);
        }
        ResetCamera(ui);
        return ui;
    }
    public GameObject Create(string name, bool isResident = false)
    {
        return Create(name, name, false, isResident);
    }
    public GameObject CreateChild(string name, bool isResident = false)
    {
        return Create(name, name, true, isResident);
    }
    public void CreateAsync(string name, string keyName, OnCreateEvent onCreated, bool isChild = false, bool isResident = false)
    {
        if (uiDict.ContainsKey(keyName))
        {
            return;
        }
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/ui/" + name + ".prefab", (asset) =>
            {
                var prefab = (GameObject)asset.assetObject;
                if (prefab != null)
                {
                    var ui = ResourceManager.Instance.CreateGameObject(prefab);
                    ResourceManager.Instance.UnloadAsset(prefab);
                    ui.name = keyName;
                    if (isResident)
                    {
                        DontDestroyOnLoad(ui);
                    }
                    if (!isChild)
                    {
                        uiDict.Add(keyName, ui);
                    }
                    onCreated(ui);
                    ResetCamera(ui);
                }
            });
    }
    public void CreateAsync(string name, OnCreateEvent onCreated, bool isResident = false)
    {
        CreateAsync(name, name, onCreated, false, isResident);
    }
    public void CreateChildAsync(string name, OnCreateEvent onCreated, bool isResident = false)
    {
        CreateAsync(name, name, onCreated, true, isResident);
    }
    public void CreateAsync(string name, string keyName, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null, 
        bool isChild = false, bool isResident = false)
    {
        CreateAsync(name, keyName, (gameObject) => { onCreated(self, gameObject, args); }, isChild, isResident);
    }
    public void CreateAsync(string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null, bool isResident = false)
    {
        CreateAsync(name, name, (gameObject) => { onCreated(self, gameObject, args); }, false, isResident);
    }
    public void CreateChildAsync(string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null, bool isResident = false)
    {
        CreateAsync(name, name, (gameObject) => { onCreated(self, gameObject, args); }, true, isResident);
    }
    public void Destroy(string keyName)
    {
        ResourceManager.Instance.DestroyGameObject(uiDict[keyName]);
        uiDict.Remove(keyName);
    }
    public GameObject GetChild(string keyName, string path)
    {
        if (uiDict.ContainsKey(keyName))
            return GetChild(uiDict[keyName], path);
        else
            return null;
    }
    public GameObject GetChild(GameObject ui, string path)
    {
        if (ui != null)
        {
            Transform transform = ui.transform.Find(path);
            if (transform != null)
                return transform.gameObject;
            else
                return null;
        }
        return null;
    }
    /// <summary>
    /// 重设RenderCamera
    /// </summary>
    /// <param name="ui"></param>
    public void ResetCamera(GameObject ui)
    {
        var canvas = ui.GetComponent<Canvas>();
        if (canvas != null)
        {
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = Camera.main;
            canvas.planeDistance = 10;
        }
    }
}
