﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OriginalSceneManager = UnityEngine.SceneManagement.SceneManager;
using LoadMode = UnityEngine.SceneManagement.LoadSceneMode;
using Scene = UnityEngine.SceneManagement.Scene;
using System.IO;

[XLua.LuaCallCSharp]
public class SceneManager : Singleton<SceneManager> 
{
    public enum TaskStatus
    {
        Start,
        Loading,
        Finished
    }

    [XLua.CSharpCallLua]
    public delegate void OnLoadEvent(Scene scene);
    [XLua.CSharpCallLua]
    public delegate void LuaOnLoadEvent(XLua.LuaTable self, Scene scene, XLua.LuaTable args);
    [XLua.CSharpCallLua]
    public delegate void OnProcessEvent(float progress);

    TaskStatus status = TaskStatus.Finished;
    string sceneName;
    OnLoadEvent onLoaded;
    OnProcessEvent onProcessing;
    public override void Init()
    {
        StartCoroutine(ProcessLoadSceneTask());
    }
    IEnumerator ProcessLoadSceneTask()
    {
        while (true)
        {
            if (status == TaskStatus.Start)
            {
                yield return StartCoroutine(ExecuteLoadSceneTask());
            }
            yield return null;
        }
    }
    IEnumerator ExecuteLoadSceneTask()
    {
        status = TaskStatus.Loading;
#if UNITY_EDITOR
        if (!Directory.Exists(PathHelper.GetAssetsOutterPath()))
        {
            string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(), PathHelper.Scenes + "/" + sceneName);
            innerFilePath = PathHelper.Combine(innerFilePath, sceneName + ".unity");
            //UnityEngine.Debug.Log("Load scene at inner path:" + innerFilePath);
            if (File.Exists(innerFilePath))
            {
                //UnityEngine.Debug.Log("Load scene succeed:" + scenePath);
                AsyncOperation loadAsyncOperation = OriginalSceneManager.LoadSceneAsync(sceneName, LoadMode.Additive);
                while (!loadAsyncOperation.isDone)
                {
                    if (onProcessing != null)
                    {
                        onProcessing(loadAsyncOperation.progress);
                    }
                    yield return null;
                }
                onLoaded(OriginalSceneManager.GetSceneByName(sceneName));
            }
            else
            {
                //UnityEngine.Debug.Log("Load scene failed:" + scenePath);
            }
        }
        else
        {
            string scenePath = PathHelper.Scenes + "/" + sceneName + ".scene";
            string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, true), scenePath);
            string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(true, true), scenePath);
            //UnityEngine.Debug.Log("Load scene at outter path:" + outterFilePath);
            WWW www = new WWW(outterFilePath);
            yield return www;
            // if can not load game asset, then try to load game asset from the inner path. 
            if (!string.IsNullOrEmpty(www.error))
            {
                //UnityEngine.Debug.Log("Load scene at inner path:" + innerFilePath);
                www.Dispose();
                www = new WWW(innerFilePath);
                yield return www;
            }
            // if load game asset succeed, then to parse the game asset
            if (string.IsNullOrEmpty(www.error) && www.isDone)
            {
                // begin to load game asset
                //UnityEngine.Debug.Log("Load scene succeed:" + scenePath);
                AssetBundle assetBundle = www.assetBundle;
                AsyncOperation loadAsyncOperation = OriginalSceneManager.LoadSceneAsync(sceneName, LoadMode.Additive);
                while (!loadAsyncOperation.isDone)
                {
                    if (onProcessing != null)
                    {
                        onProcessing(loadAsyncOperation.progress);
                    }
                    yield return null;
                }
                //UnityEngine.Debug.Log("Load scene finished:" + scenePath);
                assetBundle.Unload(false);
                www.Dispose();
                onLoaded(OriginalSceneManager.GetSceneByName(sceneName));
            }
            else
            {
                //UnityEngine.Debug.Log("Load scene failed:" + scenePath + " error: " + www.error);
                www.Dispose();
            }
        }
#else
        string scenePath = PathHelper.Scenes + "/" + sceneName + ".scene";
        string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, true), scenePath);
        string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(true, true), scenePath);
        UnityEngine.Debug.Log("Load scene at outter path:" + outterFilePath);
        WWW www = new WWW(outterFilePath);
        yield return www;
        // if can not load game asset, then try to load game asset from the inner path. 
        if (!string.IsNullOrEmpty(www.error))
        {
            UnityEngine.Debug.Log("Load scene at inner path:" + innerFilePath);
            www.Dispose();
            www = new WWW(innerFilePath);
            yield return www;
        }
        // if load game asset succeed, then to parse the game asset
        if (string.IsNullOrEmpty(www.error) && www.isDone)
        {
            // begin to load game asset
            UnityEngine.Debug.Log("Load scene succeed:" + scenePath);
            AssetBundle assetBundle = www.assetBundle;
            AsyncOperation loadAsyncOperation = OriginalSceneManager.LoadSceneAsync(sceneName, LoadMode.Additive);
            while(!loadAsyncOperation.isDone)
            {
                if (onProcessing != null)
                {
                    onProcessing(loadAsyncOperation.progress);
                }
                yield return null;
            }
            UnityEngine.Debug.Log("Load scene finished:" + scenePath);
            assetBundle.Unload(false);
            www.Dispose();
            onLoaded(OriginalSceneManager.GetSceneByName(sceneName));
        }
        else
        {
            UnityEngine.Debug.Log("Load scene failed:" + scenePath + " error: " + www.error);
            www.Dispose();
        }
#endif
        status = TaskStatus.Finished;
    }
    public void LoadAsync(string name, OnLoadEvent onLoaded, OnProcessEvent onProcessing, bool activate = true)
    {
        if (status != TaskStatus.Finished)
        {
            Debug.LogWarning("There already have a scene loading.");
            return;
        }
        sceneName = name;
        status = TaskStatus.Start;
        this.onLoaded = (scene) =>
            {
                if (activate)
                {
                    OriginalSceneManager.SetActiveScene(scene);
                }
                if (onLoaded != null)
                    onLoaded(scene);
            };
        this.onProcessing = onProcessing;
    }
    public void LoadAsync(string path, OnLoadEvent onLoaded, bool activate = true)
    {
        LoadAsync(path, onLoaded, null, activate);
    }
    public void LoadAsync(string path, LuaOnLoadEvent onLoaded, XLua.LuaTable self, XLua.LuaTable args = null, bool activate = true)
    {
        LoadAsync(path, (scene) => { onLoaded(self, scene, args); }, activate);
    }
    public void UnloadAsync(string name)
    {
        OriginalSceneManager.UnloadSceneAsync(name);
    }
    public void UnloadActiveSceneAsync()
    {
        OriginalSceneManager.UnloadSceneAsync(OriginalSceneManager.GetActiveScene());
    }
    public Scene GetActiveScene()
    {
        return OriginalSceneManager.GetActiveScene();
    }
}
