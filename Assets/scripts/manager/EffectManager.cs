﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[XLua.LuaCallCSharp]
public class EffectManager : Singleton<EffectManager> 
{
    [XLua.CSharpCallLua]
    public delegate void OnCreateEvent(Effect effect);
    [XLua.CSharpCallLua]
    public delegate void LuaOnCreateEvent(XLua.LuaTable self, Effect effect, XLua.LuaTable args);

    List<Effect> effectList;
    public Effect[] Effects { get { return effectList.ToArray(); } }
    public override void Init()
    {
        effectList = new List<Effect>();
    }
    public Effect Create(string name, bool isChild = false)
    {
        GameObject prefab = ResourceManager.Instance.Load<GameObject>("prefabs/effects/" + name + ".prefab");
        var effect = ResourceManager.Instance.CreateGameObject(prefab).AddComponent<Effect>();
        ResourceManager.Instance.UnloadAsset(prefab);
        effect.SelfStart();
        if (!isChild)
        {
            effectList.Add(effect);
        }
        return effect;
    }
    public Effect CreateOnUI(string name)
    {
        var uiEffect = Create(name, true);
        uiEffect.transform.SetParent(this.gameObject.transform, false);
        return uiEffect;
    }
    public Effect CreateOnUI(GameObject uiParent, string name)
    {
        var uiEffect = Create(name, true);
        uiEffect.transform.SetParent(uiParent.transform, false);
        return uiEffect;
    }
    public void CreateAsync(string name, OnCreateEvent onCreated, bool isChild = false)
    {
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/effects/" + name + ".prefab", (asset) =>
        {
            OnCreated((GameObject)asset.assetObject, null, onCreated, isChild);
        });
    }
    public void CreateAsync(string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null, bool isChild = false)
    {
        CreateAsync(name, (effect) => { onCreated(self, effect, args); }, isChild);
    }
    public void CreateOnUIAsync(string name, OnCreateEvent onCreated)
    {
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/effects/" + name + ".prefab", (asset) =>
        {
            OnCreated((GameObject)asset.assetObject, this.gameObject, onCreated, true);
        });
    }
    public void CreateOnUIAsync(string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null)
    {
        CreateOnUIAsync(name, (effect) => { onCreated(self, effect, args); });
    }
    public void CreateOnUIAsync(GameObject uiParent, string name, OnCreateEvent onCreated)
    {
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/effects/" + name + ".prefab", (asset) =>
        {
            OnCreated((GameObject)asset.assetObject, uiParent, onCreated, true);
        });
    }
    public void CreateOnUIAsync(GameObject uiParent, string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null)
    {
        CreateOnUIAsync(uiParent, name, (effect) => { onCreated(self, effect, args); });
    }
    Effect OnCreated(GameObject asset, GameObject uiParent, OnCreateEvent onCreated, bool isChild)
    {
        var prefab = asset;
        if (prefab != null)
        {
            var effect = ResourceManager.Instance.CreateGameObject(prefab).AddComponent<Effect>();
            ResourceManager.Instance.UnloadAsset(prefab);
            effect.SelfStart();
            if (!isChild)
            {
                effectList.Add(effect);
            }
            if (uiParent != null)
            {
                effect.transform.SetParent(uiParent.transform, false);
            }
            onCreated(effect);
            return effect;
        }
        return null;
    }
    public void Destroy(Effect effect)
    {
        effectList.Remove(effect);
        ResourceManager.Instance.DestroyGameObject(effect.gameObject);        
    }
}
