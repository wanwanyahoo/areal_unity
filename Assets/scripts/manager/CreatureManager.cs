﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[XLua.LuaCallCSharp]
public class CreatureManager : Singleton<CreatureManager>
{
    [XLua.CSharpCallLua]
    public delegate void OnCreateEvent(Creature creature);
    [XLua.CSharpCallLua]
    public delegate void LuaOnCreateEvent(XLua.LuaTable self, Creature creature, XLua.LuaTable args);

    List<Creature> creatureList;
    public Creature[] Creatures { get { return creatureList.ToArray(); } }
    public override void Init()
    {
        creatureList = new List<Creature>();
    }
    public Creature Create(string name, Vector2 position, bool isChild = false)
    {
        GameObject prefab = ResourceManager.Instance.Load<GameObject>("prefabs/creatures/" + name + ".prefab");
        var creature = ResourceManager.Instance.CreateGameObject(prefab).AddComponent<Creature>();
        ResourceManager.Instance.UnloadAsset(prefab);
        creature.transform.position = new Vector3(position.x, prefab.transform.position.y, position.y);
        creature.SelfStart();
        if (!isChild)
        {
            creatureList.Add(creature);
        }
        return creature;
    }
    public Creature Create(string name)
    {
        return Create(name, Vector2.zero, false);
    }
    public Creature CreateChild(string name)
    {
        return Create(name, Vector2.zero, true);
    }
    public void CreateAsync(string name, Vector2 position, OnCreateEvent onCreated, bool isChild = false)
    {
        ResourceManager.Instance.LoadAsync<GameObject>("prefabs/creatures/" + name + ".prefab", (asset) =>
        {
            var prefab = (GameObject)asset.assetObject;
            if (prefab != null)
            {
                var creature = ResourceManager.Instance.CreateGameObject(prefab).AddComponent<Creature>();
                ResourceManager.Instance.UnloadAsset(prefab);
                creature.transform.position = new Vector3(position.x, prefab.transform.position.y, position.y);
                creature.SelfStart();
                if (!isChild)
                {
                    creatureList.Add(creature);
                }
                onCreated(creature);
            }
        });
    }
    public void CreateAsync(string name, OnCreateEvent onCreated)
    {
        CreateAsync(name, Vector2.zero, onCreated, false);
    }
    public void CreateChildAsync(string name, OnCreateEvent onCreated)
    {
        CreateAsync(name, Vector2.zero, onCreated, true);
    }
    public void CreateAsync(string name, Vector2 position, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null, bool isChild = false)
    {
        CreateAsync(name, position, (creature) => { onCreated(self, creature, args); }, isChild);
    }
    public void CreateAsync(string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null)
    {
        CreateAsync(name, Vector2.zero, onCreated, self, args, false);
    }
    public void CreateChildAsync(string name, LuaOnCreateEvent onCreated, XLua.LuaTable self, XLua.LuaTable args = null)
    {
        CreateAsync(name, Vector2.zero, onCreated, self, args, true);
    }
    public void Destroy(Creature creature)
    {
        creatureList.Remove(creature);
        ResourceManager.Instance.DestroyGameObject(creature.gameObject);
    }
    public override void SelfUpdate()
    {
        for (int i = 0; i < creatureList.Count; ++i)
            creatureList[i].SelfUpdate();
    }
}