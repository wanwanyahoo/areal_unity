﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;
using Authc;

[LuaCallCSharp]
public class AuthcManager : Singleton<AuthcManager> {

    #region authc lua interface
    [LuaCallCSharp]
    public static List<Type> authcLuaCallCSharp = new List<Type>()
    {
        typeof(Protocol),
        typeof(Octets),
        typeof(OctetsStream),
        typeof(Encoding),
        typeof(ARCFourSecurity),
        typeof(DecomARC4Scurity),
        typeof(BitConverter)
    };
    #endregion authc lua interface

    #region delegate for lua
    [CSharpCallLua]
    public delegate void LuaReceiveProtocolDelegate(Protocol p);
    [CSharpCallLua]
    public delegate void LuaSocketExceptionDelegate(int code, string message);
    #endregion delegate for lua

    #region private members
    private LuaReceiveProtocolDelegate _receiveDelegate = null;
    private LuaSocketExceptionDelegate _exceptionDelegate = null;
    private Queue<Protocol> _receiveQueue = null;
    private Queue<Octets> _sendQueue = null;
    private AuthcClient _client = null;
    #endregion private members

    #region public properties
    public LuaReceiveProtocolDelegate ReceiveDelegate
    {
        set
        {
            _receiveDelegate = value;
        }

        get
        {
            return _receiveDelegate;
        }
    }

    public LuaSocketExceptionDelegate ExceptionDelegate
    {
        get
        {
            return _exceptionDelegate;
        }

        set
        {
            _exceptionDelegate = value;
        }
    }
    #endregion public properties 

    #region lifecycle
    public override void Init()
    {
    }
    public override void SelfUpdate()
    {
        if (_client == null)
        {
            return;
        }

        foreach (Protocol receiveProtocol in _receiveQueue)
        {
            lock (_receiveQueue)
            {
                _receiveDelegate(receiveProtocol);
            }
        }
        _receiveQueue.Clear();

        foreach (Octets sendOcetes in _sendQueue)
        {
            _client.Send(sendOcetes);
        }
        _sendQueue.Clear();
        
    }
    public override void Release()
    {
    }
    #endregion Singleton

    #region lua method
    public void ConnectToServer(string ip, int port)
    {
        _receiveQueue = new Queue<Protocol>();
        _sendQueue = new Queue<Octets>();

        _client = new AuthcClient(ip, port);
        _client.ReceiveDelegate = (receiveProtocol) =>
        {
            lock (_receiveQueue)
            {
                _receiveQueue.Enqueue(receiveProtocol);
            }
        };
        _client.ExceptionDelegate = (code, message) =>
        {
            _exceptionDelegate(code, message);
        };
        _client.Run();
    }

    public void Send(Octets sendOctets)
    {
        _sendQueue.Enqueue(sendOctets);
    }

    public static Octets StringToOctets(string str)
    {
        return new Octets(Encoding.UTF8.GetBytes(str), Encoding.UTF8.GetByteCount(str));
    }

    public void SetOutSecurity(Authc.Security security)
    {
        _client.OutSecurity = security;
    }

    public void SetInSecurity(Authc.Security security)
    {
        _client.InSecurity = security;
    }
    #endregion lua method
}
