﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[XLua.LuaCallCSharp]
public class SingletonManager : MonoBehaviour
{
    static GameObject rootObject;
    static List<Action> updateActions = new List<Action>();
    static List<Action> releaseActions = new List<Action>();
    [XLua.BlackList]
    public void Init()
    {
        rootObject = this.gameObject;
    }
    public static GameObject GetRootObject()
    {
        return rootObject;
    }
    [XLua.BlackList]
    public static void InitSingleton<T>() where T : Singleton<T>, new()
    {
        T singleton = rootObject.AddComponent<T>();
        singleton.SetInstance(singleton as T);
        singleton.Init();
        updateActions.Add(() =>
        {
            singleton.SelfUpdate();
        });
        releaseActions.Add(() =>
        {
            singleton.Release();
        });
    }
    [XLua.BlackList]
    public static void UpdateSingletons()
    {
        for (int i = 0; i < updateActions.Count; ++i)
        {
            updateActions[i]();
        }
    }
    /// <summary>
    /// 以初始化顺序倒序的方式逐一调用每个Singleton的Release
    /// </summary>
    [XLua.BlackList]
    public static void ReleaseSingletons()
    {
        for (int i = releaseActions.Count - 1; i >= 0; --i)
        {
            releaseActions[i]();
        }
        releaseActions.Clear();
    }
}
