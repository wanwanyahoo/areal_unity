﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BattleControlWindow : EditorWindow 
{
    List<BattleAction> battleActionList = new List<BattleAction>();
    List<bool> foldoutList = new List<bool>();
    Vector2 scrollPosition;
    public Battle Battle;
    void OnGUI()
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true);
        GUILayout.BeginVertical();
        if (GUILayout.Button("物理攻击"))
        {
            AddBattleAction(new Attack());
        }
        if (GUILayout.Button("法术技能"))
        {
            AddBattleAction(new Skill());
        }
        if (GUILayout.Button("发射技能"))
        {
            AddBattleAction(new ShootSkill());
        }
        if (GUILayout.Button("队友保护"))
        {
            AddBattleAction(new Protect());
        }
        if (GUILayout.Button("反击攻击"))
        {
            AddBattleAction(new CounterAttack());
        }
        GUILayout.Space(20);
        for (int i = 0; i < battleActionList.Count; ++i)
        {
            if (EditorGUILayout.Foldout(foldoutList[i], battleActionList[i].ToString()))
            {
                foldoutList[i] = true;
                battleActionList[i].OnGUI();
                if (GUILayout.Button("取消"))
                {
                    battleActionList.RemoveAt(i);
                    foldoutList.RemoveAt(i);
                    --i;
                }
            }
            else
                foldoutList[i] = false;
        }
        GUILayout.Space(20);
        if (battleActionList.Count > 0)
        {
            if (GUILayout.Button("执行"))
                Battle.Execute(battleActionList);
        }
        GUILayout.EndVertical();
        GUILayout.EndScrollView();
    }
    void AddBattleAction(BattleAction battleAction)
    {
        battleActionList.Add(battleAction);
        foldoutList.Add(false);
    }
}
