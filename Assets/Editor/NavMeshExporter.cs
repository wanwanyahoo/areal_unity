﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using System;
using UnityEditor;
using UnityEngine.AI;
using OriginalSceneManager = UnityEngine.SceneManagement.SceneManager;
using System.Text;

public class NavMeshExporter : EditorWindow
{
    class MyVector3<T>
    {
        public T x;
        public T y;
        public T z;
        public MyVector3(T a, T b, T c)
        {
            x = a;
            y = b;
            z = c;
        }
        public MyVector3()
        {
            x = default(T);
            y = default(T);
            z = default(T);
        }
        //Indexer
        public T this[int position]
        {
            get
            {
                if (position == 0)
                    return x;
                else if (position == 1)
                    return y;
                else if (position == 2)
                    return z;
                else
                    Debug.LogError("Position only goes from 0 to 2, in MyVector3");
                return default(T);
            }
            set
            {
                if (position == 0)
                    x = value;
                else if (position == 1)
                    y = value;
                else if (position == 2)
                    z = value;
                else
                    Debug.LogError("Position only goes from 0 to 2, in MyVector3");
            }
        }
    }
    //Constants
    const float LOWER_LIMIT = -10000;       //Used when looking for the navmesh bounds
    const float UPPER_LIMIT = 10000;
    const int maxPolyVerts = 300000;
    const int maxPolys = 100000;            //a hundred thousand polygons should be a good upper limit
    const int maxVertsPerPoly = 3;
    const ushort NullIndex = 0xFFFF;
    //used in LoadBase()
    private static float[] boundsMin = new float[3];      //lowest (x,y,z)       Note: CLR initializes to zero
    private static float[] boundsMax = new float[3];      //highest (x,y,z)
    private static float voxelSize = 0.1666667f;
    private static float xzCellSize = 0.30f;      //These two are important. If using the recast demo to generate your navmesh,
    private static float yCellSize = 0.20f;       //Use the same values here
    //used in LoadPolys()
    private static ushort[] verts;
    private static int vertCount;
    private static ushort[] polys;
    private static byte[] polyAreas;
    private static ushort[] polyFlags;
    private static int polyCount;
    //Remember, the index of the list is the vertex/polygon number
    private static List<MyVector3<float>> vertices = new List<MyVector3<float>>();
    private static List<MyVector3<ushort>> faces = new List<MyVector3<ushort>>();
    private static List<MyVector3<ushort>> neighborPolys = new List<MyVector3<ushort>>();
    //Loads the vertex and poly data into our Lists
    private static void ShowProgress(int percent)
    {
        if (percent < 100)
            EditorUtility.DisplayProgressBar("NavMesh Exporting", "Please wait few seconds to be done.", percent / 100.0f);
        else
            EditorUtility.ClearProgressBar();
    }
    private static void ReadFromNavMesh()
    {
        vertices.Clear();
        faces.Clear();
        NavMeshTriangulation tmpMesh = NavMesh.CalculateTriangulation();
        for (int i = 0; i < tmpMesh.indices.Length; )
        {
            faces.Add(new MyVector3<ushort>((ushort)tmpMesh.indices[i], (ushort)tmpMesh.indices[i + 1], (ushort)tmpMesh.indices[i + 2]));
            i = i + 3;
        }
        for (int i = 0; i < tmpMesh.vertices.Length; ++i)
        {
            int j = 0;
            for (; j < vertices.Count; ++j)
            {
                //Merge identical vertices and update their indices as well
                if (vertices[j].x == tmpMesh.vertices[i].x &&
                    vertices[j].y == tmpMesh.vertices[i].y &&
                    vertices[j].z == tmpMesh.vertices[i].z)
                {
                    for (int k = 0; k < faces.Count; ++k)
                    {
                        if (faces[k].x == (ushort)i)
                            faces[k].x = (ushort)j;
                        if (faces[k].y == (ushort)i)
                            faces[k].y = (ushort)j;
                        if (faces[k].z == (ushort)i)
                            faces[k].z = (ushort)j;
                    }
                    break;
                }
            }
            if (j == vertices.Count)
            {
                vertices.Add(new MyVector3<float>(tmpMesh.vertices[i].x, tmpMesh.vertices[i].y, tmpMesh.vertices[i].z));
                //Update indices because front vertices probably have been merged
                ushort newIndex = (ushort)(vertices.Count - 1);
                for (int k = 0; k < faces.Count; ++k)
                {
                    if (faces[k].x == (ushort)i)
                        faces[k].x = newIndex;
                    if (faces[k].y == (ushort)i)
                        faces[k].y = newIndex;
                    if (faces[k].z == (ushort)i)
                        faces[k].z = newIndex;
                }
            }
            if (i % 300 == 0)
                ShowProgress(i * 300 / tmpMesh.vertices.Length);
        }
        vertCount = vertices.Count;
        polyCount = faces.Count;
        //Fill polyflags array with default flags
        polyFlags = new ushort[polyCount];
        for (int i = 0; i < polyCount; i++)
        {
            polyFlags[i] = 1;               //custom user flag
        }
        //Fill polyAreas array
        polyAreas = new byte[polyCount];
        for (int i = 0; i < polyCount; i++)
        {
            polyAreas[i] = (byte)tmpMesh.areas[i];
        }
    }
    //Creates a Navmesh from the given Stream
    private static void ExportPolyMesh()
    {
        ReadFromNavMesh();
        #region Generate Neighboring polygon data
        //Then generate neighboring polygon data, by parsing the face list
        MyVector3<bool> sharedVertex;   //For the current face, what vertices are shared with the other face?
        int sharedVertices;             //if goes to 2, edge is shared
        neighborPolys.Clear();
        for (ushort q = 0; q < faces.Count; q++)
        {
            //Index of face and neighborPoly refer to the same polygon.
            neighborPolys.Add(new MyVector3<ushort>());
            neighborPolys[q].x = NullIndex;
            neighborPolys[q].y = NullIndex;
            neighborPolys[q].z = NullIndex;
            //Compare this face with every other face
            for (ushort w = 0; w < faces.Count; w++)
            {
                if (w != q)
                {
                    sharedVertices = 0;
                    sharedVertex = new MyVector3<bool>();
                    for (int j = 0; j <= 2; j++)
                    {
                        for (int k = 0; k <= 2; k++)
                        {
                            if (faces[q][j] == faces[w][k])
                            {
                                sharedVertices++;
                                sharedVertex[j] = true; //could break out of the for loop now, as face will not list the same index twice
                            }
                        }
                    }
                    if (sharedVertices == 1)
                    {
                        ushort sharedIndex = faces[q][0];
                        if (sharedVertex[1] == true)
                        {
                            sharedIndex = faces[q][1];
                        }
                        else if (sharedVertex[2] == true)
                        {
                            sharedIndex = faces[q][2];
                        }
                        for (int j = 0; j <= 2; j++)
                        {
                            if (faces[q][j] == sharedIndex)
                                continue;
                            //v1 is a edge vector of faces[q] that starts from sharedVertex to another one
                            Vector2 v1 = new Vector2(vertices[faces[q][j]].x - vertices[sharedIndex].x,
                                vertices[faces[q][j]].z - vertices[sharedIndex].z);
                            int k = 0;
                            for (; k <= 2; k++)
                            {
                                if (faces[w][k] == sharedIndex)
                                    continue;
                                //v2 is a edge vector of faces[w] that starts from sharedVertex to another one
                                Vector2 v2 = new Vector2(vertices[faces[w][k]].x - vertices[sharedIndex].x,
                                    vertices[faces[w][k]].z - vertices[sharedIndex].z);
                                //when v1 and v2 are collinear and their included angle are 0 degree, we think they are adjacent
                                if (Mathf.Approximately(v1.x * v2.y, v2.x * v1.y) && v1.x * v2.x + v1.y * v2.y > 0)
                                {
                                    if (sharedVertex[0] == true && j == 1)
                                    {
                                        neighborPolys[q][0] = w;
                                    }
                                    else if (sharedVertex[1] == true && j == 2)
                                    {
                                        neighborPolys[q][1] = w;
                                    }
                                    else if (sharedVertex[2] == true && j == 0)
                                    {
                                        neighborPolys[q][2] = w;
                                    }
                                    else
                                    {
                                        neighborPolys[q][j] = w;
                                    }
                                    break;
                                }
                            }
                            if (k < 3)
                                break;
                        }
                    }
                    if (sharedVertices > 2)
                    {
                        Debug.LogError("error: more than 2 vertices shared between polys " + q + " and " + w);
                    }
                    //Check if these faces are sharing an edge
                    if (sharedVertices == 2)
                    {
                        //get the Leftmost Right-To-Left Pair in the neighborPolys MyVector3 
                        //options are: edge 0-1, 1-2, and 2-0, respectively indexing neighboringPolys.(i.e. if index 1 of neighboring polys is 45, that means that the current polygon and polygon 45 share the edge face[1] <-> face[2]
                        if (sharedVertex[0] == true)
                        {
                            if (sharedVertex[1] == true)
                                neighborPolys[q][0] = w;        //I.e. tell this face's MyVector3 of neighboring polygons that the edge made up by vertices at 0 and 1 is shared between polygon q and w
                            else
                                neighborPolys[q][2] = w;
                        }
                        else
                        {
                            neighborPolys[q][1] = w;
                        }
                    }
                } //End iterating through other faces
            }
            if (q % 100 == 0)
                ShowProgress(q * 100 / faces.Count);
        } //End iterating through each face
        #endregion
        #region LoadBase
        //Get the min and max bounds from the vertex positions
        float lowest;
        float highest;
        //Find the bounds of the mesh. iterate through the x, y and z axes
        for (int axis = 0; axis <= 2; axis++)
        {
            lowest = UPPER_LIMIT;    //set to inital values that they do not reach
            highest = LOWER_LIMIT;
            //iterate through every vertex to find highest and lowest value of this axis
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i][axis] < lowest)
                {
                    lowest = vertices[i][axis];
                }

                if (vertices[i][axis] > highest)
                {
                    highest = vertices[i][axis];
                }
            }
            boundsMin[axis] = lowest;
            boundsMax[axis] = highest;
        }
        #endregion
        #region LoadPolys
        //Convert vertices from world space to grid space 
        xzCellSize = voxelSize;
        yCellSize = voxelSize / 2.0f;
        verts = new ushort[vertCount * 3];
        for (int i = 0; i < vertCount; i++)
        {
            verts[3 * i + 0] = (ushort)Mathf.Round((vertices[i].x - boundsMin[0]) / xzCellSize);
            verts[3 * i + 1] = (ushort)Mathf.Round((vertices[i].y - boundsMin[1]) / yCellSize);
            verts[3 * i + 2] = (ushort)Mathf.Round((vertices[i].z - boundsMin[2]) / xzCellSize);
        }
        //build polys array
        polys = new ushort[6 * polyCount];
        int ind = 0;
        int faceNo = 0;
        while (faceNo < polyCount)
        {
            polys[ind + 0] = faces[faceNo].x;
            polys[ind + 1] = faces[faceNo].y;
            polys[ind + 2] = faces[faceNo].z;

            polys[ind + 3] = neighborPolys[faceNo].x;
            polys[ind + 4] = neighborPolys[faceNo].y;
            polys[ind + 5] = neighborPolys[faceNo].z;

            ind += 6;
            faceNo++;
        }
        #endregion
    }
    private static void WritePolyMeshToFile(string path)
    {
        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            BinaryWriter writer = new BinaryWriter(fs);
            //Write basic infomations
            for (int i = 0; i < 3; ++i)
            {
                writer.Write(boundsMin[i]);
            }
            for (int i = 0; i < 3; ++i)
            {
                writer.Write(boundsMax[i]);
            }
            writer.Write(xzCellSize);
            writer.Write(yCellSize);
            writer.Write(maxVertsPerPoly);
            //Write vertices
            writer.Write(vertCount);
            for (int i = 0; i < verts.Length; ++i)
            {
                writer.Write(verts[i]);
            }
            //Write polys
            writer.Write(polyCount);
            for (int i = 0; i < polys.Length; ++i)
            {
                writer.Write(polys[i]);
            }
            //Write areas and flags
            for (int i = 0; i < polyCount; ++i)
            {
                writer.Write(polyAreas[i]);
            }
            for (int i = 0; i < polyCount; ++i)
            {
                writer.Write(polyFlags[i]);
            }
        }
    }
    [MenuItem("NavMesh/Export")]
    private static void Export()
    {
        var controller = EditorWindow.GetWindow<NavMeshExporter>("NavMesh Exporter", false);
        controller.maxSize = new Vector2(400, 200);
    }
    void OnGUI()
    {
        EditorGUILayout.HelpBox("Be sure setting the value of Voxel Size correctly, which can be seen at Navigation Window", MessageType.Info, true); 
        voxelSize = EditorGUILayout.FloatField("Voxel Size", voxelSize);
        if (GUILayout.Button("Export"))
        {
            ExportPolyMesh();
            string path = Application.dataPath + "/" + OriginalSceneManager.GetActiveScene().name + ".nvm";
            WritePolyMeshToFile(path);
            ShowProgress(100);
            Debug.Log("Success exported NavMesh at " + path);
        }
    }
}
