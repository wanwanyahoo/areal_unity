﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StandPoint))]
[CanEditMultipleObjects]
public class StandPointScene : Editor 
{
    void OnSceneGUI()
    {
        var pointsRoot = (target as StandPoint).transform;
        for (int i = 0; i < pointsRoot.childCount; ++i)
        {
            Transform point = pointsRoot.GetChild(i);
            Handles.CubeHandleCap(0, point.position, point.rotation, 1, EventType.Ignore);
            Handles.Label(point.position, point.name);
        }
    }
}
