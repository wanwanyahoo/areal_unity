﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraConstraint))]
public class CameraConstraintScene : Editor 
{
    void OnSceneGUI()
    {
        Rect constraintRect = ((CameraConstraint)target).ConstraintRect;
        Vector3 leftTop = new Vector3(constraintRect.xMin, 0, constraintRect.yMin);
        Vector3 leftBottom = new Vector3(constraintRect.xMax, 0, constraintRect.yMin);
        Vector3 rightBottom = new Vector3(constraintRect.xMax, 0, constraintRect.yMax);
        Vector3 rightTop = new Vector3(constraintRect.xMin, 0, constraintRect.yMax);
        Handles.DrawDottedLine(leftTop, leftBottom, 5);
        Handles.DrawDottedLine(leftBottom, rightBottom, 5);
        Handles.DrawDottedLine(rightBottom, rightTop, 5);
        Handles.DrawDottedLine(rightTop, leftTop, 5);
    }
}
