﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Billboard))]
public class BillboardScene : Editor 
{
	void OnSceneGUI()
    {
        Billboard billboard = (Billboard)target;
        billboard.transform.LookAt(SceneView.lastActiveSceneView.camera.transform.position, -Vector3.up);
        billboard.transform.Rotate(new Vector3(180, 0, 0));
	}
}
