﻿using System;
using System.Collections;
using UnityEngine;
using UnityEditor;

public class AutoAssetBundle : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach (var str in importedAssets)
        {
            ResetAssetBundleName(str);
        }
        foreach (var str in movedAssets)
        {
            ResetAssetBundleName(str);
        }
    }
    static void ResetAssetBundleName(string assetPath)
    {
        if (assetPath.Contains(PathHelper.GameAssets) && 
            !assetPath.EndsWith(PathHelper.GameAssets) &&
            !assetPath.Contains(PathHelper.Scenes) &&
            !(assetPath.EndsWith(".cs") || assetPath.EndsWith(".dll")))
        {
            AssetImporter importer = AssetImporter.GetAtPath(assetPath);
            string assetBundleName = assetPath.Replace("Assets/" + PathHelper.GameAssets + "/", "");
            importer.assetBundleName = assetBundleName;
        }
    }
}
