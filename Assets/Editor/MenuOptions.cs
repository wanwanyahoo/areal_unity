﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public static class MenuOptions
{
    private static MethodInfo m_PlaceUIElementRoot;

    [MenuItem("GameObject/UI/Recommended/Text", false, 0)]
    public static void AddText(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("Text");
        child.AddComponent<RectTransform>();
        Text text = child.AddComponent<Text>();
        text.raycastTarget = false;
        text.text = "Text";

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/UI/Recommended/Image", false, 0)]
    public static void AddImage(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("Image");
        child.AddComponent<RectTransform>();
        Image image = child.AddComponent<Image>();
        image.raycastTarget = false;

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/UI/Recommended/Raw Image", false, 0)]
    public static void AddRawImage(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("Raw Image");
        child.AddComponent<RectTransform>();
        RawImage rawImage = child.AddComponent<RawImage>();
        rawImage.raycastTarget = false;

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/UI/LinkImage Text", false, 2004)]
    public static void AddLinkImageText(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("LinkImage Text");
        child.AddComponent<RectTransform>();
        LinkImageText linkImageText = child.AddComponent<LinkImageText>();
        linkImageText.text = "LinkImage Text";
        linkImageText.color = Color.white;
#if UNITY_5_3_OR_NEWER
        linkImageText.alignByGeometry = true;
#endif

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/UI/Scroll Page", false, 2004)]
    public static void AddScrollPage(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("Scroll Page");
        GameObject pageFrame = new GameObject("Page Frame");
        var pageFrameRect = pageFrame.AddComponent<RectTransform>();
        pageFrame.AddComponent<GridLayoutGroup>();
        pageFrame.AddComponent<PageFrame>();
        pageFrame.transform.SetParent(child.transform);
        GameObject viewport = new GameObject("Viewport");
        var viewportRect = viewport.AddComponent<RectTransform>();
        viewportRect.anchorMin = Vector2.zero;
        viewportRect.anchorMax = Vector2.one;
        viewportRect.offsetMin = Vector2.zero;
        viewportRect.offsetMax = Vector2.zero;
        viewport.AddComponent<Mask>().showMaskGraphic = false;
        viewport.AddComponent<Image>();
        viewport.transform.SetParent(child.transform);
        GameObject content = new GameObject("Content");
        var contentRect = content.AddComponent<RectTransform>();
        content.transform.SetParent(viewport.transform);
        child.AddComponent<RectTransform>();
        var pageRect = child.AddComponent<PageRect>();
        pageRect.viewport = viewportRect;
        pageRect.content = contentRect;
        pageRect.pageFrame = pageFrameRect;
        child.AddComponent<Image>();

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/UI/Page Frame", false, 2004)]
    public static void AddPageFrame(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("Page Frame");
        child.AddComponent<RectTransform>();
        child.AddComponent<GridLayoutGroup>();
        child.AddComponent<PageFrame>();

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/UI/Recommended/Canvas", false, 0)]
    public static void AddCanvas(MenuCommand menuCommand)
    {
        GameObject child = new GameObject("Canvas");
        child.AddComponent<RectTransform>();
        var canvas = child.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        var scaler = child.AddComponent<CanvasScaler>();
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        scaler.referenceResolution = new Vector2(1280, 720);
        scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        child.AddComponent<GraphicRaycaster>();

        PlaceUIElementRoot(child, menuCommand);
    }
    [MenuItem("GameObject/Map", false ,6)]
    public static void AddMap()
    {
        GameObject child = new GameObject("Map");
        child.AddComponent<Map>();
        Selection.objects = new GameObject[] { child };
    }
    [MenuItem("GameObject/For Test/MapTest", false, 6)]
    public static void AddMapTest()
    {
        GameObject child = new GameObject("MapTest");
        child.tag = "MapTest";
        child.AddComponent<MapTest>();
        Selection.objects = new GameObject[] { child };
    }
    [MenuItem("GameObject/For Test/BattleTest", false, 6)]
    public static void AddBattleTest()
    {
        GameObject child = new GameObject("BattleTest");
        child.tag = "BattleTest";
        child.AddComponent<BattleTest>();
        Selection.objects = new GameObject[] { child };
    }

    private static void PlaceUIElementRoot(GameObject element, MenuCommand menuCommand)
    {
        if (m_PlaceUIElementRoot == null)
        {
            Assembly assembly = Assembly.GetAssembly(typeof(UnityEditor.UI.TextEditor));
            Type type = assembly.GetType("UnityEditor.UI.MenuOptions");
            m_PlaceUIElementRoot = type.GetMethod("PlaceUIElementRoot", BindingFlags.NonPublic | BindingFlags.Static);
        }
        m_PlaceUIElementRoot.Invoke(null, new object[] {element, menuCommand});
    }
}
