﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

[CustomEditor(typeof(Transform))]
public class TransformRuntimeSaver : DecoratorEditor
{
    Vector3 position;
    public TransformRuntimeSaver()
        : base("TransformInspector")
    {
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var path = Application.dataPath.Replace("Assets", "") + "/Temp/" + target.GetInstanceID() + "_runtime.byte";
        var transform = target as Transform;
        if (Application.isPlaying && GUILayout.Button("Save Current Change"))
        {
            var stream = new FileStream(path, FileMode.Create);
            stream.Write(BitConverter.GetBytes(transform.position.x), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.position.y), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.position.z), 0, 4);

            stream.Write(BitConverter.GetBytes(transform.rotation.w), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.rotation.x), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.rotation.y), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.rotation.z), 0, 4);

            stream.Write(BitConverter.GetBytes(transform.localScale.x), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.localScale.y), 0, 4);
            stream.Write(BitConverter.GetBytes(transform.localScale.z), 0, 4);
            stream.Close();
        }
        else if (!Application.isPlaying && File.Exists(path))
        {
            var stream = new FileStream(path, FileMode.Open);
            var reader = new BinaryReader(stream);
            var position = new Vector3();
            position.x = reader.ReadSingle();
            position.y = reader.ReadSingle();
            position.z = reader.ReadSingle();
            transform.position = position;
            var rotation = new Quaternion();
            rotation.w = reader.ReadSingle();
            rotation.x = reader.ReadSingle();
            rotation.y = reader.ReadSingle();
            rotation.z = reader.ReadSingle();
            transform.rotation = rotation;
            var localScale = new Vector3();
            localScale.x = reader.ReadSingle();
            localScale.y = reader.ReadSingle();
            localScale.z = reader.ReadSingle();
            transform.localScale = localScale;
            stream.Close();
            File.Delete(path);
        }
    }

}
