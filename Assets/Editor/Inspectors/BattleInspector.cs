﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Battle))]
public class BattleInspector : Editor 
{
    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Open Battle Controller"))
        {
            var controller = EditorWindow.GetWindow<BattleControlWindow>("Battle Controller", false);
            controller.maxSize = new Vector2(250, 500);
            controller.Battle = target as Battle;
        }
    }
}
