﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using System;

[CustomEditor(typeof(MapTile))]
public class MapTileInspector : Editor
{
    const string ShadeMaterialPath = "Assets/materials/map/shade.mat";
    const string FloorShadeMaterialPath = "Assets/materials/map/floorshade.mat";
    public override void OnInspectorGUI()
    {
        var mapTile = target as MapTile;
        Transform shade = mapTile.transform.Find("shade");
        if (shade != null)
        {
            var path = Application.dataPath.Replace("Assets", "") + "/Temp/" + target.GetInstanceID() + "_runtime.byte";
            if (Application.isPlaying)
            {
                mapTile.DepthOffset = Mathf.Clamp(EditorGUILayout.FloatField("Depth Offset", mapTile.DepthOffset), 0, 1);
                EditorGUILayout.Separator();
                if (GUILayout.Button("Save"))
                {
                    var stream = new FileStream(path, FileMode.Create);
                    stream.Write(BitConverter.GetBytes(mapTile.DepthOffset), 0, 4);
                    stream.Close();
                }
            }
            else if (!Application.isPlaying && File.Exists(path))
            {
                var stream = new FileStream(path, FileMode.Open);
                var reader = new BinaryReader(stream);
                float newDepthOffset = reader.ReadSingle();
                if (newDepthOffset >= 1 && mapTile.DepthOffset < 1)
                {
                    var shadeMaterial = AssetDatabase.LoadAssetAtPath<Material>(FloorShadeMaterialPath);
                    shade.GetComponent<Renderer>().material = shadeMaterial;
                    EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
                }
                else if (newDepthOffset < 1 && mapTile.DepthOffset >= 1)
                {
                    var shadeMaterial = AssetDatabase.LoadAssetAtPath<Material>(ShadeMaterialPath);
                    shade.GetComponent<Renderer>().material = shadeMaterial;
                    EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
                    
                }
                mapTile.DepthOffset = newDepthOffset;
                stream.Close();
                File.Delete(path);
            }
        }
    }
    void OnSceneGUI()
    {
        var mapTile = target as MapTile;
        Transform shade = mapTile.transform.Find("shade");
        if (shade != null && Application.isPlaying)
        {
            Vector3 pos = mapTile.transform.position;
            Vector3 scale = mapTile.transform.localScale;
            Vector3 start = new Vector3(pos.x - scale.x / 2, 0, pos.z - scale.y / 2 + mapTile.DepthOffset * scale.y);
            Vector3 end = new Vector3(pos.x + scale.x / 2, 0, start.z);
            Handles.DrawLine(start, end);
            Vector3 result = Handles.PositionHandle(start, Quaternion.identity);
            mapTile.DepthOffset = Mathf.Clamp((result.z - (pos.z - scale.y / 2)) / scale.y, 0, 1);
        }
    }
}
