﻿using System;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LinkImageText))]
class LinkImageTextInspector : Editor
{
    public override void OnInspectorGUI()
    {
        var text = (LinkImageText)target;
        base.DrawDefaultInspector();
    }
}