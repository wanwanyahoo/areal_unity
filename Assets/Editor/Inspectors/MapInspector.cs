﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using System.IO;
using UnityEditor.AI;

[CustomEditor(typeof(Map))]
public class MapInspector : Editor 
{
    const string MapFolder = "/Resources/maps/";
    const string GroundMaterialPath = "Assets/materials/map/map.mat";
    const string ShadeMaterialPath = "Assets/materials/map/shade.mat";
    public override void OnInspectorGUI()
    {
        var map = (Map)target;
        if (!map.Sliced)
        {
            map.GroundImage = EditorGUILayout.ObjectField("Ground Image", map.GroundImage, typeof(Texture2D), false) as Texture2D;
            map.UnreachableImage = EditorGUILayout.ObjectField("Unreachable Image", map.UnreachableImage, typeof(Texture2D), false) as Texture2D;
            map.ShadeImage = EditorGUILayout.ObjectField("Shade Image", map.ShadeImage, typeof(Texture2D), false) as Texture2D;
            if (map.GroundImage != null)
            {
                map.TileSize = map.TileSize <= 0 ? 256 : map.TileSize;
                map.TileSize = EditorGUILayout.IntField("Tile Size", map.TileSize);
                GUILayout.Label("Click 'Slice' to part images you choosed.");
                if (GUILayout.Button("Slice"))
                {
                    Reset(map.GroundImage);
                    Reset(map.UnreachableImage);
                    Reset(map.ShadeImage);
                    Slice(map, map.GroundImage, "ground/");
                    Slice(map, map.ShadeImage, "shade/");
                }
            }
            else
            {
                map.Sliced = false;
                map.Optimized = false;
            }
        }
        else
        {
            if (!map.Optimized)
            {
                GUILayout.Label("Click 'Optimize' to adjust settings of image tiles.");
                if (GUILayout.Button("Optimize"))
                {
                    Optimize(map, "ground/");
                    if (map.ShadeImage != null)
                    {
                        Optimize(map, "shade/");
                    }
                }
            }
            EditorGUILayout.Separator();
            map.QuadScale = map.QuadScale <= 0 ? 1 : map.QuadScale;
            map.QuadScale = EditorGUILayout.IntField("Quad Scale", map.QuadScale);
            GUILayout.Label("Click 'Tile' to put down quads.");
            if (GUILayout.Button("Tile"))
            {
                Tile(map, map.GroundImage.width, map.GroundImage.height);
            }
            if (map.UnreachableImage != null)
            {
                GUILayout.Label("Click 'Generate Navigation' to create unreachable blocks.");
                map.SaveTerrain = GUILayout.Toggle(map.SaveTerrain, "Save Terrain");
                if (GUILayout.Button("Generate Navigation"))
                {
                    GenerateNavigation(map, map.GroundImage.width, map.GroundImage.height);
                }
                GUILayout.Label("Click 'Clear Scene' if map is done.");
                if (GUILayout.Button("Clear Scene"))
                {
                    Camera mainCamera = Camera.main;
                    if (mainCamera != null)
                    {
                        GameObject.DestroyImmediate(mainCamera.gameObject);
                    }
                    GameObject mapTest = GameObject.FindGameObjectWithTag("MapTest");
                    if (mapTest != null)
                    {
                        GameObject.DestroyImmediate(mapTest);
                    }
                }
            }
        }
    }
    void Reset(Object obj)
    {
        if (obj == null)
            return;

        string path = AssetDatabase.GetAssetPath(obj);
        var importer = AssetImporter.GetAtPath(path) as TextureImporter;
        if (importer != null)
        {
            importer.npotScale = TextureImporterNPOTScale.None;
            importer.isReadable = true;
            importer.mipmapEnabled = false;
            importer.maxTextureSize = 8192;
            importer.textureCompression = TextureImporterCompression.Uncompressed;
            AssetDatabase.ImportAsset(path);
        }
    }
    void Slice(Map map, Texture2D image, string prefix)
    {
        if (image == null)
            return;

        image.GetPixel(0, 0);
        map.name = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        string folderPath = Application.dataPath + MapFolder + map.name + "/" + prefix;
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
        int row = (int)Mathf.Ceil((float)image.width / map.TileSize);
        int col = (int)Mathf.Ceil((float)image.height / map.TileSize);
        int index = 0;
        for (int y = 0; y < col; ++y)
        {
            for (int x = 0; x < row; ++x)
            {
                EditorUtility.DisplayProgressBar("Map Sliceing", "Please wait few seconds to be done.", (float)index / (col * row));

                int left = x * map.TileSize;
                int right = left + map.TileSize;
                if (right > image.width)
                {
                    right = image.width;
                }
                int up = y * map.TileSize;
                int down = up + map.TileSize;
                if (down > image.height)
                {
                    down = image.height;
                }
                var tileImage = new Texture2D(right - left, down - up);
                tileImage.wrapMode = TextureWrapMode.Clamp;
                tileImage.SetPixels(image.GetPixels(left, up, tileImage.width, tileImage.height));
                byte[] data = tileImage.EncodeToJPG();
                File.WriteAllBytes(folderPath + index + ".jpg", data);
                index++;
            }
        }
        EditorUtility.ClearProgressBar();
        map.Row = row;
        map.Col = col;
        map.Sliced = true;
        EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
    }
    void Optimize(Map map, string prefix)
    {
        string folderPath = "Assets" + MapFolder + map.name + "/" + prefix;
        int count = map.Row * map.Col;
        for (int i = 0; i < count; ++i)
        {
            EditorUtility.DisplayProgressBar("Map Optimizing", "Please wait few seconds to be done.", (float)i / count);

            var importer = AssetImporter.GetAtPath(folderPath + i + ".jpg") as TextureImporter;
            if (importer != null)
            {
                importer.wrapMode = TextureWrapMode.Clamp;
                importer.mipmapEnabled = false;
                importer.textureCompression = TextureImporterCompression.CompressedHQ;
                AssetDatabase.ImportAsset(folderPath + i + ".jpg");
            }
        }
        map.Optimized = true;
        EditorUtility.ClearProgressBar();
        EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
    }
    void GenerateNavigation(Map map, float width, float height)
    {
        var terrianData = new TerrainData();
        terrianData.heightmapResolution = 512;
        terrianData.size = new Vector3(width / map.TileSize * map.QuadScale, 1, height / map.TileSize * map.QuadScale * 2);
        byte[] colors = map.UnreachableImage.GetRawTextureData();
        float[,] heights = new float[512, 512];
        for (int y = 0; y < 512; ++y)
        {
            for (int x = 0; x < 512; ++x)
            {
                heights[y, x] = colors[(x + y * 512) * 4 + 2];
            }
        }
        terrianData.SetHeights(0, 0, heights);
        GameObject terrianObject = Terrain.CreateTerrainGameObject(terrianData);
        terrianObject.transform.position = Vector3.zero;
        NavMeshBuilder.BuildNavMesh();
        if (!map.SaveTerrain)
        {
            DestroyImmediate(terrianObject);
            DestroyImmediate(terrianData);
        }
    }
    void Tile(Map map, int width, int height)
    {
        var children = map.gameObject.GetComponentsInChildren<Transform>();
        if (children.Length > 1)
        {
            for (int i = 1; i < children.Length; ++i)
                if (children[i] != null)
                    DestroyImmediate(children[i].gameObject);
        }
        var groundMaterial = AssetDatabase.LoadAssetAtPath<Material>(GroundMaterialPath);
        var shadeMaterial = AssetDatabase.LoadAssetAtPath<Material>(ShadeMaterialPath);
        int index = 0;
        for (int y = 0; y < map.Col; ++y)
        {
            for (int x = 0; x < map.Row; ++x)
            {
                var quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                quad.AddComponent<MapTile>();
                quad.GetComponent<Renderer>().material = groundMaterial;
                quad.transform.Rotate(new Vector3(90, 0, 0));
                var scale = new Vector3(map.QuadScale, map.QuadScale * 2, 8 / Mathf.Sqrt(3));
                var position = Vector3.zero;
                position.x = (x + 0.5f) * map.QuadScale;
                position.z = (y + 0.5f) * map.QuadScale * 2;
                if (x == map.Row - 1 && width % map.TileSize != 0)
                {
                    scale.x = (width % map.TileSize) / (float)map.TileSize * map.QuadScale;
                    position.x = x * map.QuadScale + scale.x * 0.5f;
                }
                if (y == map.Col - 1 && height % map.TileSize != 0)
                {
                    scale.y = (height % map.TileSize) / (float)map.TileSize * map.QuadScale * 2;
                    position.z = y * map.QuadScale * 2 + scale.y * 0.5f;
                }
                quad.transform.localScale = scale;
                quad.transform.position = position;
                quad.transform.SetParent(map.transform, false);
                quad.name = index.ToString();
                quad.tag = "Terrain";
                if (File.Exists(Application.dataPath + MapFolder + map.name + "/shade/" + index + ".jpg"))
                {
                    var shadeQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    shadeQuad.GetComponent<Renderer>().material = shadeMaterial;
                    shadeQuad.transform.position = new Vector3(0, 0, -0.001f);
                    shadeQuad.transform.SetParent(quad.transform, false);
                    shadeQuad.name = "shade";
                    shadeQuad.tag = "Terrain";
                }
                index++;
            }
        }
        NavMeshBuilder.ClearAllNavMeshes();
        EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
    }
}
