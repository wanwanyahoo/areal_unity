﻿using System;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PageFrame))]
class PageFrameInspector : Editor
{
    public override void OnInspectorGUI()
    {
        var text = (PageFrame)target;
        int frameCount = EditorGUILayout.IntField("Total Frame", text.totalFrame);
        if (frameCount != text.totalFrame)
        {
            text.totalFrame = frameCount;
        }
        int currentFrame = EditorGUILayout.IntField("Current Frame", text.currentFrame);
        if (currentFrame != text.currentFrame)
        {
            text.currentFrame = currentFrame;
        }
        Sprite unselectedImage = EditorGUILayout.ObjectField("Unselected Image", text.unselectedImage, typeof(Sprite), false) as Sprite;
        if (unselectedImage != text.unselectedImage)
        {
            text.unselectedImage = unselectedImage;
        }
        Sprite selectedImage = EditorGUILayout.ObjectField("Selected Image", text.selectedImage, typeof(Sprite), false) as Sprite;
        if (selectedImage != text.selectedImage)
        {
            text.selectedImage = selectedImage;
        }
    }
}