﻿using System;
using System.IO;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

public class PlatformBuild
{
    private static string mOutPutPath = Application.streamingAssetsPath;
    private static string mScenePath = "Assets/"+ PathHelper.GameAssets + "/" + PathHelper.Scenes + "/";
    //private static string mMainScenePath = "Assets/"+ PathHelper.GameAssets + "/" + PathHelper.Scenes + "/main.unity";

    //[MenuItem("Build/BuildIOS/Debug", false, 1)]
    //public static void BuildDebugIOS()
    //{
        
    //}

    //[MenuItem("Build/BuildIOS/Release", false, 2)]
    //public static void BuildReleaseIOS()
    //{


    //}

    //[MenuItem("Build/BuildAndroid/Debug", false, 3)]
    //public static void BuildDebugAndroid()
    //{

    //}

    //[MenuItem("Build/BuildAndroid/Release", false, 4)]
    //public static void BuildReleaseAndroid()
    //{

    //}

    //[MenuItem("Build/BuildWindows/Debug", false, 5)]
    //public static void BuildDebugWindows()
    //{
    //    string platOutPutPath = PathHelper.Combine(mBinOutPutPath, "windows");
    //    if (Directory.Exists(platOutPutPath))
    //    {
    //        Directory.Delete(platOutPutPath, true);
    //    }
    //    BuildPipeline.BuildPlayer(new[]{mMainScenePath}, PathHelper.Combine(platOutPutPath, mAppName + ".exe"), BuildTarget.StandaloneWindows64, BuildOptions.Development|BuildOptions.ConnectWithProfiler);
    //    Directory.CreateDirectory(PathHelper.Combine(PathHelper.Combine(platOutPutPath, mAppName + "_Data"), "SdCardAssets"));
    //}

    //[MenuItem("Build/BuildWindows/Release", false, 6)]
    //public static void BuildReleaseWindows()
    //{
    //    string platOutPutPath = PathHelper.Combine(mBinOutPutPath, "windows");
    //    if (Directory.Exists(platOutPutPath))
    //    {
    //        Directory.Delete(platOutPutPath, true);
    //    }
    //    BuildPipeline.BuildPlayer(new[]{mMainScenePath}, PathHelper.Combine(platOutPutPath, mAppName + ".exe"), BuildTarget.StandaloneWindows64, BuildOptions.None);
    //    Directory.CreateDirectory(PathHelper.Combine(PathHelper.Combine(platOutPutPath, mAppName + "_Data"), "SdCardAssets"));
    //}

    [MenuItem("Build/BuildAssetsForIOS", false, 51)]
    public static void BuildAssetsForIOS()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS, BuildTarget.iOS);
        BuildAssets(BuildTarget.iOS);
        BuildScenes(BuildTarget.iOS);
    }

    [MenuItem("Build/BuildAssetsForAndroid", false, 52)]
    public static void BuildAssetsForAndroid()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        BuildAssets(BuildTarget.Android);
        BuildScenes(BuildTarget.Android);
    }

    [MenuItem("Build/BuildAssetsForWindows", false, 53)]
    public static void BuildAssetsForWindows()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
        BuildAssets(BuildTarget.StandaloneWindows64);
        BuildScenes(BuildTarget.StandaloneWindows64);
    }

    [MenuItem("Build/ClearAssets", false, 54)]
    public static void ClearAssets()
    {
        if (Directory.Exists(mOutPutPath))
        {
            Directory.Delete(mOutPutPath, true);
        }
        AssetDatabase.Refresh();
    }

    private static void BuildAssets(BuildTarget bt)
    {
        try
        {
            ClearAssets();
            Directory.CreateDirectory(mOutPutPath);
            BuildPipeline.BuildAssetBundles(mOutPutPath, BuildAssetBundleOptions.DeterministicAssetBundle, bt);
            Debug.Log("Assets Successfully builded at " + mOutPutPath);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private static void BuildScenes(BuildTarget bt)
    {
        string[] sceneFolderList = Directory.GetDirectories(mScenePath);
        string[] sceneFileList = new string[sceneFolderList.Length];
        string sceneOutputPath = PathHelper.Combine(mOutPutPath, PathHelper.Scenes);
        Directory.CreateDirectory(sceneOutputPath);
        for (int index = 0; index < sceneFolderList.Length; index++)
        {
            string[] files = Directory.GetFiles(sceneFolderList[index], "*.unity");
            if (files.Length >= 1)
                sceneFileList[index] = files[0];
        }
        for (int index = 0; index < sceneFileList.Length; index++)
        {
            string sceneOutputName = Path.GetFileNameWithoutExtension(sceneFileList[index]) + ".scene";
            BuildPipeline.BuildPlayer(new[] { sceneFileList[index] }, PathHelper.Combine(sceneOutputPath, sceneOutputName), bt, BuildOptions.BuildAdditionalStreamedScenes);
        }
    }
}