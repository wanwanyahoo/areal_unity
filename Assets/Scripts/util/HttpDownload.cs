﻿using System;
using BestHTTP;
using UnityEngine;
using System.IO;

public class HttpDownload
{
    public enum DownloadStatus
    {
        None,

        GetLength,

        Processing,

        Finished,

        Failed,
    }

    public delegate void OnResult(bool success, string key);
    public delegate void OnProgress(int cur, int total, string key);
    public DownloadStatus mCurStat{ get; set;}
    private HTTPRequest mRequest = null;

    private string mUrl;
    private string mKey;
    private string mDest;
    public int mTotalLength{ get; set;}
    public int mCurLength{ get; set;}
    public OnResult mOnResult{ get; set;}
    public OnProgress mOnProgress{ get; set;}
    private FileStream mFileStream;

    public HttpDownload(string url, string dest, string key)
    {
        mUrl = url;
        mDest = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true), dest);
        mKey = key;
        mCurStat = DownloadStatus.None;
        mCurLength = 0;
    }

    public void SetHeader(string key, string value)
    {
        if (mRequest != null)
        {
            mRequest.SetHeader(key, value);
        }
    }

    public void Start()
    {
        //先获取下载的总大小再开始下载
        GetContentLength();
    }


    //获取下载文件的总大小
    private void GetContentLength()
    {
        mCurStat = DownloadStatus.GetLength;
        mRequest = new HTTPRequest(new Uri(mUrl), (req, resp) =>
            {
                if (resp == null)
                {
                    Debug.LogError("Request for url " + mUrl + " error," + "resp = null");
                }
                else if (resp.StatusCode != 200)
                {
                    Debug.LogError("Request for url " + mUrl + " error," + "errorcode = " + resp.StatusCode + ", message = " + resp.Message);
                    mCurStat = DownloadStatus.Failed;
                    if(mOnResult != null)
                    {
                        Loom.DispatchToMainThread(()=>mOnResult(false, mKey));
                    }
                }
                else
                {
                    mTotalLength = int.Parse(resp.GetFirstHeaderValue("content-length"));
                    mCurStat = DownloadStatus.Processing;
                    mCurLength = GetFileSize(mDest);
                    if (mCurLength == mTotalLength)
                    {                
                        if(mOnResult != null)
                        {
                            Loom.DispatchToMainThread(()=>mOnResult(true, mKey));
                            return;
                        }
                    }
                    else if (mCurLength > mTotalLength)
                    {
                        File.Delete(mDest);
                        Debug.LogError("Request for url " + mUrl + " error," + "download file size error, delete local file!");
                    }
                    StartDownload();
                }
            });

        mRequest.DisableCache = true;
        mRequest.MethodType = HTTPMethods.Head;
        mRequest.Send();
    }


    //正式开始下载
    private void StartDownload()
    {
        mRequest = new HTTPRequest(new Uri(mUrl), (req, resp) =>
        {
            switch (req.State)
            {
                case HTTPRequestStates.Processing:
                        {
                            WriteFile(resp.GetStreamedFragments());
                            if(mOnProgress != null)
                            {
                                Loom.DispatchToMainThread(()=>mOnProgress(mCurLength, mTotalLength, mKey));
                            }
                        }
                    break;

                    // The request finished without any problem.
                case HTTPRequestStates.Finished:
                    if (resp.IsSuccess)
                    {
                        WriteFile(resp.GetStreamedFragments());

                        // Completly finished
                        if (resp.IsStreamingFinished)
                        {
                            if(mOnResult != null)
                            {

                                Loom.DispatchToMainThread(()=>mOnResult(true, mKey));
                            }
                            if (mFileStream != null)
                            {
                                mFileStream.Close();
                                mFileStream = null;
                            }
                            mRequest = null;
                        }
                        else
                            Debug.LogError( "Processing");
                    }
                    else
                    {
                            Debug.LogError(string.Format("Request finished Successfully, but the server sent an error. Status Code: {0}-{1} Message: {2}, url:{3}",
                                resp.StatusCode, resp.Message, resp.DataAsText, mUrl));
                        if(mOnResult != null)
                        {
                            Loom.DispatchToMainThread(()=>mOnResult(false, mKey));
                        }
                        if (mFileStream != null)
                        {
                            mFileStream.Close();
                            mFileStream = null;
                        }
                        mRequest = null;
                    }
                    break;

                    // The request finished with an unexpected error. The request's Exception property may contain more info about the error.
                case HTTPRequestStates.Error:
                        Debug.LogError("Request for url " + mUrl + " error," + "Request Finished with Error! " + (req.Exception != null ? (req.Exception.Message + "\n" + req.Exception.StackTrace) : "No Exception"));
                        if(mOnResult != null)
                        {
                            Loom.DispatchToMainThread(()=>mOnResult(false, mKey));
                        }
                        if (mFileStream != null)
                        {
                            mFileStream.Close();
                            mFileStream = null;
                        }
                        mRequest = null;
                    break;

                    // The request aborted, initiated by the user.
                case HTTPRequestStates.Aborted:
                        Debug.LogError("Request for url " + mUrl + " error," + "Request Aborted!");
                        if(mOnResult != null)
                        {
                            Loom.DispatchToMainThread(()=>mOnResult(false, mKey));
                        }
                        if (mFileStream != null)
                        {
                            mFileStream.Close();
                            mFileStream = null;
                        }
                        mRequest = null;
                    break;

                    // Ceonnecting to the server is timed out.
                case HTTPRequestStates.ConnectionTimedOut:
                        Debug.LogError("Request for url " + mUrl + " error," + "Connection Timed Out!");
                        if(mOnResult != null)
                        {
                            Loom.DispatchToMainThread(()=>mOnResult(false, mKey));
                        }
                        if (mFileStream != null)
                        {
                            mFileStream.Close();
                            mFileStream = null;
                        }
                        mRequest = null;
                    break;

                    // The request didn't finished in the given time.
                case HTTPRequestStates.TimedOut:
                        Debug.LogError("Request for url " + mUrl + " error," + "Processing the request Timed Out!");
                        if(mOnResult != null)
                        {
                            Loom.DispatchToMainThread(()=>mOnResult(false, mKey));
                        }
                        if (mFileStream != null)
                        {
                            mFileStream.Close();
                            mFileStream = null;
                        }
                        mRequest = null;
                    break;
            }
        }); 
        mRequest.UseStreaming = true;
        mRequest.DisableCache = true;
        mRequest.SetRangeHeader(mCurLength);
        mRequest.SetHeader("Content-Type", "application/octet-stream");
        mRequest.Send();
    }

    private void WriteFile(System.Collections.Generic.List<byte[]> fragments)
    {
        if (fragments != null && fragments.Count > 0)
        {
            for (int i = 0; i < fragments.Count; ++i)
            {
                if (mCurLength == 0 && !IsFileExist(mDest))
                {
                    CreateDirRecursive(mDest);
                }
                if (mFileStream == null)
                {
                    mFileStream = File.Open(mDest, FileMode.OpenOrCreate);
                    mFileStream.Seek(0, SeekOrigin.End);
                }
                mFileStream.Write(fragments[i], 0, fragments[i].Length);
                mFileStream.Flush();
                mCurLength += fragments[i].Length;
            }
        }
    }

    private bool IsFileExist(string path)
    {
        return File.Exists(path);
    }

    private int GetFileSize(string path)
    {
        if (!File.Exists(path))
            return 0;
        FileStream file = File.Open(path, FileMode.Open);
        int length  = (int)file.Length;
        file.Close();
        return length;
    }

    private void CreateDirRecursive(string path)
    {
        string dir = Path.GetDirectoryName(path);
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }
}