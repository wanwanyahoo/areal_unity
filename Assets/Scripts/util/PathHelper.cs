﻿using System;
using UnityEngine;

public class PathHelper
{
    public const string GameAssets = "GameAssets";
    public const string SdCardAssets = "SdCardAssets";
    public const string WWWPrefix = "file://";
    public const string Scenes = "scenes";
    public static string Combine(string path1, string path2)
    {
        if (path1.EndsWith("/"))
        {
            path1 = path1.Substring(0, path1.Length - 1);
        }
        if (path2 == null)
        {
            return path1;
        }
        return path1 + "/" + path2;
    }
    public static string GetAssetsInnerPath(bool abosulte = false, bool useWWW = false)
    {
#if UNITY_EDITOR
        string path = GameAssets;
        if (abosulte)
        {
            path = Combine(Application.dataPath, path);
        }
        else
        {
            path = Combine("Assets", path);
        }
        if (useWWW)
        {
            path = WWWPrefix + path;
        }
        return path;
#elif UNITY_STANDALONE_WIN
        string path = Application.streamingAssetsPath;
        if (useWWW)
        {
            path = Combine(WWWPrefix, path);
        }
        return path;
#elif UNITY_IPHONE
        string path = Application.streamingAssetsPath;
        if (useWWW)
        {
            path = Combine(WWWPrefix, path);
        }
        return path;
#elif UNITY_ANDROID
        return Application.streamingAssetsPath;
#else
        return ".";
#endif
    }
    public static string GetAssetsOutterPath(bool absolute = false, bool useWWW = false)
    {
#if UNITY_EDITOR
        string path = "StreamingAssets";
        if (absolute)
        {
            path = Combine(Application.dataPath, path);
        }
        else
        {
            path = Combine("Assets", path);
        }
        if (useWWW)
        {
            path = WWWPrefix + path;
        }
        return path;
#elif UNITY_STANDALONE_WIN
        string path = SdCardAssets;
        if (absolute)
        {
            path = Combine(Application.dataPath, path);
        }
        else
        {
            path = Combine("Assets", path);
        }
        if (useWWW)
        {
            path = Combine(WWWPrefix, path);
        }
        return path;
#elif UNITY_IPHONE
        string path = Application.persistentDataPath;
        if (useWWW)
        {
            path = Combine(WWWPrefix, path);
        }
        return path;
#elif UNITY_ANDROID
        return Application.persistentDataPath;
#else
        return ".";
#endif
    }
}
