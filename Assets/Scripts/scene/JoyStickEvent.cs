﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using HedgehogTeam.EasyTouch;

[XLua.LuaCallCSharp]
public class JoyStickEvent : MonoBehaviour
{

    [XLua.LuaCallCSharp]
    public static List<Type> JoyStickLuaCallCSharp = new List<Type>()
    {
        typeof(Vector2)
    };

    [XLua.CSharpCallLua]
    public delegate void MoveStartEvent();
    private MoveStartEvent onMoveStart = null;
    public MoveStartEvent OnMoveStart
    {
        set { onMoveStart = value; }
    }


    [XLua.CSharpCallLua]
    public delegate void MoveEvent(Vector2 move);
    private MoveEvent onMove = null;
    public MoveEvent OnMove
    {
        set { onMove = value; }
    }

    [XLua.CSharpCallLua]
    public delegate void MoveEndEvent();
    private MoveEndEvent onMoveEnd = null;
    public MoveEndEvent OnMoveEnd
    {
        set { onMoveEnd = value; }
    }


    public void MoveStart()
    {
        //Debug.Log("MoveStart");
        if (onMoveStart != null)
            onMoveStart();
    }

    public void Move(Vector2 move)
    {
        //Debug.Log("Move");
        if (onMove != null)
            onMove(move);
    }

    public void MoveSpeed(Vector2 move)
    {
        //Debug.Log("MoveSpeed");
    }

    public void MoveEnd()
    {
        //Debug.Log("MoveEnd");
        if (onMoveEnd != null)
            onMoveEnd();
    }
}