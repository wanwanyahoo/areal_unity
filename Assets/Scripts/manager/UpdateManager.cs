﻿using System;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib;

public class UpdateManager : Singleton<UpdateManager>
{
    public enum UpdateStatus
    {
        None,
        ReadLocalVersion,
        DownloadVersion,
        DownloadPatch,
        DownloadRes,
        Unzip,
        Finished,
    }
    public struct VersionMeta
    {
        public long version;
        public string[] versionUrl;
        public string[] resUrl;
    };
    public struct PatchMeta
    {
        public struct PatchInfo
        {
            public long fromVer;
            public long toVer;
            public string name;
        };
        public Dictionary<int, PatchInfo> patches;
        public int[] patchList;
    };
    public struct PatchDownloadInfo
    {
        public string destFile;
        public string url;
        public int cur;
        public int total;
        public bool finished; 
    };
    public delegate void LoadUpdateAssetCallback(string text);

    private int verDownloadIndex = 0;
    private int resDownloadIndex = 0;
    private VersionMeta localVersion;
    private VersionMeta remoteVerion;
    private PatchMeta patchMeta;
    private UpdateStatus updateStat;
    private Dictionary<string, PatchDownloadInfo> downloadInfos;
    private float downloadPercent;
    private string[] patches;
    private int curUnzip;
    private string outterPath;

    public override void Init()
    {
        updateStat = UpdateStatus.None;
        outterPath = PathHelper.GetAssetsOutterPath(true);
    }
    public void Run()
    {
        //TODO 强更检测
        GetLocalVersion();
    }
    IEnumerator ExecuteLoadVersionTask(string filename, LoadUpdateAssetCallback finishCallbackFunc)
    {
        string outterFilePath = PathHelper.Combine(PathHelper.GetAssetsOutterPath(true, true), filename);
        string innerFilePath = PathHelper.Combine(PathHelper.GetAssetsInnerPath(true, true), filename);
        WWW www = new WWW(outterFilePath);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
        {
            www.Dispose();
            www = new WWW(innerFilePath);
            yield return www;
        }
        if (string.IsNullOrEmpty(www.error) && www.isDone)
        {
            if (finishCallbackFunc != null)
            {
                finishCallbackFunc(www.text);
            }
            www.Dispose();
        }
        else
        {
            if (finishCallbackFunc != null)
            {
                finishCallbackFunc(null);
            }
            www.Dispose();
        }
    }
    public override void SelfUpdate()
    {
        if (UpdateStatus.DownloadRes == updateStat)
        {
            int num = downloadInfos.Count;
            int finishNum = 0;
            downloadPercent = 0;
            foreach (PatchDownloadInfo info in downloadInfos.Values)
            {
                if (info.finished)
                {
                    finishNum++;
                    downloadPercent += (float)1 / num;
                }
                else if (info.total != 0)
                {
                    downloadPercent += (float)info.cur / info.total / num;                
                }
            }
            if (finishNum == num)
            {
                updateStat = UpdateStatus.Unzip;
                curUnzip = 0;
                Unzip();
            }
        }
    }
    public void LoadVersionFinish(string text)
    {
        if (text == null)
        {
            return;
        }
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(text);
        if (UpdateStatus.ReadLocalVersion == updateStat)
        {
            localVersion.version = long.Parse(xmlDoc.DocumentElement.GetAttribute("version"));
            //version.txt地址
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("serverversion");
            int num = 0;
            localVersion.versionUrl = new string[nodeList.Count];
            foreach (XmlElement element in nodeList)
            {
                localVersion.versionUrl[num++] = element.GetAttribute("url");
            }

            GetRemoteVersion();
        }
        else if (UpdateStatus.DownloadVersion == updateStat)
        {
            remoteVerion.version = long.Parse(xmlDoc.DocumentElement.GetAttribute("version"));
            //version.txt地址
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("serverversion");
            int num = 0;
            remoteVerion.versionUrl = new string[nodeList.Count];
            foreach (XmlElement element in nodeList)
            {
                remoteVerion.versionUrl[num++] = element.GetAttribute("url");
            }

            //资源下载地址
            nodeList = xmlDoc.GetElementsByTagName("server");
            num = 0;
            remoteVerion.resUrl = new string[nodeList.Count];
            foreach (XmlElement element in nodeList)
            {
                remoteVerion.resUrl[num++] = element.GetAttribute("url");
            }

            if (localVersion.version < remoteVerion.version)
            {
                verDownloadIndex = 0;
                GetPatchInfo();
            }
            else
            {
                updateStat = UpdateStatus.Finished;
            }
        }
        else
        {
            Debug.LogError("load version finish, but status error!!!!");
        }
    }
    public void LoadPatchFinish(string text)
    {
        if (text == null)
        {
            return;
        }
        if (UpdateStatus.DownloadPatch == updateStat)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(text);
            XmlNodeList nodeList = xmlDoc.SelectNodes("/data/patches/patch");
            patchMeta.patches = new Dictionary<int, PatchMeta.PatchInfo>();
            foreach (XmlElement element in nodeList)
            {
                PatchMeta.PatchInfo info = new PatchMeta.PatchInfo();
                info.fromVer = long.Parse(element.GetAttribute("from"));
                info.toVer = long.Parse(element.GetAttribute("to"));
                info.name = element.GetAttribute("name");
                patchMeta.patches.Add(int.Parse(element.GetAttribute("id")), info);
            }
            nodeList = xmlDoc.SelectNodes("/data/updates/update");
            foreach (XmlElement element in nodeList)
            {
                string ids = element.GetAttribute("use");
                string[] list = ids.Split(',');
                patchMeta.patchList = new int[list.Length];
                for (int num =0; num < list.Length; num++)
                {
                    patchMeta.patchList[num] = int.Parse(list[num]);
                }
            }
            DownloadResources();
        }
        else
        {
            Debug.LogError("load patch finish, but status error!!!!");
        }
    }
    public void DownloadResult(bool success, string key)
    {
        if (key.CompareTo("version") == 0)
        {
            if (success)
            {
                StartCoroutine(ExecuteLoadVersionTask("temp/version.txt", LoadVersionFinish));
            }
            else
            {
                verDownloadIndex++;
                if (verDownloadIndex >= localVersion.versionUrl.Length)
                {
                    //TODO download version.txt failed
                    Debug.LogError("download version.txt failed!!!!!");
                }
                else
                {
                    GetRemoteVersion();
                }
            }
        }
        else if (key.CompareTo("patch") == 0)
        {
            if (success)
            {
                StartCoroutine(ExecuteLoadVersionTask("temp/patch.txt", LoadPatchFinish));
            }
            else
            {
                verDownloadIndex++;
                if (verDownloadIndex >= localVersion.versionUrl.Length)
                {
                    //TODO download patch.txt failed
                    Debug.LogError("download patch.txt failed!!!!!");
                }
                else
                {
                    GetPatchInfo();
                }
            }
        }
        else
        {
            if (success)
            {
                Debug.Log("key = " + key + " download success");
                PatchDownloadInfo downloadInfo = downloadInfos[key];
                downloadInfo.finished = true;
                downloadInfos[key] = downloadInfo;
            }
            else
            {
                resDownloadIndex++;
                if (resDownloadIndex >= remoteVerion.resUrl.Length)
                {
                    //TODO download patches failed
                    Debug.LogError("download patches failed!!!!!");
                }
                else
                {
                    PatchDownloadInfo downloadInfo = downloadInfos[key];
                    HttpDownload download = new HttpDownload(downloadInfo.url, downloadInfo.destFile, downloadInfo.destFile);
                    download.mOnResult = DownloadResult;
                    download.mOnProgress = DownloadProgress;
                    download.Start();
                }
            }
        }
    }
    public void DownloadProgress(int cur, int total, string key)
    {
        if (UpdateStatus.DownloadRes != updateStat)
        {
            Debug.LogError("UpdateManager DownloadProgress, but status error!!!!");
            return;
        }
        PatchDownloadInfo downloadInfo = downloadInfos[key];
        downloadInfo.cur = cur;
        downloadInfo.total = total;
        downloadInfos[key] = downloadInfo;
    }
    //读取本地version.txt
    private void GetLocalVersion()
    {
        updateStat = UpdateStatus.ReadLocalVersion;
        StartCoroutine(ExecuteLoadVersionTask("version.txt", LoadVersionFinish));
    }
    //获取最新的version.txt
    private void GetRemoteVersion()
    {
        updateStat = UpdateStatus.DownloadVersion;
        HttpDownload download = new HttpDownload(localVersion.versionUrl[verDownloadIndex] + "version.txt", "temp/version.txt", "version");
        download.mOnResult = DownloadResult;
        download.Start();
    }
    //获取patch.txt
    private void GetPatchInfo()
    {
        updateStat = UpdateStatus.DownloadPatch;
        HttpDownload download = new HttpDownload(remoteVerion.versionUrl[verDownloadIndex] + "patch.txt", "temp/patch.txt", "patch");
        download.mOnResult = DownloadResult;
        download.Start();
    }
    private void DownloadResources()
    {
        updateStat = UpdateStatus.DownloadRes;
        long cur = localVersion.version;
        long to = remoteVerion.version;
        int num = 0;
        patches = new string[patchMeta.patchList.Length];
        downloadInfos = new Dictionary<string, PatchDownloadInfo>();
        for (int i = 0; i < patchMeta.patchList.Length; i++)
        {
            PatchMeta.PatchInfo patchInfo = patchMeta.patches[patchMeta.patchList[i]];
            if (patchInfo.toVer <= cur)
            {
                Debug.Log("patch id = " + patchMeta.patchList[i] + ", no need to download");
            }
            else if (patchInfo.fromVer >= to)
            {
                break;
            }
            else
            {
                PatchDownloadInfo downloadInfo = new PatchDownloadInfo();
                downloadInfo.destFile = "temp" + patchInfo.name;
                downloadInfo.finished = false;
                downloadInfo.url = remoteVerion.resUrl[resDownloadIndex] + patchInfo.name;
                downloadInfos.Add(downloadInfo.destFile, downloadInfo);

                patches[num] = PathHelper.Combine(outterPath, downloadInfo.destFile);
                num++;

                HttpDownload download = new HttpDownload(downloadInfo.url, downloadInfo.destFile, downloadInfo.destFile);
                download.mOnResult = DownloadResult;
                download.mOnProgress = DownloadProgress;
                download.Start();
            }
        }
    }
    private void Unzip()
    {
        if (curUnzip < patches.Length && patches[curUnzip] != null)
        {
            Loom.StartSingleThread(AsyncUnzip);
        }
        else
        {
            CopyVersion();
        }
    }
    private void AsyncUnzip()
    {
        bool result = ZipUtility.UnzipFile(patches[curUnzip], outterPath);
        Loom.DispatchToMainThread(()=>UnzipResult(result));
    }
    private void UnzipResult(bool result)
    {
        if (!result)
        {
            //TODO unzip file error
            return;
        }
        curUnzip++;
        Unzip();
    }
    private void CopyVersion()
    {
        File.Copy(PathHelper.Combine(outterPath, "temp/version.txt"), PathHelper.Combine(outterPath, "version.txt"), true);
        Directory.Delete(PathHelper.Combine(outterPath, "temp"), true);
        updateStat = UpdateStatus.Finished;
    }
}
