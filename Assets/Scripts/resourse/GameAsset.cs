﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using UnityEngine;
using XLua;

[LuaCallCSharp]
public class GameAsset
{
    private string mAssetPath;
    private Object[] mAssetObjects;
    private WWW mWebStream;
    private AssetBundle mAssetBundlet;
    private string[] mDepends;
    private int mRefUseCnt;
    
    public string assetPath
    {
        get { return mAssetPath; }
        set { mAssetPath = value; }
    }
    public Object assetObject
    {
        get { return mAssetObjects[0]; }
        set { mAssetObjects[0] = value; }
    }
    public Object[] assetObjects
    {
        get { return mAssetObjects; }
        set { mAssetObjects = value; }
    }
    public WWW webStream
    {
        get { return mWebStream; }
        set { mWebStream = value; }
    }
    public AssetBundle assetBundle
    {
        get { return mAssetBundlet; }
        set { mAssetBundlet = value; }
    }
    public string[] depends
    {
        get { return mDepends; }
        set { mDepends = value; }
    }
    public int refUseCnt
    {
        get { return mRefUseCnt; }
        set { mRefUseCnt = value; }
    }
    public GameAsset()
    {
        mAssetObjects = new Object[1];
    }
}