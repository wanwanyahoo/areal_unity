﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using limax.endpoint;
using limax.endpoint.variant;
using limax.net;

namespace ConsoleApplication1
{
    public class UserViewHandler : TemporaryViewHandler
    {
        private readonly VariantView userSessionView;
        public UserViewHandler(VariantView v) { userSessionView = v; }
        public void onOpen(VariantView view, ICollection<long> sessionids)
        {
            Console.WriteLine(view + " onOpen " + sessionids);
            view.registerListener(e => Console.WriteLine(e));
            Variant param = Variant.createStruct();
            param.setValue("var0", 99999);
            userSessionView.sendControl("control", param);
        }
        public void onClose(VariantView view) { Console.WriteLine(view + " onClose"); }
        public void onAttach(VariantView view, long sessionid) { }
        public void onDetach(VariantView view, long sessionid, int reason) { }
    }


    public class EL : EndpointListener
    {
        public void onAbort(Transport transport)
        {
            Console.WriteLine("onAbort");
        }

        public void onErrorOccured(int source, int code, Exception exception)
        {
            Console.WriteLine("onErrorOccured");
            Console.WriteLine("error : source = " + source + " code = " + code + " e = " + exception);
        }

        public void onKeepAlived(int ms)
        {
            Console.WriteLine("onKeepAlived");
        }

        public void onKeyExchangeDone()
        {
            Console.WriteLine("onKeyExchangeDone");
        }

        public void onManagerInitialized(Manager manager, Config config)
        {
            Console.WriteLine("onManagerInitialized");
        }

        public void onManagerUninitialized(Manager manager)
        {
            Console.WriteLine("onManagerUninitialized");
        }

        public void onSocketConnected()
        {
            Console.WriteLine("onSocketConnected");
        }

        public void onReceivedLuaProtocol(Protocol p)
        {
        }

        public void onTransportAdded(Transport transport)
        {
            Console.WriteLine("onTransportAdded");
            VariantManager manager = VariantManager.getInstance((EndpointManager)transport.getManager(), 10001);
            VariantView userView = manager.getSessionOrGlobalView("users.UserView");
            userView.registerListener("readyFlag", e =>
            {
                manager.sendMessage(userView, "lastrole");
            });
            userView.registerListener("loginconfig", e =>
            {
                Console.WriteLine(e);
            });
        }

        public void onTransportRemoved(Transport transport)
        {
            Console.WriteLine("onTransportRemoved");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Endpoint.openEngine();
            EndpointConfig config = Endpoint.createLoginConfigBuilder("10.237.181.67", 11000, "1$wuyao_unity", "123456", "test")
                .variantProviderIds(10001).build();
            Endpoint.start(config, new EL());

            while (true)
            {

            }
        }
    }
}
