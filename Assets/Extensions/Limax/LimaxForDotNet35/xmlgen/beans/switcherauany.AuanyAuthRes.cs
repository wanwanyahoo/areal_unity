using System;
using System.Collections.Generic;
using limax.codec;
using limax.util;

namespace limax.switcherauany
{
	public sealed class AuanyAuthRes : limax.codec.Marshal, IComparable<AuanyAuthRes>
	{
		public int errorSource;
		public int errorCode;
		public long sessionid;
		public long mainid;
		public string uid;
		public long flags;

		public AuanyAuthRes()
		{
			errorSource = 0;
			errorCode = 0;
			sessionid = 0;
			mainid = 0;
			uid = "";
			flags = 0;
		}

		public AuanyAuthRes(int _errorSource_, int _errorCode_, long _sessionid_, long _mainid_, string _uid_, long _flags_)
		{
			this.errorSource = _errorSource_;
			this.errorCode = _errorCode_;
			this.sessionid = _sessionid_;
			this.mainid = _mainid_;
			this.uid = _uid_;
			this.flags = _flags_;
		}

		public OctetsStream marshal(OctetsStream _os_)
		{
			_os_.marshal(this.errorSource);
			_os_.marshal(this.errorCode);
			_os_.marshal(this.sessionid);
			_os_.marshal(this.mainid);
			_os_.marshal(this.uid);
			_os_.marshal(this.flags);
			return _os_;
		}

		public OctetsStream unmarshal(OctetsStream _os_)
		{
			this.errorSource = _os_.unmarshal_int();
			this.errorCode = _os_.unmarshal_int();
			this.sessionid = _os_.unmarshal_long();
			this.mainid = _os_.unmarshal_long();
			this.uid = _os_.unmarshal_string();
			this.flags = _os_.unmarshal_long();
			return _os_;
		}

		public int CompareTo(AuanyAuthRes __o__)
		{
			if (__o__ == this)
				 return 0;
			int __c__ = 0;
			__c__ = this.errorSource.CompareTo( __o__.errorSource);
			if (0 != __c__)
				return __c__;
			__c__ = this.errorCode.CompareTo( __o__.errorCode);
			if (0 != __c__)
				return __c__;
			__c__ = this.sessionid.CompareTo( __o__.sessionid);
			if (0 != __c__)
				return __c__;
			__c__ = this.mainid.CompareTo( __o__.mainid);
			if (0 != __c__)
				return __c__;
			__c__ = this.uid.CompareTo( __o__.uid);
			if (0 != __c__)
				return __c__;
			__c__ = this.flags.CompareTo( __o__.flags);
			if (0 != __c__)
				return __c__;
			return __c__;
		}

		public override bool Equals(object __o1__)
		{
			if(__o1__ == this)
				return true;
			if(__o1__.GetType() != this.GetType())
				return false;
			AuanyAuthRes __o__ = (AuanyAuthRes)__o1__;
			if (!Utils.equals(this.errorSource, __o__.errorSource))
				return false;
			if (!Utils.equals(this.errorCode, __o__.errorCode))
				return false;
			if (!Utils.equals(this.sessionid, __o__.sessionid))
				return false;
			if (!Utils.equals(this.mainid, __o__.mainid))
				return false;
			if (!Utils.equals(this.uid, __o__.uid))
				return false;
			if (!Utils.equals(this.flags, __o__.flags))
				return false;
			return true;
		}

		public override int GetHashCode()
		{
			int __h__ = 0;
			__h__ += __h__ * 31 + this.errorSource.GetHashCode();
			__h__ += __h__ * 31 + this.errorCode.GetHashCode();
			__h__ += __h__ * 31 + this.sessionid.GetHashCode();
			__h__ += __h__ * 31 + this.mainid.GetHashCode();
			__h__ += __h__ * 31 + this.uid.GetHashCode();
			__h__ += __h__ * 31 + this.flags.GetHashCode();
			return __h__;
		}

	}
}

