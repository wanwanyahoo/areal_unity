using System;
using System.Collections.Generic;
using limax.codec;
using limax.util;

namespace limax.endpoint.providerendpoint
{

	public sealed partial class LuaProtocol : limax.net.Protocol
	{

		public int TYPE;

		override public int getType()
		{
			return TYPE;
		}

        public Octets data;

		public LuaProtocol(int _type)
		{
            TYPE = _type;
		}


		public override OctetsStream marshal(OctetsStream _os_)
		{
			_os_.marshal(this.data);
			return _os_;
		}

		public override OctetsStream unmarshal(OctetsStream _os_)
		{
			this.data = _os_.unmarshal_Octets();
			return _os_;
		}


	}
}

