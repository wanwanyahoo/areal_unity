using System;
using System.Collections;
using System.Collections.Generic;

using XLua;

namespace limax.endpoint.script
{
    public class LuaScriptHandle : ScriptEngineHandle
    {
        private readonly DictionaryCache cache;
        private readonly HashSet<int> providers = new HashSet<int>();
        private readonly LimaxActionDelegate instance;

        [CSharpCallLua]
        public delegate long LimaxActionDelegate(int t, Object p);
        [CSharpCallLua]
        public delegate LimaxActionDelegate LimaxInitDelegate();
        [CSharpCallLua]
        public delegate LuaTable LimaxGetPvidsDelegate();

        public LuaScriptHandle()
        {  
            LimaxGetPvidsDelegate getpvidsdelegate = LuaManager.Instance.Env.Global.Get<LuaTable>("LimaxViewManager").Get<LimaxGetPvidsDelegate>("GetPvids");
            LuaTable r = getpvidsdelegate();

            r.ForEach<int, int>((int key, int value) => { this.providers.Add((int)(long)value); });
    

            LimaxInitDelegate initdelegate = LuaManager.Instance.Env.Global.Get<LuaTable>("LimaxViewManager").Get<LimaxInitDelegate>("Init");
            this.instance = initdelegate();
        }

        public HashSet<int> getProviders()
        {
            return providers;
        }
        public int action(int t, object p)
        {
            return (int)(long)instance(t, p);
        }
        private delegate Exception Send(string s);
        public void registerScriptSender(ScriptSender sender)
        {
            instance(0, (Send)sender.send);
        }
        public ICollection<string> getDictionaryKeys()
        {
            return cache != null ? cache.keys() : new List<string>();
        }
    }
}
