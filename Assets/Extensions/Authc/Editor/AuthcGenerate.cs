﻿using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEditor;

public class AuthcGenerate
{
    const string processFolderPath = "/" + PathHelper.GameAssets + "/lua/authc/process";
    [MenuItem("Authc/Generate")]
    public static void Generate()
    {
        string projectPath = System.IO.Directory.GetParent(Application.dataPath).ToString();
        Process.Start(projectPath + "/Generate/authc/generate.bat");
    }

    [MenuItem("Authc/Clear")]
    public static void Clear()
    {
        string projectPath = System.IO.Directory.GetParent(Application.dataPath).ToString();
        Process.Start(projectPath + "/Generate/authc/clear.bat");
    }

    [InitializeOnLoadMethod]
    public static void GenerateProcessLua()
    {
        DirectoryInfo processFolder = new DirectoryInfo(Application.dataPath + processFolderPath);
        List<FileInfo> fileList = new List<FileInfo>();
        GetFilesInFolder(processFolder, fileList);

        FileStream fs = new FileStream(Application.dataPath + processFolderPath + "/list.lua.txt", FileMode.OpenOrCreate, FileAccess.Write);
        string head = "return {\n";
        fs.Write(Encoding.UTF8.GetBytes(head), 0, Encoding.UTF8.GetByteCount(head));
        foreach (FileInfo file in fileList)
        {
            string protocolName = file.FullName.Substring(processFolder.FullName.Length + 1).Replace("\\", ".").Replace(".lua.txt", "");
            string write = "\t[\"" + protocolName + "\"] = 1,\n";
            fs.Write(Encoding.UTF8.GetBytes(write), 0, Encoding.UTF8.GetByteCount(write));
        }
        string end = "}\n";
        fs.Write(Encoding.UTF8.GetBytes(end), 0, Encoding.UTF8.GetByteCount(end));
        fs.Close();
    }

    public static void GetFilesInFolder(DirectoryInfo dirInfo, List<FileInfo> fileList)
    {
        foreach (FileInfo file in dirInfo.GetFiles())
        {
            if (file.Name.EndsWith(".lua.txt"))
            {
                fileList.Add(file);
            }
        }

        foreach (DirectoryInfo dir in dirInfo.GetDirectories())
        {
            GetFilesInFolder(dir, fileList);
        }
    }
}
