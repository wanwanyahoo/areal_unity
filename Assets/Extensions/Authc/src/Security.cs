﻿namespace Authc
{
    public abstract class Security
    {
        public virtual void SetParameter(Octets o) { }
        public virtual void GetParameter(Octets o) { }
        public virtual Octets Update(Octets o) { return o; }
        public virtual Octets Final(Octets o) { return o; }
    }
}
