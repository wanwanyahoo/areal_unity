﻿using System;
using System.Text;

namespace Authc
{
    class MarshalException : ApplicationException
    {
        public MarshalException(string message) : base(message)
        {
        }
    }

    public class OctetsStream
    {
        private Octets _data;
        private int _pos;

        public int Length
        {
            get
            {
                return _data.Length;
            }
        }

        public int Position
        {
            get
            {
                return _pos;
            }
        }

        public OctetsStream()
        {
            _pos = 0;
            _data = new Octets();
        }

        public OctetsStream(Octets x)
        {
            _pos = 0;
            _data = new Octets(x);
        }

        public Octets Octets()
        {
            return _data;
        }

        public UInt32 UncompactUInt32()
        {
            int pos = _pos;
            if (_pos < 0 || _pos >= _data.Length)
            {
                throw new MarshalException("UncompactUInt32 1");
            }
            try
            {
                switch (_data.Buffer[_pos] & 0xe0)
                {
                    case 0xe0:
                        UnmarshalByte();
                        return UnmarshalUInt32();
                    case 0xc0:
                        return UnmarshalUInt32() & ~0xc0000000;
                    case 0xa0:
                    case 0x80:
                        return (UInt32)(UnmarshalUInt16() & ~0x8000);
                    default:
                        return (UInt32)UnmarshalByte();
                }
            }
            catch (MarshalException)
            {
                _pos = pos; // 还原
                throw new MarshalException("UncompactUInt32 2");
            }
        }

        public void CompactUInt32(UInt32 x)
        {
            if (x < 0x80)
            {
                MarshalByte((byte)x);
            }
            else if (x < 0x4000)
            {
                MarshalUInt16((UInt16)(x|0x8000));
            }
            else if (x < 0x20000000)
            {
                MarshalUInt32((UInt32)(x | 0xc0000000));
            }
            else
            {
                MarshalByte(0xe0);
                MarshalUInt32(x);
            }
        }

        public byte UnmarshalByte()
        {
            if (_pos < 0 || _pos >= _data.Length)
            {
                throw new MarshalException("UnmarshalByte");
            }
            byte r = _data.Buffer[_pos];
            _pos += 1;
            return r;
        }

        public void MarshalByte(byte x)
        {
            _data.Insert(BitConverter.GetBytes(x), 1, _data.Length);
        }

        public byte[] UnmarshalByteArray(int length)
        {
            if (length <= 0 || _pos < 0 || _pos > _data.Length - length)
            {
                throw new MarshalException("UnmarshalByteArray _pos = " + _pos + ", length = " + length + ", _data.Length = " + _data.Length);
            }
            byte[] r = new byte[length];
            Array.Copy(_data.Buffer, _pos, r, 0, length);
            _pos += length;
            return r;
        }

        public Int32 UnmarshalInt32()
        {
            Int32 r = 0;
            try
            {
                r = BitConverter.ToInt32(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalInt32");
            }
            byte[] array = BitConverter.GetBytes(r);
            Array.Reverse(array);
            r = BitConverter.ToInt32(array, 0);
            _pos += 4;
            return r;
        }

        public void MarshalInt32(Int32 x)
        {
            byte[] array = BitConverter.GetBytes(x);
            Array.Reverse(array);
            _data.Insert(array, 4, _data.Length);
        }

        public UInt32 UnmarshalUInt32()
        {
            UInt32 r = 0;
            try
            {
                r = BitConverter.ToUInt32(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalUInt32");
            }
            byte[] array = BitConverter.GetBytes(r);
            Array.Reverse(array);
            r = BitConverter.ToUInt32(array, 0);
            _pos += 4;
            return r;
        }

        public void MarshalUInt32(UInt32 x)
        {
            byte[] array = BitConverter.GetBytes(x);
            Array.Reverse(array);
            _data.Insert(array, 4, _data.Length);
        }

        public UInt16 UnmarshalUInt16()
        {
            UInt16 r = 0;
            try
            {
                r = BitConverter.ToUInt16(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalUInt16");
            }
            byte[] array = BitConverter.GetBytes(r);
            Array.Reverse(array);
            r = BitConverter.ToUInt16(array, 0);
            _pos += 2;
            return r;
        }

        public void MarshalUInt16(UInt16 x)
        {
            byte[] array = BitConverter.GetBytes(x);
            Array.Reverse(array);
            _data.Insert(array, 2, _data.Length);
        }

        public Octets UnmarshalOctets()
        {
            int length = 0;
            int pos = _pos;
            try
            {
                length = (int)UncompactUInt32();
            }
            catch (MarshalException)
            {
                throw new MarshalException("UnmarshalOctets UncompactUInt32");
            }
            if (length < 0 || length > _data.Length - _pos)
            {
                _pos = pos; // 还原
                throw new MarshalException("UnmarshalOctets _pos = " + _pos + ", length = " + length + ", _data.Length = " + _data.Length);
            }
            Octets r = new Octets(_data.Buffer, _pos, length);
            _pos += length;
            return r;
        }

        public Octets UnmarshalOctets(int length)
        {
            if (length < 0 || length > _data.Length - _pos)
            {
                throw new MarshalException("UnmarshalOctets _pos = " + _pos + ", length = " + length + ", _data.Length = " + _data.Length);
            }
            Octets r = new Octets(_data.Buffer, _pos, length);
            _pos += length;
            return r;
        }

        public void MarshalOctets(Octets x)
        {
            CompactUInt32((UInt32)x.Length);
            _data.Insert(x.Buffer, x.Length, _data.Length);
        }

        public char UnmarshalChar()
        {
            char r = ' ';
            try
            {
                r = BitConverter.ToChar(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalChar");
            }
            _pos += 1;
            return r;
        }

        public void MarshalChar(char x)
        {
            _data.Insert(BitConverter.GetBytes(x), 1, _data.Length);
        }

        public string UnmarshalString()
        {
            int length = 0;
            int pos = _pos;
            try
            {
                length = (int)UncompactUInt32();
            }
            catch (MarshalException)
            {
                throw new MarshalException("UnmarshalString UncompactUInt32");
            }
            if (length < 0 || length > _data.Length - _pos)
            {
                _pos = pos; // 还原
                throw new MarshalException("UnmarshalString _pos = " + _pos + ", length = " + length + ", _data.Length = " + _data.Length);
            }
            if (length == 0)
            {
                return "";
            }
            string r = Encoding.Unicode.GetString(_data.Buffer, _pos, length);
            _pos += length;
            return r;
        }

        public void MarshalString(string x)
        {
            CompactUInt32((UInt32)Encoding.Unicode.GetByteCount(x));
            _data.Insert(Encoding.Unicode.GetBytes(x), Encoding.Unicode.GetByteCount(x));
        }

        public float UnmarshalFloat()
        {
            float r = 0;
            try
            {
                r = BitConverter.ToSingle(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalFloat");
            }
            byte[] array = BitConverter.GetBytes(r);
            Array.Reverse(array);
            r = BitConverter.ToSingle(array, 0);
            _pos += 4;
            return r;
        }

        public void MarshalFloat(float x)
        {
            byte[] array = BitConverter.GetBytes(x);
            Array.Reverse(array);
            _data.Insert(array, 4, _data.Length);
        }

        public Int64 UnmarshalInt64()
        {
            Int64 r = 0;
            try
            {
                r = BitConverter.ToInt64(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalInt64");
            }
            byte[] array = BitConverter.GetBytes(r);
            Array.Reverse(array);
            r = BitConverter.ToInt64(array, 0);
            _pos += 8;
            return r;
        }

        public void MarshalInt64(Int64 x)
        {
            byte[] array = BitConverter.GetBytes(x);
            Array.Reverse(array);
            _data.Insert(array, 8, _data.Length);
        }

        public UInt64 UnmarshalUInt64()
        {
            UInt64 r = 0;
            try
            {
                r = BitConverter.ToUInt64(_data.Buffer, _pos);
            }
            catch (IndexOutOfRangeException)
            {
                throw new MarshalException("UnmarshalUInt64");
            }
            byte[] array = BitConverter.GetBytes(r);
            Array.Reverse(array);
            r = BitConverter.ToUInt64(array, 0);
            _pos += 8;
            return r;
        }

        public void MarshalUInt64(UInt64 x)
        {
            byte[] array = BitConverter.GetBytes(x);
            Array.Reverse(array);
            _data.Insert(array, 8, _data.Length);
        }
    }
}
