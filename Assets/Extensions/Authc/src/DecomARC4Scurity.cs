﻿namespace Authc
{
    class DecomARC4Scurity : Security
    {
        private ARCFourSecurity arc4 = new ARCFourSecurity();
        private MPPCDecompress decom = new MPPCDecompress();

        public DecomARC4Scurity() { }

        public override void SetParameter(Octets o)
        {
            arc4.SetParameter(o);
        }

        public override Octets Update(Octets o)
        {
            return decom.Update(arc4.Update(o));
        }
    }
}
