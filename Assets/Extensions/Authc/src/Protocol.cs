﻿using System;

namespace Authc
{
    public class Protocol
    {
        private int type = 0;
        private Octets data = new Octets();

        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public Octets Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }
    }
}
