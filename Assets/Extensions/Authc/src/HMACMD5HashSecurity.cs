﻿using System.Security.Cryptography;

namespace Authc
{
    class HMACMD5HashSecurity : Security
    {
        private HMACMD5 hmac = new HMACMD5();

        public HMACMD5HashSecurity() { }

        public override void SetParameter(Octets o)
        {
            hmac = new HMACMD5(o.Buffer);
        }

        public override Octets Update(Octets o)
        {
            byte[] ooo = new byte[1024];
            if (hmac != null)
            {
                hmac.TransformBlock(o.Buffer, 0, o.Length, ooo, 0);
            }
            return o;
        }

        public override Octets Final(Octets digest)
        {
            if (hmac != null)
            {
                digest.Clear();
                hmac.TransformFinalBlock(new byte[0], 0, 0);
                digest.Insert(hmac.Hash, hmac.HashSize / 8);
            }
            return digest;
        }
    }
}
