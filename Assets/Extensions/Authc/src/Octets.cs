﻿using System;

namespace Authc
{
    public class Octets
    {
        private byte[] _buffer;
        private int _length;
        private int _capacity;

        public byte[] Buffer
        {
            get
            {
                return _buffer;
            }
        }

        public int Length
        {
            get
            {
                return _length;
            }
            set
            {
                _length = value;
            }
        }

        public int Capacity
        {
            get
            {
                return _capacity;
            }
        }

        public Octets()
        {
            _length = 0;
            _capacity = 0;
            _buffer = null;
        }

        public Octets(int size)
        {
            _length = 0;
            _capacity = size;
            _buffer = new byte[size];
        }

        public Octets(byte[] data, int size)
        {
            _length = size;
            _capacity = size;
            _buffer = new byte[size];
            Array.Copy(data, _buffer, size);
        }

        public Octets(byte[] data, int pos, int size)
        {
            _length = size;
            _capacity = size;
            _buffer = new byte[size];
            Array.Copy(data, pos, _buffer, 0, size);
        }

        public Octets(Octets x)
        {
            _length = x._length;
            _capacity = x._capacity;
            _buffer = x._buffer;
        }

        public void Insert(byte[] data, int length)
        {
            if (data != null && length > 0)
            {
                Resize(this._length + length);
                Array.Copy(data, 0, _buffer, this._length, length);
                this._length = this._length + length;
            }
        }

        public void Insert(byte[] data, int length, int pos)
        {
            if (data != null && length > 0)
            {
                Resize(this._length + length);
                if (pos < this._length)
                {
                    Array.Copy(_buffer, pos, _buffer, pos + length, this._length - pos);
                }
                Array.Copy(data, 0, _buffer, pos, length);
                this._length = this._length + length;
            }
        }

        public void Insert(byte[] data, int start, int length, int pos)
        {
            if (data != null && length > 0)
            {
                Resize(this._length + length);
                if (pos < this._length)
                {
                    Array.Copy(_buffer, pos, _buffer, pos + length, this._length - pos);
                }
                Array.Copy(data, start, _buffer, pos, length);
                this._length = this._length + length;
            }
        }

        public void Resize(int size)
        {
            if (_capacity < size)
            {
                if (_buffer != null)
                {
                    Array.Resize<byte>(ref _buffer, size);
                }
                else
                {
                    _buffer = new byte[size];
                }
                _capacity = size;
            }
        }

        public void Clear()
        {
            _length = 0;
        }

        public void Erase(int pos, int length)
        {
            Array.Copy(_buffer, pos + length, _buffer, pos, this._length - pos - length);
            this._length -= length;
        }
    }
}
