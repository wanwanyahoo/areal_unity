﻿using System.Security.Cryptography;

namespace Authc
{
    class MD5HashSecurity : Security
    {
        private MD5 md5 = new MD5CryptoServiceProvider();

        public MD5HashSecurity() { }

        public override Octets Update(Octets o)
        {
            if (md5 != null)
            {
                md5.TransformBlock(o.Buffer, 0, o.Length, o.Buffer, 0);
            }
            return o;
        }

        public override Octets Final(Octets digest)
        {
            if (md5 != null)
            {
                digest.Clear();
                md5.TransformFinalBlock(new byte[0], 0, 0);
                digest.Insert(md5.Hash, md5.HashSize / 8);
            }
            return digest;
        }
    }
}
