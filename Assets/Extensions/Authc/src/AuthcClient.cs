﻿using System;
using System.Net.Sockets;

namespace Authc
{
    public class AuthcClient
    {
        public delegate void ReceiveProtocolDelegate(Protocol p);
        public delegate void SocketExceptionDelegate(int code, string message);
        public delegate void SocketOnCloseDelegate();

        private string _ip;
        private int _port;
        private Session _session;
        private ReceiveProtocolDelegate _receiveDelegate;
        private SocketExceptionDelegate _exceptionDelegate;
        private SocketOnCloseDelegate _onCloseDelegate;

        public ReceiveProtocolDelegate ReceiveDelegate
        {
            set { _receiveDelegate = value; }
            get { return _receiveDelegate; }
        }

        public SocketOnCloseDelegate OnCloseDelegate
        {
            set { _onCloseDelegate = value; }
            get { return _onCloseDelegate; }
        }

        public Security InSecurity
        {
            set
            {
                if (_session != null)
                {
                    _session.InSecurity = value;
                }
            }
        }

        public Security OutSecurity
        {
            set
            {
                if (_session != null)
                {
                    _session.OutSecurity = value;
                }
            }
        }

        public SocketExceptionDelegate ExceptionDelegate
        {
            get
            {
                return _exceptionDelegate;
            }

            set
            {
                _exceptionDelegate = value;
            }
        }

        public AuthcClient(String ip, int port)
        {
            _ip = ip;
            _port = port;
        }

        public void Run()
        {
            _session = new Session(_ip, _port);

            _session.ReceiveDelegate = (receiveBuffer) =>
            {
                OctetsStream ios = new OctetsStream(receiveBuffer);
                int mark = 0;
                while (ios.Length > mark)
                {
                    int type = 0;
                    int size = 0;
                    try
                    {
                        type = (int)ios.UncompactUInt32();
                        size = (int)ios.UncompactUInt32();
                    }
                    catch (MarshalException)
                    {
                        // 流中的数据量不够
                        break;
                    }

                    if (size > ios.Length - ios.Position)
                    {
                        break;
                    }

                    Protocol p = new Protocol();
                    p.Type = type;
                    p.Data = ios.UnmarshalOctets(size);

                    mark = ios.Position;

                    ReceiveDelegate(p);
                }
                receiveBuffer.Erase(0, mark);
            };

            _session.ExpectionDelegate = (e) =>
            {
                ExceptionDelegate(e.ErrorCode, e.Message);
            };

            _session.OnCloseDelegate = () =>
            {
                if (OnCloseDelegate != null)
                {
                    OnCloseDelegate();
                }
                _session = null;
            };

            _session.Connect();
        }

        public void Close()
        {
            if (_session != null)
            {
                _session.Close();
                _session = null;
            }
        }

        public void Send(Octets send)
        {
            if (_session != null)
            {
                _session.Send(send);
            }
        }
    }
}
