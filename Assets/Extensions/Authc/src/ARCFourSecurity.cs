﻿using System;

namespace Authc
{
    public class ARCFourSecurity : Security
    {
        private byte index1;
        private byte index2;
        private byte[] perm = new byte[256];

        public ARCFourSecurity() { }

        public override void SetParameter(Octets param)
        {
            int keylen = param.Length;
            byte j;
            int i;
            for (i = 0; i < 256; i++)
            {
                perm[i] = (byte)i;
            }
            i = 0;
            for (j = 0; i < 256; i++)
            {
                j += perm[i];
                j += param.Buffer[i % keylen];
                byte t = perm[i];
                perm[i] = perm[j];
                perm[j] = t;
            }
            index1 = index2 = 0;
        }

        public override Octets Update(Octets input)
        {
            int p = 0;
            int q = input.Length;
            while (p != q)
            {
                byte j1 = perm[++index1];
                byte j2 = perm[index2 += j1];
                perm[index2] = j1;
                perm[index1] = j2;

                input.Buffer[p++] ^= perm[(byte)(j1 + j2)];
            }
            return input;
        }
    }
}
