﻿using System;

namespace Authc
{
    class MPPCDecompress
    {
        const int CTRL_OFF_EOB = 0;
        const int MPPC_HIST_LEN = 8192;

        byte[] history = new byte[MPPC_HIST_LEN];
        int hptr = 0;
        int l;
        int adjust_l;
        int blen;
        int blen_total;
        int rptr;
        int adjust_rptr;
        Octets legacyIn = new Octets();

        public static UInt32 ReverseBytes(UInt32 value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
                   (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }

        bool PassBits(int n)
        {
            l += n;
            blen += n;
            if (blen < blen_total)
            {
                return true;
            }
            l = adjust_l;
            rptr = adjust_rptr;
            return false;
        }

        uint Fetch()
        {
            rptr += l >> 3;
            l &= 7;
            return ReverseBytes(BitConverter.ToUInt32(legacyIn.Buffer, (int)rptr)) << l;
        }

        public Octets Update(Octets input)
        {
            legacyIn.Insert(input.Buffer, input.Length);
            blen_total = legacyIn.Length * 8 - l;
            legacyIn.Resize(legacyIn.Length + 3);
            rptr = 0;
            blen = 7;
            Octets output = input;
            output.Clear();
            int hhead = hptr;

            while (blen_total > blen)
            {
                adjust_l = l;
                adjust_rptr = rptr;
                uint val = Fetch();
                if (val < 0x80000000)
                {
                    if (!PassBits(8)) break;
                    history[hptr++] = (byte)(val >> 24);
                    continue;
                }
                if (val < 0xc0000000)
                {
                    if (!PassBits(9)) break;
                    history[hptr++] = (byte)(((val >> 23) | 0x80) & 0xff);
                    continue;
                }

                uint len = 0;
                uint off = 0;

                if (val >= 0xf0000000)
                {
                    if (!PassBits(10)) break;
                    off = (val >> 22) & 0x3f;
                    if (off == CTRL_OFF_EOB)
                    {
                        int advance = 8 - (l & 7);
                        if (advance < 8)
                        {
                            if (!PassBits(advance)) break;
                        }
                        output.Insert(history, hhead, hptr - hhead, output.Length);
                        if (hptr == MPPC_HIST_LEN)
                        {
                            hptr = 0;
                        }
                        hhead = hptr;
                        continue;
                    }
                }
                else if (val >= 0xe0000000)
                {
                    if (!PassBits(12)) break;
                    off = ((val >> 20) & 0xff) + 64;
                }
                else if (val >= 0xc0000000)
                {
                    if (!PassBits(16)) break;
                    off = ((val >> 16) & 0x1fff) + 320;
                }

                val = Fetch();
                if (val < 0x80000000)
                {
                    if (!PassBits(1)) break;
                    len = 3;
                }
                else if (val < 0xc0000000)
                {
                    if (!PassBits(4)) break;
                    len = 4 | ((val >> 28) & 3);
                }
                else if (val < 0xe0000000)
                {
                    if (!PassBits(6)) break;
                    len = 8 | ((val >> 26) & 7);
                }
                else if (val < 0xf0000000)
                {
                    if (!PassBits(8)) break;
                    len = 16 | ((val >> 24) & 15);
                }
                else if (val < 0xf8000000)
                {
                    if (!PassBits(10)) break;
                    len = 32 | ((val >> 22) & 0x1f);
                }
                else if (val < 0xfc000000)
                {
                    if (!PassBits(12)) break;
                    len = 64 | ((val >> 20) & 0x3f);
                }
                else if (val < 0xfe000000)
                {
                    if (!PassBits(14)) break;
                    len = 128 | ((val >> 18) & 0x7f);
                }
                else if (val < 0xff000000)
                {
                    if (!PassBits(16)) break;
                    len = 256 | ((val >> 16) & 0xff);
                }
                else if (val < 0xff800000)
                {
                    if (!PassBits(18)) break;
                    len = 0x200 | ((val >> 14) & 0x1ff);
                }
                else if (val < 0xffc00000)
                {
                    if (!PassBits(20)) break;
                    len = 0x400 | ((val >> 12) & 0x3ff);
                }
                else if (val < 0xffe00000)
                {
                    if (!PassBits(22)) break;
                    len = 0x800 | ((val >> 10) & 0x7ff);
                }
                else if (val < 0xfff00000)
                {
                    if (!PassBits(24)) break;
                    len = 0x1000 | ((val >> 8) & 0xfff);
                }
                else
                {
                    l = adjust_l;
                    rptr = adjust_rptr;
                    break;
                }

                if (hptr - off < 0 || hptr + len > MPPC_HIST_LEN)
                {
                    break;
                }
                for (int i = 0; i < len; i++)
                {
                    history[hptr + i] = history[hptr - off + i];
                }
                hptr += (int)len;
            }
            if (hptr != hhead)
            {
                output.Insert(history, hhead, hptr - hhead, output.Length);
            }

            legacyIn.Erase(0, rptr);
            return output;
        }
    }
}
