﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Authc
{

    class Session
    {
        public delegate void SessionReceiveDelegate(Octets receiveBuffer);
        public delegate void SessionExpectionDelegate(SocketException e);
        public delegate void SessionOnCloseDelegate();

        private const int BufferSize = 1024;

        private String ip;
        private int port;
        private Socket client;

        private Octets inBuffer = new Octets(BufferSize);
        private Octets outBuffer = new Octets(BufferSize);

        private Octets sendBuffer = new Octets();
        private Octets receiveBuffer = new Octets();

        private Security _inSecurity = new NullSecurity();
        private Security _outSecurity = new NullSecurity();

        private SessionReceiveDelegate _receiveDelegate;
        private SessionExpectionDelegate _expectionDelegate;
        private SessionOnCloseDelegate _oncloseDelegate;

        public SessionReceiveDelegate ReceiveDelegate
        {
            set
            {
                _receiveDelegate = value;
            }
            get
            {
                return _receiveDelegate;
            }
        }
        public SessionOnCloseDelegate OnCloseDelegate
        {
            set
            {
                _oncloseDelegate = value;
            }
            get
            {
                return _oncloseDelegate;
            }
        }

        public Security InSecurity
        {
            set
            {
                _inSecurity = value;
            }
            get
            {
                return _inSecurity;
            }
        }

        public Security OutSecurity
        {
            set
            {
                _outSecurity = value;
            }
            get
            {
                return _outSecurity;
            }
        }

        public SessionExpectionDelegate ExpectionDelegate
        {
            get
            {
                return _expectionDelegate;
            }

            set
            {
                _expectionDelegate = value;
            }
        }

        public Session(String ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public void Connect()
        {
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(ip), port);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.ReceiveTimeout = 1000;
            try
            {
                client.BeginConnect(remoteEP, (ar) =>
                {
                    try
                    {
                        client.EndConnect(ar);
                        Receive();
                    }
                    catch (SocketException e)
                    {
                        ExpectionDelegate(e);
                        client.Close();
                        OnCloseDelegate();
                    }
                }, client);
            }
            catch (SocketException e)
            {
                ExpectionDelegate(e);
                client.Close();
                OnCloseDelegate();
            }
        }

        public void Close()
        {
            if (client != null)
            {
                client.Close();
            }
        }

        public void Receive()
        {
            try
            {
                client.BeginReceive(inBuffer.Buffer, inBuffer.Length, inBuffer.Capacity - inBuffer.Length, SocketFlags.None, (ar) =>
                {
                    try
                    {
                        int receiveBytes = client.EndReceive(ar);
                        if (receiveBytes > 0)
                        {
                            inBuffer.Length = inBuffer.Length + receiveBytes;
                            inBuffer = _inSecurity.Update(inBuffer);
                            receiveBuffer.Insert(inBuffer.Buffer, inBuffer.Length);
                            inBuffer.Clear();

                            if (ReceiveDelegate != null)
                            {
                                ReceiveDelegate(receiveBuffer);
                            }

                            Receive();
                        }
                        else
                        {
                            Console.WriteLine("socket closed by server");
                            client.Close();
                            OnCloseDelegate();
                        }
                        
                    }
                    catch (SocketException e)
                    {
                        Console.WriteLine(e);
                        ExpectionDelegate(e);
                        client.Close();
                        OnCloseDelegate();
                    }
                    catch (ObjectDisposedException e)
                    {
                        // socket already closed
                        Console.WriteLine("receive: socket already closed "  + e.ToString() );
                    }
                }, null);
            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
                ExpectionDelegate(e);
                client.Close();
                OnCloseDelegate();
            }
            catch (ObjectDisposedException e)
            {
                // socket already closed
                Console.WriteLine("receive: socket already closed" + e.ToString());
            }

        }

        public void Send(Octets send)
        {
            outBuffer.Insert(send.Buffer, send.Length);
            outBuffer = _outSecurity.Update(outBuffer);
            sendBuffer.Insert(outBuffer.Buffer, outBuffer.Length);
            outBuffer.Clear();

            try
            {
                client.BeginSend(sendBuffer.Buffer, 0, sendBuffer.Length, SocketFlags.None, (ar) =>
                {
                    try
                    {
                        client.EndSend(ar);
                    }
                    catch (SocketException e)
                    {
                        Console.WriteLine(e);
                        ExpectionDelegate(e);
                        client.Close();
                        OnCloseDelegate();
                    }
                }, null);
            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
                ExpectionDelegate(e);
                client.Close();
                OnCloseDelegate();
            }
            catch (ObjectDisposedException e)
            {
                // socket already closed
                Console.WriteLine("send: socket already closed" + e.ToString());
            }
            sendBuffer.Clear();
        }
    }
}
